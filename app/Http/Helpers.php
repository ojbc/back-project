<?php
use App\Utils\Enums\EnumResponse;
use App\Utils\Enums\InventoryError;

if (!function_exists('bodyResponseRequest')) {

    /**
     * This function a implement custom response request JSON in Playbox.
     *
     * @param string $codeResp value of code transaction
     * @param array|string $data value of data result
     * @param string|null $customMessage value of custom message
     * @param string|null $methodException value of method exception
     * @return \Illuminate\Http\JsonResponse
     */
    function bodyResponseRequest($codeResp, $data = [], $customMessage = null, $methodException = null)
    {

        switch ($codeResp) {

            case EnumResponse::SUCCESS:

                return response()->json([
                    'status' => \Illuminate\Http\Response::HTTP_OK,
                    'title' => '¡Exitoso!',
                    'message' => 'Operación realizada exitosamente',
                    'data' => $data
                ], \Illuminate\Http\Response::HTTP_OK);

                break;

            case EnumResponse::POST_SUCCESS:

                return response()->json([
                    'status' => \Illuminate\Http\Response::HTTP_OK,
                    'title' => '¡Exitoso!',
                    'message' => 'Transacción efectuada exitosamente'
                ], \Illuminate\Http\Response::HTTP_OK);

                break;

            case EnumResponse::CUSTOM_SUCCESS:

                return response()->json([
                    'status' => \Illuminate\Http\Response::HTTP_OK,
                    'title' => '¡Exitoso!',
                    'message' => $customMessage,
                    'data' => $data
                ], \Illuminate\Http\Response::HTTP_OK);

                break;

            case EnumResponse::FAILED:

                return response()->json([
                    'status' => \Illuminate\Http\Response::HTTP_BAD_REQUEST,
                    'title' => '¡Fallido!',
                    'message' => 'Operación no efectuada',
                    'motives' => $data
                ], \Illuminate\Http\Response::HTTP_UNAUTHORIZED);

                break;

            case EnumResponse::CUSTOM_FAILED:

                return response()->json([
                    'status' => \Illuminate\Http\Response::HTTP_BAD_REQUEST,
                    'title' => '¡Fallido!',
                    'motives' =>array($customMessage)
                ], \Illuminate\Http\Response::HTTP_UNAUTHORIZED);

                break;

            case EnumResponse::EMPTY_NOTIFY:

                return response()->json([
                    'status' => \Illuminate\Http\Response::HTTP_BAD_REQUEST,
                    'title' => '¡Atención!',
                    'motives' => array('No se encontraron registros en la búsqueda')
                ], \Illuminate\Http\Response::HTTP_UNAUTHORIZED);

                break;

            case EnumResponse::ERROR:


                return response()->json([
                    'status'=>\Illuminate\Http\Response::HTTP_BAD_REQUEST,
                    'title'=>'¡Error!',
                    'message'=>'Ocurrió un error al procesar tu solicitud',
                    'methodException'=> $methodException
                ],\Illuminate\Http\Response::HTTP_BAD_REQUEST);

                break;

        }
    }
}

/**
 * Custom messages response json request in Playbox.
 *
 * @author David Rivero <david.dotworkers@gmail.com>
 */
if (!function_exists('responseRequest')) {

    /**
     * This function a implement custom response request JSON in My Community
     *
     * @param string $codeResp
     * @param array|null  $data
     * @param string|null  $code_error
     * @return \Illuminate\Http\JsonResponse
     */
    function responseRequest($codeResp, $data = null, $code_error = null)
    {

        switch ($codeResp) {

            case EnumResponse::SUCCESS:

                return response()->json([
                    'status' => \Illuminate\Http\Response::HTTP_OK,
                    'title' => '¡Exitoso!',
                    'message' => 'Operación realizada exitosamente',
                    'data' => $data
                ], \Illuminate\Http\Response::HTTP_OK);

                break;

            case EnumResponse::POST_SUCCESS:

                return response()->json([
                    'status' => \Illuminate\Http\Response::HTTP_OK,
                    'title' => '¡Exitoso!',
                    'message' => 'Transacción efectuada exitosamente'
                ], \Illuminate\Http\Response::HTTP_OK);

                break;

            case EnumResponse::FAILED:

                return response()->json(
                    InventoryError::getErrorStatus($code_error),
                    \Illuminate\Http\Response::HTTP_OK
                );

                break;

            case EnumResponse::CUSTOM_FAILED:

                return response()->json(
                    InventoryError::getErrorStatus($code_error, $data),
                    \Illuminate\Http\Response::HTTP_OK
                );

                break;

        }
    }
}