<?php

namespace App\Http\Controllers;

use App\Preload;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;
use App\Repository\Preload\PreloadRepo;
use App\Company;
use App\BankCompany;
use App\Bank;
use App\Utils\Enums\AuditOperation;
use App\Utils\Enums\EnumResponse;
use App\User;
use carbon\carbon;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\Finder;
use App\Utils\ServerSide;
/**
 * Class PreloadController.
 *
 * @package App\Http\Controllers
 * @author  <info@expertosdelaweb.net>
 */

class PreloadController extends Controller
{
    /**
     * Create a new construct instance.
     * @route rest-api-project\App\Repository\Preload
     * @param  PreloadRepo $_preloadRepo
     * @return void
     */
    public function __construct(PreloadRepo $_preloadRepo )
    {
        $this->preloadRepo = $_preloadRepo;
    }
     /**
     * Save Preload.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function save( Request $request )
    {
        try {


            $reference='Debe ingresar la referencia.!';
            $amount='Debe ingresar el monto.!';
            $amounts='El monto debe ser mayor de cero.!';
            $bank ='Debe ingresar el banco.!';
            $voucher_date ='Debe ingresar la fecha.!';

            if(!isset($request->reference)){
                return bodyResponseRequest( EnumResponse::FAILED, $reference );
            }
               
            if(!isset($request->amount)){
                return bodyResponseRequest( EnumResponse::FAILED, $amount );
            }
            if(!isset($request->bank)){
                return bodyResponseRequest( EnumResponse::FAILED, $bank );
            }
            if(!isset($request->voucher_date)){
                return bodyResponseRequest( EnumResponse::FAILED, $voucher_date );
            }
            if ( ((float)$request->amount) <=0) {
                return bodyResponseRequest( EnumResponse::FAILED, $amounts );

            }
                $data = $this->preloadRepo->save( $request );

             return bodyResponseRequest( EnumResponse::SUCCESS, $data );

        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PreloadController.save.catch' );
        }
    }
    /**
     * Alls Preload.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function alls(Request $request)
    {
        try {
            $data=Preload::where('company_id', $request->company_id)
                ->with('users')
                ->get();
            if (!$data) {
                return bodyResponseRequest(EnumResponse::FAILED,'No existe ningun pago');
            }
            if (!$data[0]) {
                return bodyResponseRequest(EnumResponse::FAILED,'No existe ningun pago');
            }
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'BankController.alls.catch' );
        }

    }
  

    /**
     * paginate PRELOADS.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function paginate(Request $request)
    {
      
        try {
            $data = $this->preloadRepo->paginate( $request );
                return response()->json( $data, \Illuminate\Http\Response::HTTP_OK );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'BankController.alls.catch' );
        }

         
    }

    /**
     * deletePreload Preload.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */

    public function deletePreload( Request $request )
    {
        try {

            $data = $this->preloadRepo->deletePreload( $request );

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );

        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PreloadController.deletePreload.catch' );
        }
    }

    /**
     * checkPreload Preload.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */

    public function checkPreload( Request $request )
    {
        try {

            $data = $this->preloadRepo->checkPreload( $request );

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );

        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PreloadController.checkPreload.catch' );
        }
    }

    /**
     * PrintPdfnp Preload.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function printPdfnp(Request $request)
    {
        try {

            $data = $this->preloadRepo->printPdfnp($request);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PreloadController.printPdfnp.catch' );
        }
    }

    /**
     *  PrintPdfap Preload.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function printPdfap(Request $request)
    {
        try {

            $data = $this->preloadRepo->printPdfap($request);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PreloadController.printPdfap.catch' );
        }
    }
    /**
     *  Deletelist Preload.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function deletelist(Request $request)
    {
        try {

            $data = $this->preloadRepo->deletelist($request);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PreloadController.deletelist.catch' );
        }
    }

    
}
