<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repository\Bank\BankRepository;
use App\Repository\Bank\Manager\BankManager;
use App\Utils\Enums\EnumResponse;
use App\Utils\Enums\AuditOperation;
use App\BankCompany;
use App\Bank;
use carbon\carbon;
use Illuminate\Support\Facades\DB;
/**
 * Class BankController.
 *
 * @package App\Http\Controllers
 * @author  <info@expertosdelaweb.net>
 */
class BankController extends Controller
{
    /**
     * Create a new construct instance.
     * @route rest-api-project\app\Repository\Bank
     * @param  BankRepository $_bankRepository
     * @return void
     */
    public function __construct( BankRepository $_bankRepository )
    {
        $this->bankRepository = $_bankRepository;
    }
    /**
     * All Bank.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function all(Request $request)
    {
        try {
        $data=BankCompany::where('company_id', $request->company_id)
            ->where('status', 1)
        ->with('Banks')
        ->get();
        if (!$data) {
            return bodyResponseRequest(EnumResponse::FAILED,'No existe ningun pago con esa referencia');
        }
        if (!$data[0]) {
            return bodyResponseRequest(EnumResponse::FAILED,'No existe ningun pago con esa referencia');
        }
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'BankController.all.catch' );
        }
    }
    /**
     * Alls Bank.
     *
     * @param 
     * @return bodyResponseRequest $data
     */
    public function alls()
    {
        try {
        $data=Bank::all();
       
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'BankController.all.catch' );
        }
    }
    /**
     * Save Bank.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function save(Request $request)
    {
        try {
            if(isset($request->id)){


                $data = $this->bankRepository->save( $request );

                return bodyResponseRequest(EnumResponse::SUCCESS, $data);
            }


                //
                $data = $this->bankRepository->save( $request );

                return bodyResponseRequest( EnumResponse::SUCCESS, $data );



        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'BankController.save.catch' );
        }
    }
    /**
     * Delete Bank.
     *
     * @param Request $request
     * @return bodyResponseRequest $request->campaign
     */
    public function delete( Request $request )
    {
        try {
            // Borramos
            $this->bankRepository->destroy($request->id);
            // CREAMOS LA AUDITORIA.
            auditSecurity( Auth::id(), AuditOperation::DELETE, 'Bank', 'Bank', ['id' => $request->id]);

            return bodyResponseRequest( EnumResponse::SUCCESS, $request->id);

        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'BankController.delete.catch' );
        }
    }
}
