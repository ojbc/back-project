<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;
use App\Repository\User\UserRepository;
use App\Repository\User\Manager\UserManager;
use App\Utils\Enums\AuditOperation;
use App\Utils\Enums\EnumResponse;
use App\User;
/**
 * Class UsersController.
 *
 * @package App\Http\Controllers
 * @author  <info@expertosdelaweb.net>
 */
class UsersController extends Controller
{
    /**
     * Create a new construct instance.
     * @route rest-api-project\app\Repository\User
     * @param  UserRepository $_userRepository
     * @return void
     */
    public function __construct( UserRepository $_userRepository )
    {
        $this->userRepository = $_userRepository;
    }
    /**
     * Profile User.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function profile()
    {
        try {
            $user = \JWTAuth::parseToken()->toUser();
            $data = User::where('id', '=',$user->id)
                ->with(['profile' => function( $query ) {
                }])->orderBy('created_at', 'DESC')->get();
               
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'ManagerController.profile.catch' );
        }
    }
    /**
     * All Users.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function all(Request $request)
    {
        try {

            $data = $this->userRepository->allWith(['profile', 'role', 'company'])->where('company_id', $request->company_id)->orderBy('created_at', 'DESC')->get();

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'UsersController.all.catch' );
        }
    }
    /**
     * GetFullDetails Users.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function getFullDetails( Request $request )
    {
        try {

            $data = $this->userRepository->findWith( $request->user_id, ['profile', 'role', 'permissions', 'company']);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'UsersController.all.catch' );
        }
    }
    /**
     * ChangeRole Users.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function changeRole( Request $request )
    {
        try {

            $data = $this->userRepository->changeRole( $request );

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'UsersController.changeRole.catch' );
        }
    }
    /**
     * Save Users.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function save( Request $request )
    {
        try {


                //
                $data = $this->userRepository->save( $request );

                return bodyResponseRequest( EnumResponse::SUCCESS, $data );

        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'SecurityController.saveRole.catch' );
        }
    }
    /**
     * Delete Users.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function delete( Request $request )
    {
        try {
            // Necesitamos verificar que no seamos nostros mismos quien nos eliminamos.
            if( Auth::user()->id != $request->user ) {
                $data = $this->userRepository->destroy( $request->user );
                $user = JWTAuth::parseToken()->authenticate();
                // CREAMOS LA AUDITORIA.
                auditSecurity( Auth::id(), $user->company_id, AuditOperation::DELETE, 'SECURITY.USERS', 'User', ['id' => $request->user]);

                return bodyResponseRequest( EnumResponse::SUCCESS, $request->user );
            }

            return bodyResponseRequest( EnumResponse::CUSTOM_FAILED, [], _('No puedes borrarte a ti mismo.'));
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'UsersController.delete.catch' );
        }
    }

    /**
     * FindUsers Users.
     *
     * @param Request $request
     * @return bodyResponseRequest $resultusers->values()->all()
     */
    public function findUsers(Request $request)
    {
        $users = $this->userRepository->findUsers($request->all());

        $resultusers = collect();

        if (count($users)) {
            foreach ($users as $user) {
                $resultusers->push([
                    'id'=> $user->id,
                    'names'=> $user->names,
                    'lastnames' => $user->lastnames,
                    'email' => $user->email,
                    'complete_name' => sprintf('%s %s',
                        $user->names,
                        $user->lastnames
                    ),
                    'ci' => $user->ci,
                    'rif' => $user->rif,
                    'rol_name' => $user->rol_name
                ]);
            }
        }

        return responseRequest(EnumResponse::SUCCESS, [
            'users' => $resultusers->values()->all()
        ]);
    }
    /**
     * SaveUpdate User.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function SaveUpdate(Request $request)
    {
        try {
            if ( $request->id ) {
                //
                $data = $this->userRepository->SaveUpdate($request);

                return bodyResponseRequest(EnumResponse::SUCCESS, $data);
            }

        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'UserController.SaveUpdate.catch' );
        }
    }
     /**
     * allUserCompany Users.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function allUserCompany(Request $request)
    {
        try {
            $users = JWTAuth::parseToken()->authenticate();
            $data = $this->userRepository->allWith(['profile', 'role', 'company'])->where('id','<>',  $users->id)->orderBy('created_at', 'DESC')->get();

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'UsersController.allUserCompany.catch' );
        }
    }
     /**
     * resetPassword SuperAdmin.
     *
     * @param $request
     * @return $data
     */
    public function resetPassword(Request $request )
    {  
        try {
               
             $data = $this->userRepository->resetPassword($request);

            return bodyResponseRequest(EnumResponse::SUCCESS, $data);
                

            } catch (\Exception $e) {
                return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'UserController.resetPassword.catch' );

            }
    }
    /**
     * status User.
     *
     * @param Request $request
     * @return bodyResponseRequest $request->status
     */
    public function status(Request $request)
    {
        try {
        
            $this->userRepository->status(['status' => $request->status], $request->user);
            return bodyResponseRequest(EnumResponse::SUCCESS, $request->status);

        } catch (\Exception $e) {
            return bodyResponseRequest(EnumResponse::ERROR, $e, [], 'UserController.delete.catch');
        }
    }
}