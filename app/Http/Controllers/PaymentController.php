<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repository\Payment\PaymentRepository;
use App\Repository\DataBankTransaction\DataBankTransactionRepo;
use Illuminate\Support\Facades\DB;
use App\Utils\Enums\EnumResponse;
use App\Utils\Enums\AuditOperation;
use App\Payment;
use JWTAuth;
use carbon\carbon;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\Finder;
use App\Utils\ServerSide;
/**
 * Class PaymentController.
 *
 * @package App\Http\Controllers
 * @author  <info@expertosdelaweb.net>
 */
class PaymentController extends Controller
{
    /**
     * @var PaymentRepository $_PaymentRepository
     * @var DataBankTransactionRepo $_DataBank
     */
    private $PaymentRepository;
    /**
     * Create a new construct instance.
     * @route rest-api-project\app\Repository\RegisterPay
     * @param  PaymentRepository $_PaymentRepository
     * @param  DataBankTransactionRepo $_DataBank
     * @return void
     */
    public function __construct( PaymentRepository $_PaymentRepository, DataBankTransactionRepo $_DataBank)
    {
        $this->PaymentRepository = $_PaymentRepository;
        $this->DataBankTransactionRepo = $_DataBank;
    }
    /**
     * All payment.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function all(Request $request)
    {
        try {
            $data = Payment::with('users') ->where('company_id', $request->company_id)->get();
            
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.all.catch' );
        }
    }
      /**
     * All Manager.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function paginate(Request $request)
    {
        
        $primaryKey = 'id';
        $query = "
            SELECT 
                `payments`.id,
                `payments`.user_id, 
                `payments`.reference, 
                `payments`.bank,
                `payments`.voucher_date,
                `payments`.amount,
                concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
                `payments`.created_at,
                `payments`.description
            FROM `profiles` 
            INNER JOIN payments ON payments.user_id = `profiles`.user_id
            WHERE `payments`.company_id = {$request['company_id']}";
         
        $columns = array(
            array( 'db' => 'id', 'dt' => 'id' ),
            array( 'db' => 'user_id', 'dt' => 'user_id' ),
            array( 'db' => 'reference', 'dt' => 'reference' ),
            array( 'db' => 'bank', 'dt' => 'bank' ),
            array( 'db' => 'voucher_date', 'dt' => 'voucher_date' ),
            array( 'db' => 'amount', 'dt' => 'amount' ),
            array( 'db' => 'full_name', 'dt' => 'full_name' ),
            array( 'db' => 'created_at', 'dt' => 'created_at' ),
            array( 'db' => 'description', 'dt' => 'description' ),
        );
        
        $data = ServerSide::simple( $request, $query, $primaryKey, $columns );

        return response()->json( $data, \Illuminate\Http\Response::HTTP_OK ); 
    }
    /**
     * Save payment.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function save(Request $request)
    {
        try {

            if (!$this->PaymentRepository->paidValidate($request)) {
                return bodyResponseRequest(EnumResponse::FAILED,'Algunos de los datos no coinciden con el registro.');
            }
            if ($this->PaymentRepository->isExistRegisterPay($request)) {
                return bodyResponseRequest(EnumResponse::FAILED,'Este pago ya fue registrado');
            }
            if ($this->PaymentRepository->isLockedBank($request)) {
                return bodyResponseRequest(EnumResponse::FAILED,'Este pago se encuentra bloqueado');
            }
            

               $data = $this->PaymentRepository->save($request);
               $this->DataBankTransactionRepo->verified($request);
                return bodyResponseRequest(EnumResponse::SUCCESS, $data);

        } catch (\Exception $e) {
            return bodyResponseRequest(EnumResponse::ERROR, $e, [], 'PaymentController.save.catch');
        }
    }
    /**
     * ReferenceID payment.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function referenceID(Request $request)
    {
        try {

            $ref = $this->PaymentRepository->isExistReferenceBank($request);


            if (!$ref) {
                return bodyResponseRequest(EnumResponse::FAILED,'No existe ningun pago con esa referencia');
            }
            $data = $this->PaymentRepository->referenceID($request);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.referenceID.catch' );
        }
    }


    /**
     * ReferenceInfo payment.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function referenceInfo(Request $request)
    {
        try {


            $data = $this->PaymentRepository->referenceInfo($request);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.referenceInfo.catch' );
        }
    }
    /**
     * Payments payment.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function payments(Request $request)
    {
        try {


            $data = $this->PaymentRepository->payments($request);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.payments.catch' );
        }
    }
    /**
     * Total payment.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function total(Request $request)
    {
        try {

           $data = $this->PaymentRepository->total($request);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.total.catch' );
        }
    }
    /**
     * Today payment.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function today( Request $request)
    {
        try {


            $data = $this->PaymentRepository->today($request);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.today.catch' );
        }
    }
    /**
     * PrintPdf  payment.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function printPdf(Request $request)
    {
        try {

            $data = $this->PaymentRepository->printPdf($request);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.printPdf.catch' );
        }
    }

    /**
     * PrintPdf  payment.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function printPdfToday(Request $request)
    {
        try {
            $data = $this->PaymentRepository->printPdfToday($request);
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.printPdfToday.catch' );
        }
    }
    /**
     * check payment.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function check(Request $request)
    {
        try {
           $amount ='Debe introducir un monto.!';
            $reference ='Debe introducir una referencia.!';
            $bank ='Debe introducir un banco.!';
            $today ='Debe introducir un fecha.!';
            $bankP ='Este Banco no esta permitido para este metodo';
            if(!isset($request->data['amount'])){
                return bodyResponseRequest( EnumResponse::FAILED, $amount );
            }
            if(!isset($request->data['ref'])){
                return bodyResponseRequest( EnumResponse::FAILED, $reference );
            }
            if(!isset($request->bank)){
                return bodyResponseRequest( EnumResponse::FAILED, $bank );
            }
            if($request->bank === 'Provincial'){
                return bodyResponseRequest( EnumResponse::FAILED, $bankP );
            }
            if(!isset($request->voucher_date)){
                return bodyResponseRequest( EnumResponse::FAILED, $today );
            }
            $data = $this->PaymentRepository->checks($request);
            if (!$data) {
                return bodyResponseRequest(EnumResponse::FAILED,'No existe ningun pago con esa referencia');
            }
            if (!$data[0]) {
                return bodyResponseRequest(EnumResponse::FAILED,'No existe ningun pago con esa referencia');
            }
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.check.catch' );
        }
    }
    /**
     * searchVoucher payment.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function searchVoucher(Request $request)
    {
        try {
            $amount ='Debe introducir un monto.!';
            $reference ='Debe introducir una referencia.!';
            $bank ='Debe introducir un banco.!';
            $today ='Debe introducir un fecha.!';
         
            if(!isset($request->amount)){
                return bodyResponseRequest( EnumResponse::FAILED, $amount );
            }
            if(!isset($request->reference)){
                return bodyResponseRequest( EnumResponse::FAILED, $reference );
            }
            if(!isset($request->bank)){
                return bodyResponseRequest( EnumResponse::FAILED, $bank );
            }
            if(!isset($request->voucher_date)){
                return bodyResponseRequest( EnumResponse::FAILED, $today );
            }
            

          $data = $this->PaymentRepository->searchVoucher($request);
         
          if (isset($data[0]['id'])) {
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        }else{
            return bodyResponseRequest(EnumResponse::FAILED,'No existe ningun pago con esa referencia');
        }
          
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.searchVoucher.catch' );
        }
    }

    /**
     *  searchCDN payment.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function  searchCDN(Request $request)
    {
        try {
          $data = $this->PaymentRepository->searchCDN($request);
         
          
          if (!$data) {
            return bodyResponseRequest(EnumResponse::FAILED,'No existe ningun pago con esos datos');
        }
        if (!$data[0]) {
            return bodyResponseRequest(EnumResponse::FAILED,'No existe ningun pago con  esos datos');
        }

          return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.searchCDN.catch' );
        }
    }
    /**
     *  searchCDNData payment.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function  searchCDNData(Request $request)
    {
        try {
            $amount ='Debe introducir un monto.!';
            
            $today ='Debe introducir un fecha.!';
         
            if(!isset($request->data['amount'])){
                return bodyResponseRequest( EnumResponse::FAILED, $amount );
            }
            
            if(!isset($request->voucher_date)){
                return bodyResponseRequest( EnumResponse::FAILED, $today );
            }
           
            $data = $this->PaymentRepository-> searchCDNData($request);
            
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.searchCDNData.catch' );
        }
    }
    /**
    *  upData payment.
    *
    * @param Request $request
    * @return bodyResponseRequest $data
    */
    public function  upData(Request $request)
    {
        try {

            $data = $this->PaymentRepository->upData($request);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.upData.catch' );
        }
    }

    public function todaysPaginates( Request $request)
    {
        try {


            $data = $this->PaymentRepository->paginates($request);

            return response()->json( $data, \Illuminate\Http\Response::HTTP_OK );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.todaysPaginates.catch' );
        }
    }

    public function totalPaginates( Request $request)
    {
        try {

            $data = $this->PaymentRepository->totalPaginates($request);

            return response()->json( $data, \Illuminate\Http\Response::HTTP_OK );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'PaymentController.totalPaginates.catch' );
        }
    }
    

    

}
