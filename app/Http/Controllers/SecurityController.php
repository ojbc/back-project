<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repository\Security\RoleRepository;
use App\Repository\Security\PermissionRepository;
use App\Repository\AuditSecurity\AuditRepository;
use App\Repository\Security\Manager\RoleManager;
use App\Repository\Security\Manager\PermissionManager;
use App\Utils\Enums\EnumResponse;
use App\Utils\Enums\AuditOperation;
use Illuminate\Support\Facades\DB;
use carbon\carbon;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\Finder;
use App\Utils\ServerSide;

/**
 * Class SecurityController.
 *
 * @package App\Http\Controllers
 * @author  <info@expertosdelaweb.net>
 */
class SecurityController extends Controller
{
    /**
     * Create a new construct instance.
     * @route rest-api-project\app\Repository\Security
     * @param  RoleRepository $_rolRepository
     * @param  PermissionRepository $_permissionRepository
     * @route rest-api-project\app\Repository\AuditSecurity
     * @param  AuditRepository $_auditRepository
     * @return void
     */
    public function __construct(
        RoleRepository $_rolRepository,
        PermissionRepository $_permissionRepository,
        AuditRepository $_auditRepository )
    {
        $this->rolRepository = $_rolRepository;
        $this->permissionRepository = $_permissionRepository;
        $this->auditRepository = $_auditRepository;
    }
    /**
     * AllRoles Security.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function allRoles()
    {
        try {

            //$data = $this->rolRepository->all();
            $data = DB::table('roles')
            ->whereIn('id', [1, 2])
            ->get();
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'SecurityController.allRoles.catch' );
        }
    }
    /**
     * TogglePermissionRole Security.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function togglePermissionRole( Request $request )
    {
        try {

            $data = $this->rolRepository->togglePermission( $request );

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'SecurityController.togglePermissionRole.catch' );
        }
    }
    /**
     * GetPermissionByRole Security.
     *
     * @param $slug
     * @return bodyResponseRequest $data
     */
    public function getPermissionByRole( $slug )
    {
        try {

            $data = $this->rolRepository->getPermission( $slug );

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'SecurityController.getPermissionByRole.catch' );
        }
    }
    /**
     * SaveRole Security.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function saveRole( Request $request )
    {
        try {

            $manager = new RoleManager( $request->all() );

            if ( $manager->validate() ) {
                //
                $data = $this->rolRepository->saveRole( $request->all() );

                return bodyResponseRequest( EnumResponse::SUCCESS, $data );
            } else {
                return bodyResponseRequest( EnumResponse::FAILED, $manager->getError() );
            }
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'SecurityController.saveRole.catch' );
        }
    }
    /**
     * AllPermission Security.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function allPermission()
    {
        try {

            $data = $this->permissionRepository->all();

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'SecurityController.allPermission.catch' );
        }
    }
    /**
     * SavePermission Security.
     *
     * @param Request $request, $perm_id = null
     * @return bodyResponseRequest $data
     */
    public function savePermission( Request $request, $perm_id = null )
    {
        try {

            $manager = new PermissionManager( $request->all() );

            if ( $manager->validate() ) {
                //
                $data = $this->permissionRepository->savePermission( $request->all(), $perm_id );

                return bodyResponseRequest( EnumResponse::SUCCESS, $data );
            } else {
                return bodyResponseRequest( EnumResponse::FAILED, $manager->getError() );
            }
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'SecurityController.savePermission.catch' );
        }
    }
    /**
     * AllConsulting Security.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function allConsulting(Request $request)
    {
        try {

            $data = $this->auditRepository->allWith(['profile'])->where('company_id', $request->company_id)->orderBy('created_at', 'DESC')->get();

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'SecurityController.allConsulting.catch' );
        }
    }

       /**
     * All Manager.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function paginate(Request $request)
    {
        
        $primaryKey = 'id';
        $query = "
            SELECT 
                `audits`.id,
                `audits`.user_id, 
                `audits`.module, 
                `audits`.type_operation,
                `audits`.details,
                concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
                `audits`.created_at
            FROM `profiles` 
            INNER JOIN audits ON audits.user_id = `profiles`.user_id
            WHERE `audits`.company_id = {$request['company_id']}";
         
        $columns = array(
            array( 'db' => 'id', 'dt' => 'id' ),
            array( 'db' => 'user_id', 'dt' => 'user_id' ),
            array( 'db' => 'module', 'dt' => 'module' ),
            array( 'db' => 'type_operation', 'dt' => 'type_operation' ),
            array( 'db' => 'details', 'dt' => 'details' ),
            array( 'db' => 'full_name', 'dt' => 'full_name' ),
            array( 'db' => 'created_at', 'dt' => 'created_at' ),
        );
        
        $data = ServerSide::simple( $request, $query, $primaryKey, $columns );

        return response()->json( $data, \Illuminate\Http\Response::HTTP_OK ); 
    }
}
