<?php

namespace App\Http\Controllers;
use App\DataBankTransaction;
use App\Utils\Enums\EnumResponse;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;
use App\Utils\Enums\AuditOperation;
use Illuminate\Support\Facades\DB;
use carbon\carbon;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\Finder;
use App\Utils\ServerSide;


/**
 * Class AdvancedSearchController.
 *
 * @package App\Http\Controllers
 * @author  <info@expertosdelaweb.net>
 */
class AdvancedSearchController extends Controller
{
    
    /**
     * Search AdvancedSearch.
     *
     * @param Request $request
     * @return bodyResponseRequest $DataBankTransaction
     */
    public function search(Request $request)
    {
      
        
        $primaryKey = 'id';

    
          if( (isset($request['element'])) || (isset($request['name_bank']))){
            if((isset($request['name_bank'])) && (isset($request['element'])) && (isset($request['company_id']))){

                
                $query = "
                SELECT 
                    `data_banks_transactions`.id,
                    `data_banks_transactions`.transaction_date,
                    `data_banks_transactions`.reference,
                    `data_banks_transactions`.amount,  
                    `data_banks_transactions`.description
                    FROM data_banks_transactions
                    WHERE data_banks_transactions.company_id = {$request['company_id']} 
                    AND data_banks_transactions.name_bank = '{$request['name_bank']}' AND
                    (data_banks_transactions.reference like '%{$request['element']}%'
                    OR data_banks_transactions.transaction_date like '%{$request['element']}%'
                    OR data_banks_transactions.amount like '%{$request['element']}%'
                    OR data_banks_transactions.description like '%{$request['element']}%')";
            
                }elseif((isset($request['name_bank'])) && (isset($request['company_id']))){
                $query = "
                SELECT 
                    `data_banks_transactions`.id,
                    `data_banks_transactions`.transaction_date,
                    `data_banks_transactions`.reference,
                    `data_banks_transactions`.amount,
                    `data_banks_transactions`.description
                    FROM data_banks_transactions
                    WHERE data_banks_transactions.company_id = {$request['company_id']} 
                    AND data_banks_transactions.name_bank = '{$request['name_bank']}'";
                   
                }elseif((isset($request['element'])) && (isset($request['company_id']))){
                $query = "
                SELECT 
                    `data_banks_transactions`.id,
                    `data_banks_transactions`.transaction_date,
                    `data_banks_transactions`.reference,
                    `data_banks_transactions`.amount,  
                    `data_banks_transactions`.description
                    FROM data_banks_transactions
                    WHERE data_banks_transactions.company_id = {$request['company_id']} 
                    AND 
                    (data_banks_transactions.reference like '%{$request['element']}%'
                    OR data_banks_transactions.transaction_date like '%{$request['element']}%'
                    OR data_banks_transactions.amount like '%{$request['element']}%'
                    OR data_banks_transactions.description like '%{$request['element']}%')";
           
                }
                else{
                    $query = "
                    SELECT 
                        `data_banks_transactions`.id,
                        `data_banks_transactions`.transaction_date,
                        `data_banks_transactions`.reference,
                        `data_banks_transactions`.amount,  
                        `data_banks_transactions`.description
                        FROM data_banks_transactions
                        WHERE data_banks_transactions.company_id = 0";
                }
            
          }else{
            $query = "
                    SELECT 
                        `data_banks_transactions`.id,
                        `data_banks_transactions`.transaction_date,
                        `data_banks_transactions`.reference,
                        `data_banks_transactions`.amount,  
                        `data_banks_transactions`.description
                        FROM data_banks_transactions
                        WHERE data_banks_transactions.company_id = 0";
          }
        
         
        $columns = array(
            array( 'db' => 'id', 'dt' => 'id' ),
            array( 'db' => 'transaction_date', 'dt' => 'transaction_date' ),
            array( 'db' => 'reference', 'dt' => 'reference' ),
            array( 'db' => 'amount', 'dt' => 'amount' ),
            array( 'db' => 'description', 'dt' => 'description' ),
        ); 
        $data = ServerSide::simple( $request, $query, $primaryKey, $columns );
        return response()->json( $data, \Illuminate\Http\Response::HTTP_OK ); 
          //  $DataBankTransaction= DB::table('data_banks_transactions')->orWhere('reference','=',$reference)->orWhere('description', '=',$reference)->orWhere('amount','=',$amount)->where('company_id', $request->company_id) ->where('name_bank', $request->name_bank)->orderBy('created_at', 'DESC')->get();

        
    }
    /**
     * Search AdvancedSearchadmin.
     *
     * @param Request $request
     * @return bodyResponseRequest $analyst
     */
    public function searchadmin(Request $request)
    {
            $primaryKey = 'id';

    
            if( (isset($request['elements'])) || (isset($request['name_bank'])) || (isset($request['company_id']))){
              if((isset($request['name_bank'])) && (isset($request['elements'])) && (isset($request['company_id']))){
  
                
                if (empty($request['elements'])){ 
                    $query = "
                    SELECT 
                        `data_banks_transactions`.id,
                        `data_banks_transactions`.transaction_date,
                        `data_banks_transactions`.reference,
                        `data_banks_transactions`.amount,
                        `data_banks_transactions`.description
                        FROM data_banks_transactions
                        WHERE data_banks_transactions.company_id = {$request['company_id']} 
                        AND data_banks_transactions.name_bank = '{$request['name_bank']}'";

                   }else  {   $query = "
                   SELECT 
                       `data_banks_transactions`.id,
                       `data_banks_transactions`.transaction_date,
                       `data_banks_transactions`.reference,
                       `data_banks_transactions`.amount,  
                       `data_banks_transactions`.description
                       FROM data_banks_transactions
                       WHERE data_banks_transactions.company_id = {$request['company_id']} 
                       AND data_banks_transactions.name_bank = '{$request['name_bank']}' AND
                       (data_banks_transactions.reference like '%{$request['elements']}%'
                       OR data_banks_transactions.transaction_date like '%{$request['elements']}%'
                       OR data_banks_transactions.amount like '%{$request['elements']}%'
                       OR data_banks_transactions.description like '%{$request['elements']}%')"; }
                 
              
                  }elseif((isset($request['name_bank'])) && (isset($request['company_id']))){
                 
                  $query = "
                  SELECT 
                      `data_banks_transactions`.id,
                      `data_banks_transactions`.transaction_date,
                      `data_banks_transactions`.reference,
                      `data_banks_transactions`.amount,
                      `data_banks_transactions`.description
                      FROM data_banks_transactions
                      WHERE data_banks_transactions.company_id = {$request['company_id']} 
                      AND data_banks_transactions.name_bank = '{$request['name_bank']}'";
                     
                  }elseif((isset($request['elements'])) && (isset($request['company_id']))){
                  

                    if (empty($request['elements'])){ 
                        $query = "
                        SELECT 
                            `data_banks_transactions`.id,
                            `data_banks_transactions`.transaction_date,
                            `data_banks_transactions`.reference,
                            `data_banks_transactions`.amount,  
                            `data_banks_transactions`.description
                            FROM data_banks_transactions
                            WHERE data_banks_transactions.company_id = {$request['company_id']}";

                       }else  {   $query = "
                       SELECT 
                           `data_banks_transactions`.id,
                           `data_banks_transactions`.transaction_date,
                           `data_banks_transactions`.reference,
                           `data_banks_transactions`.amount,  
                           `data_banks_transactions`.description
                           FROM data_banks_transactions
                           WHERE data_banks_transactions.company_id = {$request['company_id']} 
                           AND 
                           (data_banks_transactions.reference like '%{$request['elements']}%'
                           OR data_banks_transactions.transaction_date like '%{$request['elements']}%'
                           OR data_banks_transactions.amount like '%{$request['elements']}%'
                           OR data_banks_transactions.description like '%{$request['elements']}%')"; }
               
             
                  
                }elseif((isset($request['elements'])) && (isset($request['name_bank']))){
                    if (empty($request['elements'])){ 
                        $query = "
                    SELECT 
                        `data_banks_transactions`.id,
                        `data_banks_transactions`.transaction_date,
                        `data_banks_transactions`.reference,
                        `data_banks_transactions`.amount,
                        `data_banks_transactions`.description
                        FROM data_banks_transactions
                        WHERE data_banks_transactions.name_bank = '{$request['name_bank']}'";

                       }else {$query = "
                        SELECT 
                            `data_banks_transactions`.id,
                            `data_banks_transactions`.transaction_date,
                            `data_banks_transactions`.reference,
                            `data_banks_transactions`.amount,  
                            `data_banks_transactions`.description
                            FROM data_banks_transactions
                            WHERE data_banks_transactions.name_bank = '{$request['name_bank']}' 
                            AND 
                            (data_banks_transactions.reference like '%{$request['elements']}%'
                            OR data_banks_transactions.transaction_date like '%{$request['elements']}%'
                            OR data_banks_transactions.amount like '%{$request['elements']}%'
                            OR data_banks_transactions.description like '%{$request['elements']}%')"; }
                  
             
                  }
                    elseif(isset($request['name_bank'])) {
                    $query = "
                    SELECT 
                        `data_banks_transactions`.id,
                        `data_banks_transactions`.transaction_date,
                        `data_banks_transactions`.reference,
                        `data_banks_transactions`.amount,
                        `data_banks_transactions`.description
                        FROM data_banks_transactions
                        WHERE data_banks_transactions.name_bank = '{$request['name_bank']}'";
                       
                    }elseif(isset($request['elements'])) {
                       if (empty($request['elements'])){ 
                        $query = "
                        SELECT 
                            `data_banks_transactions`.id,
                            `data_banks_transactions`.transaction_date,
                            `data_banks_transactions`.reference,
                            `data_banks_transactions`.amount,  
                            `data_banks_transactions`.description
                            FROM data_banks_transactions
                            WHERE data_banks_transactions.company_id = 0";

                       }else {$query = "
                        SELECT 
                            `data_banks_transactions`.id,
                            `data_banks_transactions`.transaction_date,
                            `data_banks_transactions`.reference,
                            `data_banks_transactions`.amount,  
                            `data_banks_transactions`.description
                            FROM data_banks_transactions
                            WHERE data_banks_transactions.reference like '%{$request['elements']}%'
                            OR data_banks_transactions.transaction_date like '%{$request['elements']}%'
                            OR data_banks_transactions.amount like '%{$request['elements']}%'
                            OR data_banks_transactions.description like '%{$request['elements']}%'"; }
                    
               
                    }
                    elseif(isset($request['company_id'])){
                       
                        $query = "
                        SELECT 
                            `data_banks_transactions`.id,
                            `data_banks_transactions`.transaction_date,
                            `data_banks_transactions`.reference,
                            `data_banks_transactions`.amount,  
                            `data_banks_transactions`.description
                            FROM data_banks_transactions
                            WHERE data_banks_transactions.company_id = {$request['company_id']}";
                   
                        }
                  else{
     
                      $query = "
                      SELECT 
                          `data_banks_transactions`.id,
                          `data_banks_transactions`.transaction_date,
                          `data_banks_transactions`.reference,
                          `data_banks_transactions`.amount,  
                          `data_banks_transactions`.description
                          FROM data_banks_transactions
                          WHERE data_banks_transactions.company_id = 0";
                  }
              
            }else{
              $query = "
                      SELECT 
                          `data_banks_transactions`.id,
                          `data_banks_transactions`.transaction_date,
                          `data_banks_transactions`.reference,
                          `data_banks_transactions`.amount,  
                          `data_banks_transactions`.description
                          FROM data_banks_transactions
                          WHERE data_banks_transactions.company_id = 0";
            }
          
           
          $columns = array(
              array( 'db' => 'id', 'dt' => 'id' ),
              array( 'db' => 'transaction_date', 'dt' => 'transaction_date' ),
              array( 'db' => 'reference', 'dt' => 'reference' ),
              array( 'db' => 'amount', 'dt' => 'amount' ),
              array( 'db' => 'description', 'dt' => 'description' ),
          ); 
          $data = ServerSide::simple( $request, $query, $primaryKey, $columns );
          return response()->json( $data, \Illuminate\Http\Response::HTTP_OK ); 


       
    }
}