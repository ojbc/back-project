<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;
use App\Repository\Company\CompanyRepo;
use App\Company;
use App\BankCompany;
use App\Bank;
use App\Utils\Enums\AuditOperation;
use App\Utils\Enums\EnumResponse;
use App\User;
/**
 * Class CompanyController.
 *
 * @package App\Http\Controllers
 * @author  <info@expertosdelaweb.net>
 */

class CompanyController extends Controller
{
    /**
     * Create a new construct instance.
     * @route rest-api-project\app\Repository\Company
     * @param  CompanyRepo $_companyRepo
     * @return void
     */
    public function __construct( CompanyRepo $_companyRepo )
    {
        $this->companyRepo = $_companyRepo;
    }
     /**
     * Save Company.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function save( Request $request )
    {
        try {
          
            if (empty($request['data']['id'])) {
            $name='Debe ingresar el nombre de la empresa.!';
            $ci='Debe ingresar la cedula del usuario.!';
            $password ='Debe ingresar la contraseña del usuario.!';
            $email ='Debe ingresar el correo del usuario.!';
            $names='Debe ingresar el nombre del usuario.!';
            $lastname='Debe ingresar el apellido del usuario.!';
           
            $company ='El nombre de la empresa ya existe.!';

            if(!isset($request['data']['name'])){
                return bodyResponseRequest( EnumResponse::FAILED, $name );
            }
               
            if(!isset($request['data']['user']['email'])){
                return bodyResponseRequest( EnumResponse::FAILED, $email );
            }
            if(!isset($request['data']['user']['profile']['ci'])){
                return bodyResponseRequest( EnumResponse::FAILED, $ci );
            }
            if(!isset($request['data']['password'])){
                return bodyResponseRequest( EnumResponse::FAILED, $password );
            }
            if(!isset($request['data']['user']['profile']['name'])){
                return bodyResponseRequest( EnumResponse::FAILED, $names );
            }
            if(!isset($request['data']['user']['profile']['lastname'])){
                return bodyResponseRequest( EnumResponse::FAILED, $lastname );
            }
           }
           
           $data = $this->companyRepo->SearchName( $request );
      
            if(isset($data)){
                return bodyResponseRequest( EnumResponse::FAILED, $company );
            }

                $data = $this->companyRepo->save( $request );
             return bodyResponseRequest( EnumResponse::SUCCESS, $data );

        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'CompanyController.save.catch' );
        }
    }
    /**
     * Alls Company.
     *
     * @param 
     * @return bodyResponseRequest $data
     */
    public function alls()
    {
        try {
        $data=Company::all();

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'CompanyController.all.catch' );
        }
    }
    /**
     * association Company.
     *
     * @param Request $request
     * @return bodyResponseRequest $data
     */
    public function association(Request $request)
    {
        try {
            $bank='Debe ingresar el banco .!';
            $company='Debe ingresar la empresa.!';
            if(!isset($request->bank)){
                return bodyResponseRequest( EnumResponse::FAILED, $bank );
            }
        
            if(!isset($request ->company)){
                return bodyResponseRequest( EnumResponse::FAILED, $company );
            }
            if (empty($request->id)) {
              
           $data = $this->companyRepo->verification( $request );
           
          $bank ='La empresa ya tiene asociado el banco.!';
         
            if(isset($data)){
                return bodyResponseRequest( EnumResponse::FAILED, $bank );
            }
            $data = $this->companyRepo->association( $request );
        }else{
            $data = $this->companyRepo->verificationExit( $request );
            if(isset($data)){
                return bodyResponseRequest( EnumResponse::FAILED, $bank );
            }
             $data = $this->companyRepo->updateAssociation($request);
        }
           return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'CompanyController.association.catch' );
        }
    }
     /**
     * allUsers Company.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function allUsers()
    {
        try {
            
            $data = Company::with(['user.profile' => function( $query ) {
                }])->orderBy('created_at', 'DESC')->get();
                
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'CompanyController.allUsers.catch' );
        }
    }
    /**
     * allassociation allBankCompany.
     *
     * @param 
     * @return $analyst
     */
    public function allassociation( )
    {
        try {
            
            $data = BankCompany::with(['banks', 'companies'=> function( $query ) {
                }])->orderBy('id', 'DESC')->get();
                
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'CompanyController.allUsers.catch' );
        }

        
    }
   /**
     * status Company.
     *
     * @param Request $request
     * @return bodyResponseRequest $request->status
     */
    public function status(Request $request)
    {
        try {
         
            $this->companyRepo->status(['status' => $request->status], $request->company);

           

            return bodyResponseRequest(EnumResponse::SUCCESS, $request->status);

        } catch (\Exception $e) {
            return bodyResponseRequest(EnumResponse::ERROR, $e, [], 'CompanyController.delete.catch');
        }
    }
    /**
     * status Company.
     *
     * @param Request $request
     * @return bodyResponseRequest $request->status
     */
    public function statusBank(Request $request)
    {
        try {

            $this->companyRepo->statusBank(['status' => $request->status], $request->company);



            return bodyResponseRequest(EnumResponse::SUCCESS, $request->status);

        } catch (\Exception $e) {
            return bodyResponseRequest(EnumResponse::ERROR, $e, [], 'CompanyController.delete.catch');
        }
    }

    
}
