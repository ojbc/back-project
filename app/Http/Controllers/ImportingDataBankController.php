<?php

namespace App\Http\Controllers;

use App\DataBankTransaction;
use App\DataTransactionDebit;
use App\Repository\DataBank\Collection\BankCollection;
use App\Repository\DataBank\DataBankRepo;
use App\Repository\DataBankTransaction\DataBankTransactionRepo;
use App\Utils\Enums\BankEnumData;
use App\Utils\Enums\EnumResponse;
use App\Utils\Enums\InventoryError;
use Illuminate\Support\Facades\DB;
use App\Utils\Enums\CommercialEnumData;
use Excel;
use App\Utils\Enums\MonthSofitasa;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Utils\Enums\AuditOperation;
use JWTAuth;



/**
 * Class ImportingDataBankController.
 *
 * @package App\Http\Controllers
 * @author  <info@expertosdelaweb.net>
 */
class ImportingDataBankController extends Controller
{
    /**
     * @var DataBankRepo $repoDataBank
     */
    private $dataBankRepo;

    /**
     * @var DataBankRepo $repoDataBank
     */
    private $dataBankTransactionRepo;

    /**
     * @var DataBankTransactionRepo $bankTransRepo
     */
    private $bankTransRepo;
    /**
     * Create a new construct instance.
     * @route rest-api-project\app\Repository\DataBank
     * @param  DataBankRepo $repoDataBank
     * @route rest-api-project\app\Repository\DataBankTransaction
     * @param DataBankTransaction $repoDataBankTransaction
     * @route rest-api-project\app\Repository\DataBankTransaction
     * @param DataBankTransactionRepo $repoBanksTrans
     * @return void
     */
    public function __construct(
        DataBankRepo $repoDataBank,
        DataBankTransaction $repoDataBankTransaction,
        DataBankTransactionRepo $repoBanksTrans, DataBankTransaction $model
    )
    {
        $this->dataBankRepo = $repoDataBank;
        $this->dataBankTransactionRepo = $repoDataBankTransaction;
        $this->bankTransRepo = $repoBanksTrans;

    }
    /**
     * Processing ImportingDataBank.
     *
     * @param Request $request
     * @return bodyResponseRequest $excel
     */
    public function processing(Request $request)
    {
        /*  $collection = collect([
              ['id' => 0, 'nombre' => 'Banesco', 'slug' => 'banesco'],
              ['id' => 1, 'nombre' => 'Bicentenario', 'slug' => 'bicentenario'],
              ['id' => 2, 'nombre' => 'Mercantil', 'slug' => 'mercantil'],
              ['id' => 3, 'nombre' => 'BBVA Provincial', 'slug' => 'provincial'],
              ['id' => 4, 'nombre' => 'Venezuela', 'slug' => 'venezuela'],
              ['id' => 5, 'nombre' => 'Sofitasa', 'slug' => 'sofitasa'],
              ['id' => 6, 'nombre' => '100% Banco', 'slug' => 'cien_por_ciento_banco'],
              ['id' => 7, 'nombre' => 'B.O.D.', 'slug' => 'bod'],
              ['id' => 8, 'nombre' => 'B.F.C.', 'slug' => 'bfc'],
              ['id' => 9, 'nombre' => 'B.N.C.', 'slug' => 'bnc'],
              ['id' => 10, 'nombre' => 'Caroní', 'slug' => 'caroni'],
              ['id' => 11, 'nombre' => 'Banco Activo', 'slug' => 'banco_activo'],
              ['id' => 12, 'nombre' => 'Exterior', 'slug' => 'exterior']
          ]);

          $file = $request->file('archive');
          //
          $bank = $collection->first(function ($item) use ($request) {
              return $item['id'] == $request->bank;
          });


          $storage = $file->storeAs(
              'data/' . $bank['slug'], uniqid() . $file->getClientOriginalName()
          );

          //return $storage;

          $excel = Excel::load('/storage/app/' . $storage, function ($reader) {

          })->get();

          return response()->json($excel);*/
    }
    /**
     * ProcessingDataBank ImportingDataBank.
     * Processing Import Data bank by Providers Bank Banesco.
     * @param Request $request
     * @return bodyResponseRequest $successfulRegistrations
     */
    public function processingDataBank(Request $request)
    {
        if ($request->file('archive')->isValid()) {
            $file = $request->file('archive');
            $user = \JWTAuth::parseToken()->toUser();
            $bank = $request->get('bank_description');
            $bank_id = $request->get('bank_id');
            $company_id = $request->get('company_id');
            $alm = [];
            $data =[];
            $cont= 0;
            $conts= 0;
            $cont1= 0;
            $cont2= 0;
            $cont3 = 0;
            $change= 0;
            $bankInformation = new BankCollection;

            if ($bank === BankEnumData::MERCANTIL) {

                $file = $request->file('archive');

                $lines = file($file);

                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $txt = $this->registerCommercialBankInformation($lines);

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::MERCANTIL
                ]);
                if (!empty($txt)) {
                    $resultdata = collect();

                    // recording data by excel
                    foreach ($txt as $key => $values) {
                      
                        $amount = $values['amount'];
                        $amount =str_replace('.', '', $amount);
                        $amount = number_format(
                            (double)str_replace(',', '.', $amount),
                            2,
                            '.',
                            ''
                        );

                        $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                            ->where('name_bank', (string) $bank)
                            ->where('transaction_date',$values['transaction_date'])
                            ->where('reference', (string) $values['reference'])
                            ->where('amount', (float) $amount)
                            ->where('company_id', $company_id)
                            ->first();
                        if(!isset($dataBanksTransactions)){
                            if( (float) $amount>0){
                                $cont = $cont +1;
                                $DataBankTransaction = new DataBankTransaction;
                                $DataBankTransaction->databank_id = (int) $successfulRegistrations->id;
                                $DataBankTransaction->reference = (string) $values['reference'];
                                $DataBankTransaction->name_bank = (string) $bank;
                                $DataBankTransaction->transaction_date = $values['transaction_date'];
                                $DataBankTransaction->amount = $amount;
                                $DataBankTransaction->description = $values['description'];
                                $DataBankTransaction->locked =  false;
                                $DataBankTransaction->status =  false;
                                $DataBankTransaction->company_id =$company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;
                            }

                        }
                        if( (float) $amount < 0.00){
                            $amount =str_replace('-', '', $amount);
                          
                            $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                ->where('name_bank', (string) $bank)
                                ->where('transaction_date',$values['transaction_date'])
                                ->where('reference', (string) $values['reference'])
                                ->where('amount', (float) $amount)
                                ->where('company_id', $company_id)
                                ->first();
                            if (!isset($dataTransactionDebit)){

                                $cont = $cont + 1;
                                $DataBankTransaction = new DataTransactionDebit;
                                $DataBankTransaction->databank_id = (int) $successfulRegistrations->id;
                                $DataBankTransaction->reference = (string) $values['reference'];
                                $DataBankTransaction->name_bank = (string) $bank;
                                $DataBankTransaction->transaction_date = $values['transaction_date'];
                                $DataBankTransaction->amount =  (float) $amount;
                                $DataBankTransaction->description = $values['description'];
                                $DataBankTransaction->locked =  false;
                                $DataBankTransaction->status =  false;
                                $DataBankTransaction->company_id =$company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;



                            }
                        }
                    }
                    $data =  [$alm];
                    return responseRequest( EnumResponse::SUCCESS, $data );

                }

            }
            elseif ($bank === BankEnumData::BANESCO) {
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);

                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::BANESCO
                ]);

                if (!empty($excelData)) {


                    foreach ($excelData as $key => $values) {
                        $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                            ->where('name_bank', (string) $bank)
                            ->where('transaction_date',$values->fecha)
                            ->where('reference', $values->referencia)
                            ->where('amount', $values->monto)
                            ->where('company_id', $company_id)
                            ->first();
                        $amount=$values->monto;
                        $amount =str_replace('-', '', $amount);
                        $amount =str_replace(',', '.', $amount);
                        $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                            ->where('name_bank', (string) $bank)
                            ->where('transaction_date',$values->fecha)
                            ->where('reference', $values->referencia)
                            ->where('amount',(float) $amount)
                            ->where('company_id', $company_id)
                            ->first();
                        if(!isset($dataBanksTransactions)){
                            if($values->monto>0){
                                $cont = $cont +1;
                                $DataBankTransaction = new DataBankTransaction;
                                $DataBankTransaction->databank_id = (int) $successfulRegistrations->id;
                                $DataBankTransaction->reference = (string)  $values->referencia;
                                $DataBankTransaction->name_bank = (string) $bank;
                                $DataBankTransaction->transaction_date = $values->fecha;
                                $DataBankTransaction->amount = (float)$amount;
                                $DataBankTransaction->description = $values->descripcion;
                                $DataBankTransaction->locked =  false;
                                $DataBankTransaction->status =  false;
                                $DataBankTransaction->company_id =$company_id;
                                $DataBankTransaction->save();

                                $alm [$cont] = $DataBankTransaction;

                            }elseif($values->monto<0){
                                if(!isset($dataTransactionDebit)) {


                                    $cont = $cont + 1;
                                    $DataBankTransaction = new DataTransactionDebit;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string)$values->referencia;
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values->fecha;
                                    $DataBankTransaction->amount = (float)$amount;
                                    $DataBankTransaction->description = $values->descripcion;
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status = false;
                                    $DataBankTransaction->company_id = $company_id;
                                    $DataBankTransaction->save();
                                  
                                    $alm [$cont] = $DataBankTransaction;


                                }
                            }

                        }
                    }
                    $data =  [$alm];
                   
                    return responseRequest( EnumResponse::SUCCESS, $data );

                }
            }
            elseif ($bank === BankEnumData::BICENTENARIO) {
                $bankInformation = [];
                $file = $request->file('archive');

                $lines = file($file);

                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();

                })->get();
                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::BICENTENARIO
                ]);

                if (!empty($excelData)) {
                    foreach ($excelData as $key => $values) {
                        if ($key >='11') {

                                if ($values[6]>0) {

                                    $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                        ->where('name_bank', (string)$bank)
                                        ->where('transaction_date', $values[2])
                                        ->where('reference', (string)$values[3])
                                        ->where('amount', $values[6])
                                        ->where('company_id', $company_id)
                                        ->first();
                                    if (!isset($dataBanksTransactions)) {
                                        if ($values[6] > 0) {
                                            $cont = $cont +1;
                                            $DataBankTransaction = new DataBankTransaction;
                                            $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                            $DataBankTransaction->reference = (string)$values[3];
                                            $DataBankTransaction->name_bank = (string)$bank;
                                            $DataBankTransaction->transaction_date = $values[2];
                                            $DataBankTransaction->amount = $values[6];
                                            $DataBankTransaction->description = $values[4];
                                            $DataBankTransaction->locked = false;
                                            $DataBankTransaction->status =  false;
                                            $DataBankTransaction->company_id =$company_id;
                                            $DataBankTransaction->save();
                                            $alm [$cont] = $DataBankTransaction;
                                        }

                                    }

                                }elseif($values[5]>0){

                                    $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                        ->where('name_bank', (string)$bank)
                                        ->where('transaction_date', $values[2])
                                        ->where('reference', (string)$values[3])
                                        ->where('amount', $values[5])
                                        ->where('company_id', $company_id)
                                        ->first();
                                    if(!isset($dataTransactionDebit)) {


                                        $cont = $cont + 1;
                                        $DataBankTransaction = new DataTransactionDebit;
                                        $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                        $DataBankTransaction->reference = (string)$values[3];
                                        $DataBankTransaction->name_bank = (string)$bank;
                                        $DataBankTransaction->transaction_date = $values[2];
                                        $DataBankTransaction->amount = $values[5];
                                        $DataBankTransaction->description = $values[4];
                                        $DataBankTransaction->locked = false;
                                        $DataBankTransaction->status =  false;
                                        $DataBankTransaction->company_id =$company_id;
                                        $DataBankTransaction->save();
                                        $alm [$cont] = $DataBankTransaction;


                                    }
                                }
                        }

                    }
                    $data =  [$alm];
                    return responseRequest( EnumResponse::SUCCESS, $data );

                }
            }
            elseif ($bank === BankEnumData::CARONI){
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);

                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');

                })->get();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::CARONI
                ]);
                foreach ($excelData as $key => $values) {
                  
                    if( (((string)$values['no._de_cheque'])!= '0') || ( ((string)$values->codigo) != 'WC')){

                        $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                            ->where('name_bank', (string)$bank)
                            ->where('transaction_date', $values->fecha)
                            ->where('reference', (string)$values['no._de_cheque'])
                            ->where('amount', $values->credito)
                            ->where('company_id', $company_id)
                            ->first();
                        if (!isset($dataBanksTransactions)) {
                            if ($values->credito > 0) {
                                $cont = $cont + 1;
                                $DataBankTransaction = new DataBankTransaction;
                                $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                $DataBankTransaction->reference = (string)$values['no._de_cheque'];
                                $DataBankTransaction->name_bank = (string)$bank;
                                $DataBankTransaction->transaction_date = $values->fecha;
                                $DataBankTransaction->amount = $values->credito;
                                $DataBankTransaction->description = $values->descripcion;
                                $DataBankTransaction->locked = false;
                                $DataBankTransaction->status = false;
                                $DataBankTransaction->company_id = $company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;

                            }
                        }


                    }
                    if((float)$values->debito > 0){

                        $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                            ->where('name_bank', (string)$bank)
                            ->where('transaction_date', $values->fecha)
                            ->where('reference', (string)$values['no._de_cheque'])
                            ->where('amount', (float) $values->debito)
                            ->where('company_id', $company_id)
                            ->first();
                        if (!isset( $dataTransactionDebit)) {

                            $cont = $cont + 1;
                            $DataBankTransaction = new DataTransactionDebit;
                            $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                            $DataBankTransaction->reference = (string)$values['no._de_cheque'];
                            $DataBankTransaction->name_bank = (string)$bank;
                            $DataBankTransaction->transaction_date = $values->fecha;
                            $DataBankTransaction->amount = (float) $values->debito;
                            $DataBankTransaction->description = $values->descripcion;
                            $DataBankTransaction->locked = false;
                            $DataBankTransaction->status = false;
                            $DataBankTransaction->company_id = $company_id;
                            $DataBankTransaction->save();
                            $alm [$cont] = $DataBankTransaction;


                        }
                    }
                        

                }
                $data =  [$alm];
                return responseRequest( EnumResponse::SUCCESS, $data );

            }
            elseif ($bank === BankEnumData::EXTERIOR){
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);
                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();

                })->get();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::EXTERIOR
                ]);

                foreach ($excelData as $key => $values) {
                    if($values[4] == '-'){
                        $amount = $values[3];

                        $amount =str_replace('.', '', $amount);
                        $amount = number_format(
                            (double)str_replace(',', '.', $amount),
                            2,
                            '.',
                            ''
                        );
                        $long=strlen($values[1]);
                        $pos = stripos($values[1], "/");
                        $date = Carbon::now();
                        if($long === 7){
                            if ($pos===1){
                                $day= substr($values[1], 0, 1);
                                $month = substr($values[1], 2, 2);
                                if(((int)$date->month)  < ((int)$month)){
                                    $dateYear = ($date->year) - 1;
                                } else {
                                    $dateYear = $date->year;
                                }
                                $dates = '0'.$day.'/'.$month.'/'.$dateYear;
                            }else{

                                $day= substr($values[1], 0, 2);
                                $month = substr($values[1], 3, 1);
                                if(((int)$date->month)  < ((int)$month)){
                                    $dateYear = ($date->year) - 1;
                                } else {
                                    $dateYear = $date->year;
                                }
                                $dates = $day.'/0'.$month.'/'.$dateYear;
                            }


                        } elseif($long === 6){

                            $day= substr($values[1], 0, 1);
                            $month = substr($values[1], 2, 1);
                            if(((int)$date->month)  < ((int)$month)){
                                $dateYear = ($date->year) - 1;
                            } else {
                                $dateYear = $date->year;
                            }
                            $dates = '0'.$day.'/0'.$month.'/'.$dateYear;

                        }else{
                            $day= substr($values[1], 0, 2);
                            $month = substr($values[1], 3, 2);
                            if(((int)$date->month)  < ((int)$month)){
                                $dateYear = ($date->year) - 1;
                            } else {
                                $dateYear = $date->year;
                            }
                            $dates = $day.'/'. $month.'/'.$dateYear;

                        }

                        $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                            ->where('name_bank', (string)$bank)
                            ->where('transaction_date', $dates)
                            ->where('reference', (string)$values[2])
                            ->where('amount',  $amount)
                            ->where('company_id', $company_id)
                            ->first();
                        if (!isset($dataBanksTransactions)) {
                            if ( $amount > 0) {
                                $cont = $cont +1;
                                $DataBankTransaction = new DataBankTransaction;
                                $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                $DataBankTransaction->reference = (string)$values[2];
                                $DataBankTransaction->name_bank = (string)$bank;
                                $DataBankTransaction->transaction_date = $dates;
                                $DataBankTransaction->amount =  $amount;
                                $DataBankTransaction->description = $values[0];
                                $DataBankTransaction->locked = false;
                                $DataBankTransaction->status = false;
                                $DataBankTransaction->company_id =$company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;
                            }

                        }
                    }
                    if($values[4] == '+'){
                        $amount = $values[3];

                        $amount =str_replace('.', '', $amount);
                        $amount = number_format(
                            (double)str_replace(',', '.', $amount),
                            2,
                            '.',
                            ''
                        );
                        $long=strlen($values[1]);
                        $pos = stripos($values[1], "/");
                        $date = Carbon::now();
                        if($long === 7){
                            if ($pos===1){
                                $day= substr($values[1], 0, 1);
                                $month = substr($values[1], 2, 2);
                                if(((int)$date->month)  < ((int)$month)){
                                    $dateYear = ($date->year) - 1;
                                } else {
                                    $dateYear = $date->year;
                                }
                                $dates = '0'.$day.'/'.$month.'/'.$dateYear;
                            }else{

                                $day= substr($values[1], 0, 2);
                                $month = substr($values[1], 3, 1);
                                if(((int)$date->month)  < ((int)$month)){
                                    $dateYear = ($date->year) - 1;
                                } else {
                                    $dateYear = $date->year;
                                }
                                $dates = $day.'/0'.$month.'/'.$dateYear;
                            }


                        } elseif($long === 6){

                            $day= substr($values[1], 0, 1);
                            $month = substr($values[1], 2, 1);
                            if(((int)$date->month)  < ((int)$month)){
                                $dateYear = ($date->year) - 1;
                            } else {
                                $dateYear = $date->year;
                            }
                            $dates = '0'.$day.'/0'.$month.'/'.$dateYear;

                        }else{
                            $day= substr($values[1], 0, 2);
                            $month = substr($values[1], 3, 2);
                            if(((int)$date->month)  < ((int)$month)){
                                $dateYear = ($date->year) - 1;
                            } else {
                                $dateYear = $date->year;
                            }
                            $dates = $day.'/'. $month.'/'.$dateYear;

                        }

                        $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                            ->where('name_bank', (string)$bank)
                            ->where('transaction_date', $dates)
                            ->where('reference', (string)$values[2])
                            ->where('amount',  $amount)
                            ->where('company_id', $company_id)
                            ->first();
                        if (!isset($dataTransactionDebit)) {
                            if ( $amount > 0) {
                                $cont = $cont +1;
                                $DataBankTransaction = new DataTransactionDebit;
                                $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                $DataBankTransaction->reference = (string)$values[2];
                                $DataBankTransaction->name_bank = (string)$bank;
                                $DataBankTransaction->transaction_date = $dates;
                                $DataBankTransaction->amount =  $amount;
                                $DataBankTransaction->description = $values[0];
                                $DataBankTransaction->locked = false;
                                $DataBankTransaction->status = false;
                                $DataBankTransaction->company_id =$company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;
                            }

                        }
                    }
                }
                $data =  [$alm];
                return responseRequest( EnumResponse::SUCCESS, $data );

            }
            elseif ($bank === BankEnumData::PROVINCIAL){
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);

                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->get();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::PROVINCIAL
                ]);
                $long =count($excelData);
                $max = $long -1;
                foreach ($excelData as $key => $values) {
                    if (($key >= '20') && ($key<$max)){
                        if($values[5]>0){ $max = $long -1;
                            $caract = 0;
                            $reference=(string) $values[3];
                            $reference=ltrim( $reference, "'");
                            $long=strlen($reference);
                            if($long>1){
                            $reference = preg_replace('/^0+/', '', $reference);
                            }else{
                                $reference= $reference;
                            }
                            $amount=$values[5];
                            $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', $values[1])
                                ->where('reference', (string)$reference)
                                ->where('amount', $amount)
                                ->where('company_id', $company_id)
                                ->first();
                            if (!isset($dataBanksTransactions)) {
                                $cont = $cont +1;
                                $rest1 = substr($values[4], 3, 1);
                            
                                
                                $findCase = 'W';
                                $secondCase = 'R';
                                $posFind = strpos($rest1, $findCase);
                                
                                $posSecond = strpos($rest1, $secondCase);
                                 if($posFind !== false){
                                    $result = substr($values[4], 5, 11);
                                    $result = str_replace(' ', '', $result);
                                    $index = substr($result,0, 1);
                                    $top = substr($result, 1, 10);
                                    $DNI=preg_replace('/^0+/', '', $top);
                                    $DNI=($index . $DNI);
                                  
                                 }elseif($posSecond !== false){
                                    $result= substr($values[4], 12, 10);
                                    $result = str_replace(' ', '', $result);
                                    $index = substr($result, 0, 1);
                                    $top = substr($result, 1, 10);
                                    $DNI=preg_replace('/^0+/', '', $top);
                                    $DNI=($index . $DNI);
                                   
                                 }else{
                                    
                                    $DNI = substr($values[4], 5, 40);
                                   
                                 }
                                $DataBankTransaction = new DataBankTransaction;
                                $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                $DataBankTransaction->reference = (string)$reference;
                                $DataBankTransaction->name_bank = (string)$bank;
                                $DataBankTransaction->transaction_date = $values[1];
                                $DataBankTransaction->amount = $amount;
                                $DataBankTransaction->description = $values[4];
                                $DataBankTransaction->DNI = $DNI;
                                $DataBankTransaction->locked = false;
                                $DataBankTransaction->status =  false;
                                $DataBankTransaction->company_id =$company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;

                            }

                        }
                        if((float) $values[5]<0){ $max = $long -1;
                            $caract = 0;
                            $reference=(string) $values[3];
                            $reference=ltrim( $reference, "'");
                            $long=strlen($reference);
                            if($long>1){
                            $reference = preg_replace('/^0+/', '', $reference);
                            }else{
                                $reference= $reference;
                            }
                            $amount =str_replace('-', '', $values[5]);
                            $amount = str_replace(',', '.', $amount);
                            $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', $values[1])
                                ->where('reference', (string)$reference)
                                ->where('amount', (float) $amount)
                                ->where('company_id', $company_id)
                                ->first();
                            if (!isset($dataTransactionDebit )) {
                                $cont = $cont +1;
                                $rest1 = substr($values[4], 3, 1);


                                $findCase = 'W';
                                $secondCase = 'R';
                                $posFind = strpos($rest1, $findCase);

                                $posSecond = strpos($rest1, $secondCase);
                                if($posFind !== false){
                                    $result = substr($values[4], 5, 11);
                                    $result = str_replace(' ', '', $result);
                                    $index = substr($result,0, 1);
                                    $top = substr($result, 1, 10);
                                    $DNI=preg_replace('/^0+/', '', $top);
                                    $DNI=($index . $DNI);

                                }elseif($posSecond !== false){
                                    $result= substr($values[4], 12, 10);
                                    $result = str_replace(' ', '', $result);
                                    $index = substr($result, 0, 1);
                                    $top = substr($result, 1, 10);
                                    $DNI=preg_replace('/^0+/', '', $top);
                                    $DNI=($index . $DNI);

                                }else{

                                    $DNI = substr($values[4], 5, 40);

                                }
                                $DataBankTransaction = new DataTransactionDebit;
                                $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                $DataBankTransaction->reference = (string)$reference;
                                $DataBankTransaction->name_bank = (string)$bank;
                                $DataBankTransaction->transaction_date = $values[1];
                                $DataBankTransaction->amount = (float)$amount;
                                $DataBankTransaction->description = $values[4];
                                $DataBankTransaction->DNI = $DNI;
                                $DataBankTransaction->locked = false;
                                $DataBankTransaction->status =  false;
                                $DataBankTransaction->company_id =$company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;

                            }

                        }
                    }



                }
                $data =  [$alm];
                return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank === BankEnumData::BANCOACTIVO){
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);

                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::BANCOACTIVO
                ]);
                foreach ($excelData as $key => $values) {
                    if ($key >= '2') {
                       
                        $comilla= str_replace('"', '', $values[6]);
                          $amount= str_replace('.', '', $comilla);
                            if ($values[4] === 'WC ') {
                                if ( ((float)$amount)>0) {
                                    $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                        ->where('name_bank', (string)$bank)
                                        ->where('transaction_date', $values[0])
                                        ->where('reference', (string)$values[3])
                                        ->where('amount', (float)$amount)
                                        ->where('company_id', $company_id)
                                        ->first();
                                    if (!isset($dataBanksTransactions)) {
                                        $cont = $cont +1;
                                        $DataBankTransaction = new DataBankTransaction;
                                        $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                        $DataBankTransaction->reference = (string)$values[3];
                                        $DataBankTransaction->name_bank = (string)$bank;
                                        $DataBankTransaction->transaction_date = $values[0];
                                        $DataBankTransaction->amount = (float) $amount;
                                        $DataBankTransaction->description = $values[5];
                                        $DataBankTransaction->locked = false;
                                        $DataBankTransaction->status =  false;
                                        $DataBankTransaction->company_id =$company_id;
                                        $DataBankTransaction->save();
                                        $alm [$cont] = $DataBankTransaction;
                                    }
                                }
                            }elseif ($values[5] === 'DEBITO') {
                                if ( ((float)$amount)>0) {
                                    $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                        ->where('name_bank', (string)$bank)
                                        ->where('transaction_date', $values[0])
                                        ->where('reference', (string)$values[3])
                                        ->where('amount',(float) $amount)
                                        ->where('company_id', $company_id)
                                        ->first();
                                    if(!isset($dataTransactionDebit)) {


                                        $cont = $cont + 1;
                                        $DataBankTransaction = new DataTransactionDebit;
                                        $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                        $DataBankTransaction->reference = (string)$values[3];
                                        $DataBankTransaction->name_bank = (string)$bank;
                                        $DataBankTransaction->transaction_date = $values[0];
                                        $DataBankTransaction->amount = (float) $amount;
                                        $DataBankTransaction->description = $values[5];
                                        $DataBankTransaction->locked = false;
                                        $DataBankTransaction->status =  false;
                                        $DataBankTransaction->company_id =$company_id;
                                        $DataBankTransaction->save();
                                        $alm [$cont] = $DataBankTransaction;
                                    }
                                }
                            }



                    }
                }
                $data =  [$alm];
                return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank ===BankEnumData::BFC){

                $bankInformation = [];
                $file = $request->file('archive');

                $lines = file($file);

                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->get();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::BFC
                ]);
                foreach ($excelData as $key => $values) {

                        if ($key > 0) {

                            $reference=(string)$values[2];
                            
                            $long=strlen($reference);
                            if($long>1){
                            $reference = preg_replace('/^0+/', '', $reference);
                            }else{
                                $reference= $reference;
                            }
                            $amount= str_replace('.', '', $values[4]);

                          if ($amount>0) {
                                $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', $values[1])
                                    ->where('reference', (string) $reference)
                                    ->where('amount',(float) $amount)
                                    ->where('company_id', $company_id)
                                    ->first();

                                if (!isset($dataBanksTransactions)) {
                                    $cont = $cont +1;
                                    $DataBankTransaction = new DataBankTransaction;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string) $reference;
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values[1];
                                    $DataBankTransaction->amount = (float) $amount;
                                    $DataBankTransaction->description = $values[4];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status =  false;
                                    $DataBankTransaction->company_id =$company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;

                                }
                            }elseif($amount<0){
                                $amount =str_replace('-', '', $amount);
                        
                                $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', $values[1])
                                ->where('reference', (string) $reference)
                                ->where('amount',(float) $amount)
                                ->where('company_id', $company_id)
                                    ->first();
                                if(!isset($dataTransactionDebit)) {


                                    $cont = $cont + 1;
                                    $DataBankTransaction = new DataTransactionDebit;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string) $reference;
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values[1];
                                    $DataBankTransaction->amount = (float) $amount;
                                    $DataBankTransaction->description = $values[4];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status =  false;
                                    $DataBankTransaction->company_id =$company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;


                                }
                            }

                            

                       }
                }
                $data =  [$alm];
                return responseRequest( EnumResponse::SUCCESS, $data );

            }
            elseif ($bank ===BankEnumData::BOD){

                $bankInformation = [];
                $file = $request->file('archive');

                $lines = file($file);

                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::BOD
                ]);

                foreach ($excelData as $key => $values) {
                        if ($key >= 4) {
                            $amount=$values[4];
                            if (($amount>0) && ($values[2]>=0)) {

                                $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', $values[0])
                                    ->where('reference', (string)$values[2])
                                    ->where('amount',(float) $amount)
                                    ->where('company_id', $company_id)
                                    ->first();
                                if (!isset($dataBanksTransactions)) {
                                    $cont = $cont +1;
                                    $DataBankTransaction = new DataBankTransaction;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string) $values[2];
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values[0];
                                    $DataBankTransaction->amount = (float) $amount;
                                    $DataBankTransaction->description = $values[3];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status =  false;
                                    $DataBankTransaction->company_id =$company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;

                                }
                            }
                            elseif($values[4]<0){
                                $amount =str_replace('-', '', $values[4]);

                                $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', $values[0])
                                    ->where('reference', (string)$values[2])
                                    ->where('amount',(float) $amount)
                                    ->where('company_id', $company_id)
                                    ->first();
                                if(!isset($dataTransactionDebit)) {


                                    $cont = $cont + 1;
                                    $DataBankTransaction = new DataTransactionDebit;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string) $values[2];
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values[0];
                                    $DataBankTransaction->amount = (float) $amount;
                                    $DataBankTransaction->description = $values[3];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status =  false;
                                    $DataBankTransaction->company_id =$company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;


                                }
                            }
                        }
                }
                $data =  [$alm];
                return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank ===BankEnumData::VENEZUELA){

                $bankInformation = [];
                $file = $request->file('archive');

                $lines = file($file);

                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::VENEZUELA
                ]);
                foreach ($excelData as $key => $values) {

                    $long =count($values);

                        $long=strlen($values->fecha);
                        if($long === 7){
                            $day= substr($values->fecha, 0, 1);
                            $month = substr($values->fecha, 1, 2);
                            $year = substr($values->fecha, 3, 4);
                            $date = '0'.$day.'/'.$month.'/'.$year;

                        }else{
                            $day = substr($values->fecha, 0, 2);
                            $month = substr($values->fecha, 2, 2);
                            $year = substr($values->fecha, 4, 4);
                            $date = $day.'/'.$month.'/'.$year;
                        }

                        if ($values->abono>0) {
                            $long=strlen($values->abono);
                            $to= $long - 2;

                            $con1 = substr($values->abono, 0, $to);
                            $con2 = substr($values->abono, $to, 2);
                            $mont = $con1.'.'.$con2;
                            $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', $date)
                                ->where('reference', (string)$values->referencia)
                                ->where('amount', (float) $mont)
                                ->where('company_id',  $company_id)
                                ->first();
                            if (!isset($dataBanksTransactions)) {
                                $cont = $cont +1;
                                $DataBankTransaction = new DataBankTransaction;
                                $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                $DataBankTransaction->reference = (string)$values->referencia;
                                $DataBankTransaction->name_bank = (string)$bank;
                                $DataBankTransaction->transaction_date = $date;
                                $DataBankTransaction->amount = (float)$mont;
                                $DataBankTransaction->description = $values->concepto;
                                $DataBankTransaction->locked = false;
                                $DataBankTransaction->status =  false;
                                $DataBankTransaction->company_id =$company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;

                            }
                        }elseif($values->cargo>0){
                            $long=strlen($values->cargo);
                            $to= $long - 2;

                            $con1 = substr($values->cargo, 0, $to);
                            $con2 = substr($values->cargo, $to, 2);
                            $mont = $con1.'.'.$con2;
                            $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', $date)
                                ->where('reference', (string)$values->referencia)
                                ->where('amount', (float) $mont)
                                ->where('company_id',  $company_id)
                                ->first();
                            if (!isset($dataTransactionDebit)) {
                                $cont = $cont +1;
                                $DataBankTransaction = new DataTransactionDebit;
                                $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                $DataBankTransaction->reference = (string)$values->referencia;
                                $DataBankTransaction->name_bank = (string)$bank;
                                $DataBankTransaction->transaction_date = $date;
                                $DataBankTransaction->amount = (float)$mont;
                                $DataBankTransaction->description = $values->concepto;
                                $DataBankTransaction->locked = false;
                                $DataBankTransaction->status =  false;
                                $DataBankTransaction->company_id =$company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;

                            }
                        }
                    }

                $data =  [$alm];

                return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank === BankEnumData::CIEN_BANCO){
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);

                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::CIEN_BANCO
                ]);
                foreach ($excelData as $key => $values) {

                        if ($key >= 1) {
                            if ( $values[3]>0) {
                                $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', $values[0])
                                    ->where('reference', (string)$values[1])
                                    ->where('amount', $values[3])
                                    ->where('company_id', $company_id)
                                    ->first();
                                if (!isset($dataBanksTransactions)) {
                                    $cont = $cont +1;
                                    $DataBankTransaction = new DataBankTransaction;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string)$values[1];
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values[0];
                                    $DataBankTransaction->amount = $values[3];
                                    $DataBankTransaction->description = $values[2];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status =  false;
                                    $DataBankTransaction->company_id =$company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;
                                }
                            }elseif($values[3]<0){
                                $amount =str_replace('-', '', $values[3]);
                        
                                $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', $values[0])
                                ->where('reference', (string)$values[1])
                                ->where('amount',(float) $amount)
                                ->where('company_id', $company_id)
                                    ->first();
                                if(!isset($dataTransactionDebit)) {


                                    $cont = $cont + 1;
                                    $DataBankTransaction = new DataTransactionDebit;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string)$values[1];
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values[0];
                                    $DataBankTransaction->amount =  $amount;
                                    $DataBankTransaction->description = $values[2];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status =  false;
                                    $DataBankTransaction->company_id =$company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;


                                }
                            }
                        }
                }
                $data =  [$alm];
                return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank === BankEnumData::BNC){
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);

                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::BNC
                ]);
                foreach ($excelData as $key => $values) {
                        if ($key >= 7) {
                            if ( (float) $values[7]>0) {
                                $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', $values[0])
                                    ->where('reference', (string)$values[1])
                                    ->where('amount', (float)$values[7])
                                    ->where('company_id', $company_id)
                                    ->first();
                                if (!isset($dataBanksTransactions)) {
                                    $cont = $cont +1;
                                    $DataBankTransaction = new DataBankTransaction;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string)$values[1];
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values[0];
                                    $DataBankTransaction->amount =  (float)$values[7];
                                    $DataBankTransaction->description = $values[5];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status =  false;
                                    $DataBankTransaction->company_id =$company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;
                                }
                            }
                            if( (float) $values[6]>0){
                                $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', $values[0])
                                ->where('reference', (string)$values[1])
                                ->where('amount', (float)$values[6])
                                ->where('company_id', $company_id)
                                    ->first();
                                    if (!isset($dataTransactionDebit)){
     
                                         $cont = $cont + 1;
                                         $DataBankTransaction = new DataTransactionDebit;
                                         $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                         $DataBankTransaction->reference = (string)$values[1];
                                         $DataBankTransaction->name_bank = (string)$bank;
                                         $DataBankTransaction->transaction_date = $values[0];
                                         $DataBankTransaction->amount = (float) $values[6];
                                         $DataBankTransaction->description = $values[5];
                                         $DataBankTransaction->locked = false;
                                         $DataBankTransaction->status =  false;
                                         $DataBankTransaction->company_id =$company_id;
                                         $DataBankTransaction->save();
                                         $alm [$cont] = $DataBankTransaction;
     
     
                                
                                 }
                            }
                        }
                }
                $data =  [$alm];
                return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank === BankEnumData::PLAZA){
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);

                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::PLAZA
                ]);
                foreach ($excelData as $key => $values) {


                        if ( (float) $values[4]>0) {
                            $amount = $values[4];
                            $reference = (string) $values[1];
                            $long=strlen($reference);
                            if($long>1){
                            $reference = preg_replace('/^0+/', '', $reference);
                            }else{
                                $reference= $reference;
                            }
                            $amount =str_replace('.', '', $amount);
                            $amount = number_format(
                                (double)str_replace(',', '.', $amount),
                                2,
                                '.',
                                ''
                            );
                            $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', $values[0])
                                ->where('reference', (string)$reference)
                                ->where('amount', (float)$amount)
                                ->where('company_id', $company_id)
                                ->first();
                            if (!isset($dataBanksTransactions)) {
                                $cont = $cont +1;
                                $DataBankTransaction = new DataBankTransaction;
                                $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                $DataBankTransaction->reference = (string)$reference;
                                $DataBankTransaction->name_bank = (string)$bank;
                                $DataBankTransaction->transaction_date = $values[0];
                                $DataBankTransaction->amount =  (float)$amount;
                                $DataBankTransaction->description = $values[2];
                                $DataBankTransaction->locked = false;
                                $DataBankTransaction->status =  false;
                                $DataBankTransaction->company_id =$company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;
                            }
                        }
                        if( (float) $values[3]>0){
                            $reference = (string) $values[1];
                            $long=strlen($reference);
                            if($long>1){
                            $reference = preg_replace('/^0+/', '', $reference);
                            }else{
                                $reference= $reference;
                            }
                            $amounts = $values[3];
                            $amounts =str_replace('.', '', $amounts);
                            $amounts = number_format(
                                (double)str_replace(',', '.', $amounts),
                                2,
                                '.',
                                ''
                            );
                            $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', $values[0])
                                ->where('reference', (string)$reference)
                                ->where('amount', (float)$amounts)
                                ->where('company_id', $company_id)
                                ->first();
                            if (!isset($dataTransactionDebit)){

                                $cont = $cont + 1;
                                $DataBankTransaction = new DataTransactionDebit;
                                $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                $DataBankTransaction->reference = (string)$reference;
                                $DataBankTransaction->name_bank = (string)$bank;
                                $DataBankTransaction->transaction_date = $values[0];
                                $DataBankTransaction->amount = (float) $amounts;
                                $DataBankTransaction->description = $values[2];
                                $DataBankTransaction->locked = false;
                                $DataBankTransaction->status =  false;
                                $DataBankTransaction->company_id =$company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;



                            }
                        }

                }
                $data =  [$alm];
                return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank === BankEnumData::SOFITASA) {
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);
                $months =0;
                $change=0;
                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::SOFITASA
                ]);

                if (!empty($excelData)) {

                        $longArray =count($excelData);
                        $max =  $longArray -1;
                    $dayArray=[];
                    $dayPrevious=[];
                        foreach ($excelData as $m => $value) {
                            if ($m === 3) {
                                $long=strlen($value[0]);
                                $ini =$long - 4;
                                $resultado = substr($value[0], $ini , $long);
                                $resultado = str_replace(' ', '', $resultado);
                                $resultado = str_replace(' ', '', $resultado);
                                foreach (MonthSofitasa::$list_months_sofitasa as $key => $month) {
                                    $existenceOfMonth = strpos($month, $resultado);
                                    if ($existenceOfMonth !== false) {
                                        $month = $key + 1;
                                        $months=$month;

                                    }
                                }
                            }
                        }

                    foreach ($excelData as $k =>  $items) {
                        if (($k >= '4') && ($k<$max)) {
                            $conts = $conts +1;
                            $dayArray[$conts]= $items[0];
                        }
                        if(($k >= '4') && ($k<$max))
                        {
                            $cont1 = $cont1 +1;
                            $dayPrevious[$cont1]= $items[0];


                        }

                    }
                    $countDay=count($dayArray);
                    for ($i = 1; $i < $countDay; $i++) {

                        if($dayArray[$i] < $dayArray[$i+1]){

                            $change=1;
                        }
                    }
                    if($change==1){
                        $months= (int)$months;
                    }
                        foreach ($excelData as $k =>  $item) {
                            if (($k >= '4') && ($k<$max)){
                                $monthTest=1;
                                    $date = Carbon::now();

                                    $day = $item[0];
                                    $longDay=strlen($day);
                                    $longMonth=strlen($months);
                                if(($k >= '4') && ($k<$max))
                                {
                                    $cont3 = $cont3 +1;


                                    if($dayPrevious[$cont3 +1]>0){
                                        if($dayPrevious[$cont3] > $dayPrevious[$cont3 +1]){

                                            $cont2 = $cont2 +1;

                                            if( $cont2 === 1){ $months= (int)$months + 1;}


                                        }else{


                                        }
                                    }




                                }
                                if(((int)$date->month)  < ((int)$months)){
                                    $dateYear = ($date->year) - 1;
                                } else {
                                    $dateYear = $date->year;
                                }
                                if(($longDay>1)&&($longMonth>1)){
                                    if(($months == 12) || ($date->month == 1)){
                                        $yearold =(($date->year) -1);
                                        $dates = $item[0] . '/' .$months . '/' .  $dateYear;

                                    }else{
                                        $dates = $item[0] . '/' .$months . '/' . $dateYear;

                                    }


                                } elseif (($longDay == 1)&&($longMonth>1)){
                                    if(($months == 12) ||  ($date->month == 1)){
                                        $yearold =(($date->year) - 1);
                                        $dates = '0'.$item[0] . '/' . $months . '/' . $dateYear;
                                    }else{
                                        $dates = '0'.$item[0] . '/' . $months . '/' .  $dateYear;
                                    }
                                } elseif (($longDay == 1)&&($longMonth == 1)){

                                    $dates = '0'.$item[0] . '/' .'0'.$months . '/' .$dateYear;

                                }elseif (($longDay>1)&&($longMonth == 1)){

                                    $dates = $item[0] . '/' .'0'.$months . '/' .$dateYear;

                                }else{
                                    $dates = $item[0]. '/' .'0'.$months . '/' . $dateYear;
                                }
                                    $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                        ->where('name_bank', (string) $bank)
                                        ->where('transaction_date', $dates)
                                        ->where('reference', (string) $item[1])
                                        ->where('amount', $item[4])
                                        ->where('company_id', $company_id)
                                        ->first();
                                      
                                
                                        $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                        ->where('name_bank', (string) $bank)
                                        ->where('transaction_date', $dates)
                                        ->where('reference', (string) $item[1])
                                        ->where('amount',(float)$item[3])
                                        ->where('company_id', $company_id)
                                            ->first();
                                          
                                    if(!isset($dataBanksTransactions)){
                                        if($item[4]>0){
                                            $cont = $cont +1;
                                            $DataBankTransaction = new DataBankTransaction;
                                            $DataBankTransaction->databank_id = (int) $successfulRegistrations->id;
                                            $DataBankTransaction->reference = (string)   $item[1];
                                            $DataBankTransaction->name_bank = (string) $bank;
                                            $DataBankTransaction->transaction_date = $dates;
                                            $DataBankTransaction->amount = $item[4];
                                            $DataBankTransaction->description = $item[2];
                                            $DataBankTransaction->locked =  false;
                                            $DataBankTransaction->status =  false;
                                            $DataBankTransaction->company_id =$company_id;
                                            $DataBankTransaction->save();
                                            $alm[$cont]  = $DataBankTransaction;
                                        }

                                     }
                                     if(!isset($dataTransactionDebit)){
                                        if((float) $item[3]>0) {
        
        
                                            $cont = $cont + 1;
                                            $DataBankTransaction = new DataTransactionDebit;
                                            $DataBankTransaction->databank_id = (int) $successfulRegistrations->id;
                                            $DataBankTransaction->reference = (string)   $item[1];
                                            $DataBankTransaction->name_bank = (string) $bank;
                                            $DataBankTransaction->transaction_date = $dates;
                                            $DataBankTransaction->amount =  (float)$item[3];
                                            $DataBankTransaction->description = $item[2];
                                            $DataBankTransaction->locked = false;
                                            $DataBankTransaction->status =  false;
                                            $DataBankTransaction->company_id =$company_id;
                                            $DataBankTransaction->save();
                                            $alm [$cont] = $DataBankTransaction;
        
        
                                        }
                                    }
                                
                            }
                        }
                        $data = [$alm];
                        return responseRequest( EnumResponse::SUCCESS, $data );

                }

            }
            elseif ($bank === BankEnumData::TESORO) {
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);
                $months =0;
                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::TESORO
                ]);
                    foreach ($excelData as $key => $values) {
                    
                        
                        if($key >=  2){
                           
                            $reference=(string)$values[2];
                            
                            
                            
                        if((float) $values[4]>0){
                            $amount = $values[4];
                            $long=strlen($reference);
                            if($long>1){
                                $reference = preg_replace('/^0+/', '', $reference);
                                if( (string) $reference[1]=== 'J'){
                                    $reference = preg_replace('/^1+/', '', $reference);
                                }elseif(($long>3) && ((string) $reference[0]=== 'R') && ((string) $reference[1]=== 'E') && ((string) $reference[2]=== 'F')){

                                $reference= $reference;
                                $long=strlen($reference);
                                $reference = substr($reference, 3, $long);
                                $reference = preg_replace('/^0+/', '', $reference);
                                 }else{
                                $reference= $reference;
                                $long=strlen($reference);
                                $leng=$long - 3;
                                $reference = substr($reference, 0, $leng);
                            }

                            }
                           
                                $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', $values[0])
                                    ->where('reference', (string)$reference)
                                    ->where('amount', (float)$amount)
                                    ->where('company_id', $company_id)
                                    ->first();
                                if (!isset($dataBanksTransactions)) {
                                    $cont = $cont +1;
                                    $DataBankTransaction = new DataBankTransaction;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string)$reference;
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values[0];
                                    $DataBankTransaction->amount =  (float)$amount;
                                    $DataBankTransaction->description = $values[1];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status =  false;
                                    $DataBankTransaction->company_id =$company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;
                                }
                            }
                            if( (float) $values[3]>0){
                                 $amount = $values[3];
                                 $long=strlen($reference);
                                if($long>1){
                                    $reference = preg_replace('/^0+/', '', $reference);
                                    if( (string) $reference[1]=== 'J'){
                                        $reference = preg_replace('/^1+/', '', $reference);
                                    }elseif(($long>3) && ((string) $reference[0]=== 'R') && ((string) $reference[1]=== 'E') && ((string) $reference[2]=== 'F')){

                                        $reference= $reference;
                                        $long=strlen($reference);
                                        $reference = substr($reference, 3, $long);
                                        $reference = preg_replace('/^0+/', '', $reference);
                                    }else{
                                        $reference= $reference;
                                        $long=strlen($reference);
                                        $leng=$long - 3;
                                        $reference = substr($reference, 0, $leng);
                                    }

                                }

                                $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', $values[0])
                                ->where('reference', (string)$reference)
                                ->where('amount', (float)$amount)
                                ->where('company_id', $company_id)
                                    ->first();
                                    if (!isset($dataTransactionDebit)){

                                            $cont = $cont + 1;
                                            $DataBankTransaction = new DataTransactionDebit;
                                            $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                            $DataBankTransaction->reference = (string)$reference;
                                            $DataBankTransaction->name_bank = (string)$bank;
                                            $DataBankTransaction->transaction_date = $values[0];
                                            $DataBankTransaction->amount = (float) $amount;
                                            $DataBankTransaction->description = $values[1];
                                            $DataBankTransaction->locked = false;
                                            $DataBankTransaction->status =  false;
                                            $DataBankTransaction->company_id =$company_id;
                                            $DataBankTransaction->save();
                                            $alm [$cont] = $DataBankTransaction;


                                
                                    }
                            }
                        }
                }
               $data =  [$alm];
               return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank === BankEnumData::BANCARIBE) {
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);
                $months =0;
                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::BANCARIBE
                ]);
                    foreach ($excelData as $key => $values) {
                        $longArray =count($excelData);
                        $max =  $longArray -1;
                        
                       if(($key >=  1) && ($key<$max)){
                        

                        if($values[4] ==="+"){
                          
                               $reference=$values[1];
                           
                           
                                $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', (string)$values[0])
                                    ->where('reference', (string)$reference)
                                    ->where('amount', (float)$values[5])
                                    ->where('company_id', $company_id)
                                    ->first();
                                if (!isset($dataBanksTransactions)) {
                                    $cont = $cont +1;
                                    $DataBankTransaction = new DataBankTransaction;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string)$reference;
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = (string)$values[0];
                                    $DataBankTransaction->amount =  (float) $values[5];
                                    $DataBankTransaction->description = (string)$values[3];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status =  false;
                                    $DataBankTransaction->company_id =$company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;
                                }
                            }
                            if( $values[4]=== "-"){
                                
                           
                               $reference=$values[1];
                         
                                $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', (string)$values[0])
                                ->where('reference', (string)$reference)
                                ->where('amount', (float) $values[5])
                                ->where('company_id', $company_id)
                                    ->first();
                                    if (!isset($dataTransactionDebit)){

                                            $cont = $cont + 1;
                                            $DataBankTransaction = new DataTransactionDebit;
                                            $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                            $DataBankTransaction->reference = (string)$reference;
                                            $DataBankTransaction->name_bank = (string)$bank;
                                            $DataBankTransaction->transaction_date = (string)$values[0];
                                            $DataBankTransaction->amount = (float) $values[5];
                                            $DataBankTransaction->description = (string)$values[3];
                                            $DataBankTransaction->locked = false;
                                            $DataBankTransaction->status =  false;
                                            $DataBankTransaction->company_id =$company_id;
                                            $DataBankTransaction->save();
                                            $alm [$cont] = $DataBankTransaction;


                                
                                    }
                            }
                        }
                }
              $data =  [$alm];
              return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank === BankEnumData::BANCRECER) {
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);
                $months =0;
                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::BANCRECER
                ]);
                    foreach ($excelData as $key => $values) {
                        $longArray =count($excelData);
                        $max =  $longArray -1;
                        $reference = str_replace('REF: ', '', $values[1]);
                    
                        
                       if($key >=  2){
                        
                       
                        if($values[4] > 0){
                           
                           
                                $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', (string)$values[0])
                                    ->where('reference', (string) $reference)
                                    ->where('amount', (float)$values[4])
                                    ->where('company_id', $company_id)
                                    ->first();
                                if (!isset($dataBanksTransactions)) {
                                    $cont = $cont +1;
                                    $DataBankTransaction = new DataBankTransaction;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string) $reference;
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = (string)$values[0];
                                    $DataBankTransaction->amount =  (float) $values[4];
                                    $DataBankTransaction->description = (string)$values[2];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status =  false;
                                    $DataBankTransaction->company_id =$company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;
                                }
                            }
                            if( $values[3]> 0){
                                
                            
                                $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', (string)$values[0])
                                ->where('reference', (string) $reference)
                                ->where('amount', (float) $values[3])
                                ->where('company_id', $company_id)
                                    ->first();
                                    if (!isset($dataTransactionDebit)){

                                            $cont = $cont + 1;
                                            $DataBankTransaction = new DataTransactionDebit;
                                            $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                            $DataBankTransaction->reference = (string) $reference;
                                            $DataBankTransaction->name_bank = (string)$bank;
                                            $DataBankTransaction->transaction_date = (string)$values[0];
                                            $DataBankTransaction->amount = (float) $values[3];
                                            $DataBankTransaction->description = (string)$values[2];
                                            $DataBankTransaction->locked = false;
                                            $DataBankTransaction->status =  false;
                                            $DataBankTransaction->company_id =$company_id;
                                            $DataBankTransaction->save();
                                            $alm [$cont] = $DataBankTransaction;


                                
                                    }
                            }
                        }
                 }
              $data =  [$alm];
             return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank === BankEnumData::DELSUR) {
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);
                $months =0;
                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::DELSUR
                ]);
                    foreach ($excelData as $key => $values) {
                        $longArray =count($excelData);
                        $max =  $longArray -1;
                        
                       if($key >=  2){
                        
                        if ( (float) $values[4] > 0){
                           
                           
                                $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', (string)$values[0])
                                    ->where('reference', (string) $values[2])
                                    ->where('amount', (float)$values[4])
                                    ->where('company_id', $company_id)
                                    ->first();
                                if (!isset($dataBanksTransactions)) {
                                    $cont = $cont +1;
                                    $DataBankTransaction = new DataBankTransaction;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string)$values[2];
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = (string)$values[0];
                                    $DataBankTransaction->amount =  (float) $values[4];
                                    $DataBankTransaction->description = (string)$values[1];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status =  false;
                                    $DataBankTransaction->company_id =$company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;
                                }
                            }
                            if( (float) $values[3]> 0){
                                
                            
                                $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', (string)$values[0])
                                ->where('reference', (string) $values[2])
                                ->where('amount', (float) $values[3])
                                ->where('company_id', $company_id)
                                    ->first();
                                    if (!isset($dataTransactionDebit)){

                                            $cont = $cont + 1;
                                            $DataBankTransaction = new DataTransactionDebit;
                                            $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                            $DataBankTransaction->reference = (string) $values[2];
                                            $DataBankTransaction->name_bank = (string)$bank;
                                            $DataBankTransaction->transaction_date = (string)$values[0];
                                            $DataBankTransaction->amount = (float) $values[3];
                                            $DataBankTransaction->description = (string)$values[1];
                                            $DataBankTransaction->locked = false;
                                            $DataBankTransaction->status =  false;
                                            $DataBankTransaction->company_id =$company_id;
                                            $DataBankTransaction->save();
                                            $alm [$cont] = $DataBankTransaction;


                                
                                    }
                            }
                        }
                 }
              $data =  [$alm];
             return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank === BankEnumData::BANCAMIGA) {
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);
                $months =0;
                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::BANCAMIGA
                ]);
                foreach ($excelData as $key => $values) {



                        if ($key >= 3) {

                            if ((float)$values[5] > 0) {
                                $amount = $values[5];

                                $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', $values[1])
                                    ->where('reference', (string)$values[2])
                                    ->where('amount', (float)$amount)
                                    ->where('company_id', $company_id)
                                    ->first();
                                if (!isset($dataBanksTransactions)) {
                                    $cont = $cont + 1;
                                    $DataBankTransaction = new DataBankTransaction;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string)$values[2];
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values[1];
                                    $DataBankTransaction->amount = (float)$amount;
                                    $DataBankTransaction->description = $values[3];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status = false;
                                    $DataBankTransaction->company_id = $company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;
                                }
                            }
                            if( (float) $values[4]> 0){

                                $amount = $values[4];
                                $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', $values[1])
                                    ->where('reference', (string)$values[2])
                                    ->where('amount', (float)$amount)
                                    ->where('company_id', $company_id)
                                    ->first();
                                if (!isset($dataTransactionDebit)){

                                    $cont = $cont + 1;
                                    $DataBankTransaction = new DataTransactionDebit;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string)$values[2];
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values[1];
                                    $DataBankTransaction->amount = (float)$amount;
                                    $DataBankTransaction->description = $values[3];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status = false;
                                    $DataBankTransaction->company_id = $company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;



                                }
                            }
                        }

                }
                $data =  [$alm];
                return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank === BankEnumData::BANPLUS) {
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);
                $months =0;
                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::BANPLUS
                ]);
                $cant=count($excelData);
                foreach ($excelData as $key => $values) {

                    $t=$cant - 1;

                    if(($key >=  2 )&& ($key<$t)){

                        if ((float)$values[4] > 0) {
                            $amount = $values[4];
                            $reference=(string) $values[1];
                            $reference=ltrim( $reference, "'");
                            $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', $values[0])
                                ->where('reference', (string)$reference)
                                ->where('amount', (float)$amount)
                                ->where('company_id', $company_id)
                                ->first();
                            if (!isset($dataBanksTransactions)) {
                                $cont = $cont + 1;
                                $DataBankTransaction = new DataBankTransaction;
                                $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                $DataBankTransaction->reference = (string)$reference;
                                $DataBankTransaction->name_bank = (string)$bank;
                                $DataBankTransaction->transaction_date = $values[0];
                                $DataBankTransaction->amount =  (float)$amount;
                                $DataBankTransaction->description = $values[2];
                                $DataBankTransaction->locked = false;
                                $DataBankTransaction->status = false;
                                $DataBankTransaction->company_id = $company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;
                            }
                        }
                        if( (float) $values[3]> 0){
                            $reference=(string) $values[1];
                            $reference=ltrim( $reference, "'");
                            $amount = $values[3];
                            $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                ->where('name_bank', (string)$bank)
                                ->where('transaction_date', $values[0])
                                ->where('reference', (string)$reference)
                                ->where('amount', (float)$amount)
                                ->where('company_id', $company_id)
                                ->first();
                            if (!isset($dataTransactionDebit)){

                                $cont = $cont + 1;
                                $DataBankTransaction = new DataTransactionDebit;
                                $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                $DataBankTransaction->reference = (string)$reference;
                                $DataBankTransaction->name_bank = (string)$bank;
                                $DataBankTransaction->transaction_date = $values[0];
                                $DataBankTransaction->amount =  (float)$amount;
                                $DataBankTransaction->description = $values[2];
                                $DataBankTransaction->locked = false;
                                $DataBankTransaction->status = false;
                                $DataBankTransaction->company_id = $company_id;
                                $DataBankTransaction->save();
                                $alm [$cont] = $DataBankTransaction;



                            }
                        }
                    }

                }
                $data =  [$alm];
                return responseRequest( EnumResponse::SUCCESS, $data );
            }
            elseif ($bank === BankEnumData::AGRICOLA) {
                $bankInformation = [];
                $file = $request->file('archive');
                $lines = file($file);
                $months =0;
                if (!count($lines)) {
                    return $bankInformation->makeValidation($bank);
                }
                $excelData = Excel::load($file, function ($reader) {
                    $reader->setDateFormat('d/m/Y');
                    $reader->noHeading();
                })->all();

                $successfulRegistrations = $this->dataBankRepo->transaction([

                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => BankEnumData::AGRICOLA
                ]);
                foreach ($excelData as $key => $values) {



                        if($key>2){

                            if((float) $values[4]>0){
                                $amount = $values[4];
                                $reference=(string) $values[2];

                                $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', $values[0])
                                    ->where('reference', (string)$reference)
                                    ->where('amount', (float)$amount)
                                    ->where('company_id', $company_id)
                                    ->first();
                                if (!isset($dataBanksTransactions)) {
                                    $cont = $cont +1;
                                    $DataBankTransaction = new DataBankTransaction;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string)$reference;
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values[0];
                                    $DataBankTransaction->amount =  (float)$amount;
                                    $DataBankTransaction->description = $values[3];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status = false;
                                    $DataBankTransaction->company_id = $company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;
                                }
                            }
                            if( (float) $values[4] < 0){
                                $reference=(string) $values[2];
                                $amount =str_replace('-', '', $values[4]);
                                $dataTransactionDebit = DB::table('data_transaction_debit')->select('id')
                                    ->where('name_bank', (string)$bank)
                                    ->where('transaction_date', $values[0])
                                    ->where('reference', (string)$reference)
                                    ->where('amount', (float)$amount)
                                    ->where('company_id', $company_id)
                                    ->first();
                                if (!isset($dataTransactionDebit)){

                                    $cont = $cont + 1;
                                    $DataBankTransaction = new DataTransactionDebit;
                                    $DataBankTransaction->databank_id = (int)$successfulRegistrations->id;
                                    $DataBankTransaction->reference = (string)$reference;
                                    $DataBankTransaction->name_bank = (string)$bank;
                                    $DataBankTransaction->transaction_date = $values[0];
                                    $DataBankTransaction->amount =  (float)$amount;
                                    $DataBankTransaction->description = $values[3];
                                    $DataBankTransaction->locked = false;
                                    $DataBankTransaction->status = false;
                                    $DataBankTransaction->company_id = $company_id;
                                    $DataBankTransaction->save();
                                    $alm [$cont] = $DataBankTransaction;



                                }
                            }
                        }

                }
                $data =  [$alm];
                return responseRequest( EnumResponse::SUCCESS, $data );
            }
            else {
                if (!$bankInformation->getFileUpload()) {
                    return $bankInformation->makeValidation($bank);
                }

                $successfulRegistrations = $this->dataBankRepo->transaction([
                    'owner_user' => $user->id,
                    'bank_id' => $bank_id,
                    'company_id' => $company_id,
                    'bank_description' => $bank
                ]);

            }

            return responseRequest(EnumResponse::SUCCESS, [

            ]);

        }

        return responseRequest(
            EnumResponse::CUSTOM_FAILED,
            'Error al cargar fichero',
            InventoryError::CUSTOM_ERROR_MESSAGE
        );
    }

    /**
     * GetTransactionLog ImportingDataBank.
     *
     * @param Request $request
     * @return bodyResponseRequest $transaction
     */
    public function getTransactionLog(Request $request)
    {
        $transaction = $this->dataBankRepo->getTransactionByReference($request->input('reference'));

        return count($transaction)
            ? responseRequest(EnumResponse::SUCCESS, [
                'transaction' => $transaction
            ])
            : responseRequest(
                EnumResponse::FAILED, [],
                InventoryError::NO_RECORDS_FOUND
            );
    }
    /**
     * LockUnlockData ImportingDataBank.
     * This function locked unlock data.
     * @param Request $request
     * @return bodyResponseRequest
     */
    public function lockUnlockData(Request $request, $id)
    {
        if (!$this->bankTransRepo->isExist($id))
            return responseRequest(
                EnumResponse::CUSTOM_FAILED, 'banktransaction',
                InventoryError::IS_NOT_REGISTERED
            );

        $this->bankTransRepo->lockedUnlocked($id, $request->input('option'));

        return responseRequest(EnumResponse::POST_SUCCESS, []);
    }
    /**
     * All DataBankTransactionRepo.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function all()
    {
        try {

            $data = DB::table('data_banks_transactions')->where('locked',  0)->get();
            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'ImportingDataBankController.all.catch' );
        }
    }
    public function registerCommercialBankInformation($lines)
    {
        $bankInformation = collect();

        foreach ($lines as $Key => $line) {
            $parts = preg_split('/\s+/', $line);
            $position = $this->_getPositionAmount($parts);
            $positionAmount = isset($parts[$position]) ? $parts[$position] : 0.00;
            $condition = substr(
                $line,
                CommercialEnumData::START_CONDITION,
                CommercialEnumData::END_CONDITION
            );

            $day = substr($line, 26, 2);
            $month = substr($line, 28, 2);
            $year = substr($line, 30, 4);
            $date = $day.'/'.$month.'/'.$year;
            $reference = str_pad(substr($line, 37, 9), 9, '0', STR_PAD_LEFT);
            $amount = 0.00;
            $information = '';

            switch ($condition) {
                case CommercialEnumData::DP:
                    $stringConsulted = substr($line, 50);

                    $pos = strpos($stringConsulted, CommercialEnumData::CHECK);

                    if ($pos === false) {
                        $initialPosition = CommercialEnumData::INITIAL_POSITION_WITHOUT_CHECK;
                        $information = substr($line, 50, 20);
                    } else {
                        $initialPosition = CommercialEnumData::INITIAL_POSITION_WITH_CHECK;
                        $information = substr($line, 50, 29);
                    }

                    $stringConsulted = substr($line, $initialPosition);
                    $dividedString = explode(' ', $stringConsulted);
                    //$amount = $dividedString[0];
                    $amount = $positionAmount;
                    break;
                case CommercialEnumData::NC:
                $conditionstransaction = substr(
                    $line,
                    CommercialEnumData::START_CONDITIONTRANSTION,
                    CommercialEnumData::END_CONDITIONTRANSTION
                );
                
                if($conditionstransaction==="TRANSFERENCIA"){
                    $stringConsulted = substr($line, 88);

                    $dividedString = explode(' ', $stringConsulted);

                    //$amount = $dividedString[0];
                    $amount = $positionAmount;
                    
                    $information = substr($line, 50, 38);
                    

                }else{
                    $stringConsulted = substr($line, 80);

                    $dividedString = explode(' ', $stringConsulted);

                    //$amount = $dividedString[0];
                    $amount = $positionAmount;
                   
                    $information = substr($line, 50, 28);
                }

                    break;
                case CommercialEnumData::ND:


                    $amount = '-'.$positionAmount;
                    $information = substr($line, 50, 80);

                    break;
            }


                $long=strlen($reference);

                if($long>9){
                    $ini =  $long -9;
                    $resultado = substr($reference, $ini , $long);
                }else{
                    $resultado = $reference;

                }


                $bankInformation->push([
                    'transaction_date' => $date,
                    'reference'=> $resultado,
                    'description' => $information,
                    'amount' => $amount,
                ]);

        }

        return $bankInformation->values()->all();
    }
    public function lockUnLocked(Request $request)
    {

        $amount ='Debe introducir un monto.!';
        $reference ='Debe introducir una referencia.!';
        $bank ='Debe introducir un banco.!';
        $today ='Debe introducir un fecha.!';
        if(!isset($request->amount)){
            return bodyResponseRequest( EnumResponse::FAILED, $amount );
        }
        if(!isset($request->reference)){
            return bodyResponseRequest( EnumResponse::FAILED, $reference );
        }
        if(!isset($request->bank)){
            return bodyResponseRequest( EnumResponse::FAILED, $bank );
        }
        if(!isset($request->today)){
            return bodyResponseRequest( EnumResponse::FAILED, $today );
        }
        $transaction ='No se encontró ningún depósito relacionado.!';

        $exit=$this->bankTransRepo->checkExistence($request);
        if(!isset($exit->id)){
            return bodyResponseRequest( EnumResponse::FAILED, $transaction );
        }
        $bank = DataBankTransaction::find( (int) $exit->id );
        if($bank->locked === 0){

            $bank->locked = true;
            $bank->save();
            return bodyResponseRequest( EnumResponse::SUCCESS, $bank );
        }else{
            $locked =' Este Depósito ya fue bloqueado..!';

            return bodyResponseRequest( EnumResponse::FAILED, $locked );
        }

    }


    public function UnLocked(Request $request)
    {
        $amount ='Debe introducir un monto.!';
        $reference ='Debe introducir una referencia.!';
        $bank ='Debe introducir un banco.!';
        $today ='Debe introducir un fecha.!';
        if(!isset($request->amount)){
            return bodyResponseRequest( EnumResponse::FAILED, $amount );
        }
        if(!isset($request->reference)){
            return bodyResponseRequest( EnumResponse::FAILED, $reference );
        }
        if(!isset($request->bank)){
            return bodyResponseRequest( EnumResponse::FAILED, $bank );
        }
        if(!isset($request->today)){
            return bodyResponseRequest( EnumResponse::FAILED, $today );
        }
        $transaction ='No se encontró ningún depósito relacionado.!';

        $exit=$this->bankTransRepo->checkExistences($request);
        if(!isset($exit)){
            return bodyResponseRequest( EnumResponse::FAILED, $transaction );
        }
        $bank = DataBankTransaction::find( (int) $exit->id );
        if($bank->locked === 1){
            $bank->locked = false;
            $bank->save();
            return bodyResponseRequest( EnumResponse::SUCCESS, $bank );
        }else{
            $locked =' Este Depósito ya fue desbloqueado..!';

            return bodyResponseRequest( EnumResponse::FAILED, $locked );
        }


    }
    private function _detectOperation($parts)
    {
        if (count($parts)) {
            $firstPosition = $parts[5];

            if ($firstPosition === 'SI') {
                return CommercialEnumData::IS_INITIAL_BALANCE;
            }

            if ($firstPosition === 'SF') {
                return CommercialEnumData::IS_FINAL_BALANCE;
            }

            if ($firstPosition == CommercialEnumData::NC) {
                if (isset($parts[6])  && $parts[6]  === 'PAGO'     &&
                    isset($parts[7])  && $parts[7]  === 'A'        &&
                    isset($parts[8])  && $parts[8]  === 'TERCEROS' &&
                    isset($parts[9])  && $parts[9]  === 'VIA'      &&
                    isset($parts[10]) && $parts[10] === 'INTERNET'
                ) {
                    return CommercialEnumData::IS_PAY_THIRD_INTERNET;
                }

                if (isset($parts[6])  && $parts[6]  === 'PAGO'        &&
                    isset($parts[7])  && $parts[7]  === 'A'           &&
                    isset($parts[8])  && $parts[8]  === 'PROVEEDORES' &&
                    isset($parts[9])  && $parts[9]  === 'EN'          &&
                    isset($parts[10]) && $parts[10] === 'LINEA'
                ) {
                    return CommercialEnumData::IS_PAY_PROVIDER_ONLINE;
                }

                if (isset($parts[6])  && $parts[6]  === 'ORDEN'        &&
                    isset($parts[7])  && $parts[7]  === 'DE'           &&
                    isset($parts[8])  && $parts[8]  === 'PAGO'         &&
                    isset($parts[9])  && $parts[9]  === 'VIA'          &&
                    isset($parts[10]) && $parts[10] === 'ELECTRONICA'
                ) {
                    return CommercialEnumData::IS_PAY_ORDER_ELECTRONIC;
                }
                if (isset($parts[6])  && $parts[6]  === 'TRANSFERENCIA' && 
                            isset($parts[7])  && $parts[7]  === 'DE'                && 
                            isset($parts[8])  && $parts[8]  === 'FONDOS'            && 
                            isset($parts[9])  && $parts[9]  === 'VIA'              && 
                            isset($parts[10]) && $parts[10] === 'INTERNET'
                ) {
                            return CommercialEnumData::IS_PAY_TRANSACTION_ONLINE;
                }

                if (isset($parts[6])  && $parts[6]  === 'ORDEN'     &&
                    isset($parts[7])  && $parts[7]  === 'DE'        &&
                    isset($parts[8])  && $parts[8]  === 'PAGO'      &&
                    isset($parts[9])  && $parts[9]  !== 'VIA'
                ) {
                    return CommercialEnumData::IS_PAY_ORDER;
                }
            }

            if ($firstPosition == CommercialEnumData::DP) {
                if (isset($parts[6])  && $parts[6]  === 'DEPOSITO' &&
                    isset($parts[7])  && $parts[7]  === 'EN'       &&
                    isset($parts[8])  && $parts[8]  === 'EFECTIVO'
                ) {
                    return CommercialEnumData::IS_CASH_DEPOSIT;
                }

                if (isset($parts[6])  && $parts[6]  === 'DEPOSITO' &&
                    isset($parts[7])  && $parts[7]  === 'EN'       &&
                    isset($parts[8])  && $parts[8]  === 'CHEQUE'
                ) {
                    return CommercialEnumData::IS_CHECK_DEPOSIT;
                }
            }
            if ($firstPosition == CommercialEnumData::ND) {
                if (isset($parts[6])  && $parts[6]  === 'DEBITO' &&
                    isset($parts[7])  && $parts[7]  === 'CHEQUE' &&
                    isset($parts[8])  && $parts[8]  === 'DIFERIDO'
                ) {
                    return CommercialEnumData::IS_DEBIT;
                }

                if (isset($parts[6])  && $parts[6]  === 'TRANSF.ENTRE' &&
                    isset($parts[7])  && $parts[7]  === 'CUENTAS'      &&
                    isset($parts[8])  && $parts[8]  === 'MISMO'        &&
                    isset($parts[9])  && $parts[9]  === 'TITULAR-EXENT'
                ) {
                    return CommercialEnumData::IS_TRANS;
                }

                if (isset($parts[6])  && $parts[6]  === 'COMISION' &&
                    isset($parts[7])  && $parts[7]  === 'POR'      &&
                    isset($parts[8])  && $parts[8]  === 'RECEPCION' &&
                    isset($parts[9])  && $parts[9]  === 'SERVICIOS' &&
                    isset($parts[10])  && $parts[10]  === 'ESPECIA'
                ) {
                    return CommercialEnumData::IS_COMMISSION;
                }
                if (isset($parts[6])  && $parts[6]  === 'IMPUESTOS' &&
                    isset($parts[7])  && $parts[7]  === 'A'      &&
                    isset($parts[8])  && $parts[8]  === 'LAS'        &&
                    isset($parts[9])  && $parts[9]  === 'TRANSACCIONES' &&
                    isset($parts[10])  && $parts[10]  === 'FINANCIERA'

                ) {
                    return CommercialEnumData::IS_TAX;
                }
                if (isset($parts[6])  && $parts[6]  === 'CARGO' &&
                    isset($parts[7])  && $parts[7]  === 'POR'      &&
                    isset($parts[8])  && $parts[8]  === 'CHEQUES'        &&
                    isset($parts[9])  && $parts[9]  === 'INCONFORMES'

                ) {
                    return CommercialEnumData::IS_CHARGE_FOR_CHECKS;
                }
                if (isset($parts[6])  && $parts[6]  === 'TARIFA'    &&
                    isset($parts[7])  && $parts[7]  === 'MENSUAL'   &&
                    isset($parts[8])  && $parts[8]  === 'POR'       &&
                    isset($parts[9])  && $parts[9]  === 'MERCANTIL' &&
                    isset($parts[10])  && $parts[10]  === 'EN'      &&
                    isset($parts[11])  && $parts[11]  === 'LINEA'   &&
                    isset($parts[12])  && $parts[12]  === 'EM'

                ) {
                    return CommercialEnumData::IS_RATE;
                }
                if (isset($parts[6])  && $parts[6]  === 'PAGO'    &&
                    isset($parts[7])  && $parts[7]  === 'A'   &&
                    isset($parts[8])  && $parts[8]  === 'PROVEEDORES' &&
                    isset($parts[9])  && $parts[9]  === 'EN' &&
                    isset($parts[10])  && $parts[10]  === 'LINEA'

                ) {
                    return CommercialEnumData::IS_PAY_PROVIDER;
                }

                if (isset($parts[6])  && $parts[6]  === 'RECARGO'    &&
                    isset($parts[7])  && $parts[7]  === 'COMISION'

                ) {
                    return CommercialEnumData::IS_RECHARGE_COMMISSION;
                }
            }
        }

        return null;
    }

    private function _getPositionAmount($parts)
    {
        $typeOperation = $this->_detectOperation($parts);
        $position = null;

        switch ($typeOperation) {
            case CommercialEnumData::IS_INITIAL_BALANCE:
            case CommercialEnumData::IS_FINAL_BALANCE:
                $position = 8;
                break;

            case CommercialEnumData::IS_PAY_THIRD_INTERNET:
            case CommercialEnumData::IS_PAY_PROVIDER_ONLINE:
            case CommercialEnumData::IS_PAY_ORDER_ELECTRONIC:
                $position = 11;
                break;

            case CommercialEnumData::IS_PAY_ORDER:
            case CommercialEnumData::IS_CASH_DEPOSIT:
            case CommercialEnumData::IS_CHECK_DEPOSIT:
                $position = 9;
                break;
            case CommercialEnumData::IS_COMMISSION;
            case CommercialEnumData::IS_TAX;
            case CommercialEnumData::IS_PAY_PROVIDER;
            $position = 11;

                break;
            case CommercialEnumData::IS_DEBIT;
                $position = 9;

                break;
            case CommercialEnumData::IS_TRANS;
            case CommercialEnumData::IS_CHARGE_FOR_CHECKS;
                $position = 10;

                break;
            case CommercialEnumData::IS_RATE;
            case CommercialEnumData::IS_RECHARGE_COMMISSION;
                $position = 13;

                break;
                    case CommercialEnumData::IS_PAY_TRANSACTION_ONLINE:
            $position = 11;
            break;
            default:
                break;
        }

        return $position;

    }
    public function deleteTransaction(Request $request)
    {
        $bank ='Debe introducir un banco.!';
        $today ='Debe introducir un fecha.!';

        if(!isset($request->bank)){
            return bodyResponseRequest( EnumResponse::FAILED, $bank );
        }
        if(!isset($request->today)){
            return bodyResponseRequest( EnumResponse::FAILED, $today );
        }
        $exit=$this->bankTransRepo->VerificaDeleteTransaction($request);
        $transaction ='No se encontró ningún depósito relacionado.!';
        if(!isset($exit)){
            return bodyResponseRequest( EnumResponse::FAILED, $transaction );
        }

        try {
                $data=$this->bankTransRepo->deleteTransaction($request);

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'ImportingDataBankController.deleteTransaction.catch' );
        }

    }


}
