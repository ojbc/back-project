<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Utils\Enums\EnumResponse;
use App\Utils\Enums\InventoryError;
use App\Utils\Enums\AuditOperation;
use App\Company;
use App\Repository\Company\CompanyRepo;
use App\Repository\User\UserRepository;
/**
 * Class AuthController.
 *
 * @package App\Http\Controllers
 * @author  <info@expertosdelaweb.net>
 */
class AuthController extends Controller
{     


    /**
     * Create a new construct instance.
     * @route rest-api-project\app\Repository\Company
     * @route rest-api-project\app\Repository\User
     * @param  UserRepository $_userRepository
     * @param  CompanyRepo $_companyRepo
     * @return void
     */
    public function __construct( CompanyRepo $_companyRepo, UserRepository $_userRepository )
    {
        $this->companyRepo = $_companyRepo;
        $this->userRepository = $_userRepository;
    }
    /**
     * .
     *
     * Auth AuthController.
     *
     * @param Request $request
     * @return responseRequest $data
     */
    public function auth(Request $request)
    {

        $credentials = $request->only('email', 'password');
        $token = null;
        $name='La empresa esta desactivada.!  Contactar al administrador.!';
        $name='El usuario esta desactivado.!  Contactar al administrador.!';
        $creden='Las credenciales son invalidas. Por favor intenta nuevamente!';
        try {
            if(!$token = JWTAuth::attempt($credentials)) {

                return bodyResponseRequest( EnumResponse::FAILED, $creden );
            }
        } catch (JWTException $e) {

            return response()->json(['error' => 'something_went_wrong e:' . $e], 500);
        }

        // Obtenemos el usuario logueado
        $user = JWTAuth::toUser($token);
        if (!empty($user->company_id)) {
        $data = $this->companyRepo->statusVerification($user->company_id );
            if(!empty($data)){
                return bodyResponseRequest( EnumResponse::FAILED, $name );
            }
        }
        if (!empty($user->id)) {
            $data = $this->userRepository->statusVerification($user->id );
                if(!empty($data)){
                    return bodyResponseRequest( EnumResponse::FAILED, $name );
                }
            }
    

        if( $user ) {

            $data = [
                'name_id' => $user->profile->full_name,
                'user_id' => $user->id,
                'company_id' => $user->company_id,
                'role_obj' => $user->role,
                'role' => $user->role->slug,
                'permissions' => $user->getPermissions(),
                'token' => $token,
                'status'=> $user->status
            ];


            // CREAMOS LA AUDITORIA.
            auditSecurity( $user->id, $user->company_id, AuditOperation::LOGIN, 'ACCESS' );

            // Devolvemos
            return responseRequest( EnumResponse::SUCCESS, $data );
        }
    }
    /**
     * .
     *
     * Logout AuthController.
     *
     * @param Request $request
     * @return responseRequest
     */
    public function logout(Request $request)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();

            // GENERATED AUDIT ACCESS
            auditSecurity( $user->id, $user->company_id, AuditOperation::LOGOUT, 'ACCESS' );
            //
            JWTAuth::invalidate( JWTAuth::getToken() );

            return bodyResponseRequest( EnumResponse::CUSTOM_SUCCESS, [], 'Sesión finalizada exitosamente' );
        } catch (\Exception $ex) {
            return bodyResponseRequest(EnumResponse::ERROR, $ex, [], 'CustomOAuthController.getAuthorization.catch');
        }

    }
}