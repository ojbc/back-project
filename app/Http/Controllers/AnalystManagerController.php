<?php

namespace App\Http\Controllers;
use App\User;
use App\Utils\Enums\EnumResponse;
use Illuminate\Http\Request;
use App\AnalystManager;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;
use App\Utils\Enums\AuditOperation;


/**
 * Class AnalystManagerController.
 *
 * @package App\Http\Controllers
 * @author  <info@expertosdelaweb.net>
 */
class AnalystManagerController extends Controller
{
    
    /**
     * All AnalystManager.
     *
     * @param Request $request
     * @return bodyResponseRequest $analyst
     */
    public function all(Request $request)
    {
        try {
            $analyst = User::where('role_id', 2)->With('users', 'profile')->where('company_id', $request->company_id)->orderBy('created_at', 'DESC')->get();

            return bodyResponseRequest(EnumResponse::SUCCESS, $analyst);
        } catch (\Exception $e) {
            return bodyResponseRequest(EnumResponse::ERROR, $e, [], 'analystManagerController.all.catch');
        }
    }
   






}
