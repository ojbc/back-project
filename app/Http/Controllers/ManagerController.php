<?php

namespace App\Http\Controllers;

use App\Gift;
use App\User;
use App\Profile;
use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Utils\Enums\EnumResponse;
use App\Utils\Enums\AuditOperation;
use App\Repository\Manager\ManagerRepository;
use App\Repository\Manager\Manager\GerentesManager;
use App\Repository\Manager\Manager\GiftGerentesManager;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\Finder;

/**
 * Class ManagerController.
 *
 * @package App\Http\Controllers
 * @author  <info@expertosdelaweb.net>
 */
class ManagerController extends Controller
{
    private $managerRepository;
    /**
     * Create a new construct instance.
     * @route rest-api-project\app\Repository\Manager
     * @param  ManagerRepository $_managerRepository
     * @return void
     */
    public function __construct( ManagerRepository $_managerRepository )
    {
        $this->managerRepository = $_managerRepository;
    }

    /**
     * All Manager.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function all()
    {
        try {

            $data = User::where('role_id', '=', 1)
                ->with('profile')->orderBy('created_at', 'DESC')->get();

            return bodyResponseRequest( EnumResponse::SUCCESS, $data );
        } catch (\Exception $e) {
            return bodyResponseRequest( EnumResponse::ERROR, $e, [], 'ManagerController.all.catch' );
        }
    }


    
    

}

