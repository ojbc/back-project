<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function banks()
    {
        return $this->belongsTo('App\BankCompany', 'company_id', 'id');
    }
    public function user()
    {
        return $this->hasOne('App\User');
    }
    public function bankcompany()
    {
        return $this->belongsToMany('App\Bank');
    }

}
