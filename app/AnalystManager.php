<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnalystManager extends Model
{

    /**
     * Tabla asociada al modelo.
     *
     * @var string
     */

    protected $table = 'analyst_manager';




    public function users()
    {

        return $this->hasMany('App\Profile', 'user_id', 'manager_id', 'analyst_id');

    }
    public function usuario()
    {

        return $this->hasMany('App\Profile', 'user_id', 'analyst_id');

    }
}
