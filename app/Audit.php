<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Audit extends Model
{
    /**
     * Tabla asociada al modelo.
     *
     * @var string
     */
    protected $table = 'audits';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'company_id', 'type_operation', 'module', 'model', 'details', 'created_at'
    ];

    protected $dates = [
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'user_id', 'user_id');
    }
}
