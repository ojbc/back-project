<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * Tabla asociada al modelo.
     *
     * @var string
     */
    protected $table = 'profiles';

    protected $appends = ['full_name'];

    function getFullNameAttribute()
    {
        return $this->name . ' ' . $this->lastname;
    }
}
