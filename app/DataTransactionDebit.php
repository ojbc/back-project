<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataTransactionDebit extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'data_transaction_debit';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'    ];

    public function transactionsBank()
    {
        return $this->hasOne('App\DataBank', 'id', 'databank_id');
    }
    public function transactions()
    {
        return $this->hasMany(DataBankTransaction::class, 'databank_id');
    }
}