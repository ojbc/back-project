<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankCompany extends Model
{
  

    /**
     * Tabla asociada al modelo.
     *
     * @var string
     */
    protected $table = 'bank_company';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bank_id', 'company_id', 'status'
    ];
    
    public function banks()
    {
        return $this->belongsTo('App\Bank', 'bank_id', 'id');
    }
    public function companies()
    {
        return $this->belongsTo('App\Company', 'company_id', 'id');
    }
}
