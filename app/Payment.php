<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * Tabla asociada al modelo.
     *
     * @var string
     */
    protected $table = 'payments';


    public function managers()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function users()
    {

        return $this->hasOne('App\Profile', 'user_id', 'user_id');

    }

    
}
