<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportingDataBank extends Model
{
    protected $table = 'data_banks_transactions';

    public function bank()
    {

        return $this->hasOne('App\DataBank', 'id', 'databank_id');

    }
    public function references()
    {

        return $this->hasOne('App\Payment', 'reference', 'reference');

    }
}
