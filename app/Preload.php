<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preload extends Model
{
    protected $table = 'preloads';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];


    public function managers()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function users()
    {

        return $this->hasOne('App\Profile', 'user_id', 'user_id');

    }
}
