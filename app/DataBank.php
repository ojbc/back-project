<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataBank extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'data_banks';

    /**
     * @var array
     */
    protected $fillable = ['owner_user','bank_id', 'bank_description','company_id', 'created_at'];

    /**
     * This relationship with data_banks_transactions.
     *
     * @return mixed
     */
    public function transactions()
    {
        return $this->hasMany(DataBankTransaction::class, 'databank_id');
    }
    public function bank()
    {

        return $this->hasOne('App\Bank', 'id', 'bank_id');

    }
}
