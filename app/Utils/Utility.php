<?php

namespace App\Utils;

use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use iio\libmergepdf\Merger;
use Symfony\Component\Finder\Finder;
use PDF;
use App;
use View;

class Utility
{
    /**
     * Convert to float value by string in value Example: '1.000,25' to 1000,25.
     * 
     * @param string $num
     * @return double
     */
    public static function tofloat($num) 
    {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos : 
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
       
        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        } 

        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
        );
    }

    public static function buildPdf($dataRows, $dataCompact, $view, $chunk = 500, $module = 'ChangeBreakdowns', $pdfName = 'Cambios_Averias', $horientation = 'portrait', $size = 'A4')
    {
        if (empty($dataRows)) return null;

        ini_set('max_execution_time', 0);
        ini_set("memory_limit", '1024M');
        set_time_limit(5000);

        $mkdirName = [];
        $date = Carbon::now();

        if ($pdfName === 'Cambios_Averias') {
            $pdfName = uniqid();
        }

        $namePDF = str_slug($date) . $pdfName . '.pdf';
        
        $user = \JWTAuth::parseToken()->toUser();
        $pathConcatenate = "pdf/concatenate/$module/" . $user->id;
        $pathReport = "pdf/report/$module/" . $user->id;
        $publicPathConcatenate = public_path($pathConcatenate);
        $publicPathReport = public_path($pathReport);
        
        $t = count($dataRows);
        
    
            $concatenate = array_chunk($dataRows, $chunk);
       
        
        if (File::exists($publicPathConcatenate)) {
            $files = glob($publicPathConcatenate . '/*.pdf');
            foreach ($files as $filename) {
                unlink($filename);
            }
        } else {
            File::makeDirectory($pathConcatenate, 0777, true);
        }

        if (File::exists($publicPathReport)) {
            $files = glob($publicPathReport . '/*.pdf');
            foreach ($files as $filename) {
                unlink($filename);
            }
        } else {
            File::makeDirectory($pathReport, 0777, true);
        }

        $totalChunks = count($concatenate);
        
        $page = 1;

        foreach ($concatenate as $key => $items) {
            sleep(1);
            $concatentePDF = $key . '.pdf';
            $mkdirName[$key] = $publicPathConcatenate . '/' . $concatentePDF;
            $pdf = App::make('dompdf.wrapper');
            $pdf->setPaper($size, $horientation);
            $html = View::make($view, compact('dataCompact', 'items', 'key', 'totalChunks', 'page'))->render();
            $pdf->loadHTML($html)->save($mkdirName[$key]);
            $page++;
        }

        $finder = new Finder;
        $finder->files()->in($publicPathConcatenate)->name('*.pdf')->sortByAccessedTime();
        $time = time();

        $routeToFile = $publicPathReport . '/' . $pdfName . '.pdf';
        $urlToFile = $pathReport . '/' . $pdfName . '.pdf';

        $merger = new Merger;
        $merger->addFinder($finder);
        $createdPdf = $merger->merge();
        
        file_put_contents($routeToFile, $createdPdf);
        
        gc_collect_cycles();

        if (File::exists($publicPathConcatenate)) {
            $files = glob($publicPathConcatenate . '/*.pdf');
            foreach ($files as $filename) {
                unlink($filename);
            }
        }

        utf8_encode($createdPdf);
        $url = ['url' => url($urlToFile), 'type' => 'download'];
        return   $url; 
    }
}