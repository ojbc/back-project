<?php

namespace App\Utils\Enums;
class BankEnumData
{
    /**
     * @var string
     */
    const BANESCO = 'Banesco'; /**/

    /**
     * @var string
     */
    const PROVINCIAL = 'Provincial'; /**/

    /**
     * @var string
     */
    const PROVINCIAL_PERU = 'Provincial Peru';

    /**
     * @var string
     */
    const MERCANTIL = 'Mercantil'; /**/

    /**
     * @var string
     */
    const BANCOACTIVO = 'Banco Activo'; /**/

    /**
     * @var string
     */
    const BICENTENARIO = 'Bicentenario'; /**/

    /**
     * @var string
     */
    const BFC = 'BFC';

    /**
     * @var string
     */
    const CIEN_BANCO = '100%Banco'; /**/

    /**
     * @var string
     */
    const EXTERIOR = 'Exterior'; /**/

    /**
     * @var string
     */
    const VENEZUELA = 'Venezuela'; /**/

    /**
     * @var string
     */
    const CARONI = 'Caroni'; /**/

    /**
     * @var string
     */
    const SOFITASA = 'Sofitasa'; /**/

    /**
     * @var string
     */
    const BNC = 'BNC';/**/

    /**
     * @var string
     */
    const BOD = 'BOD'; /**/
    /**
     * @var string
     */
    const PLAZA = 'Plaza'; /**/
    /**
     * @var string
     */
    const TESORO = 'Tesoro'; /**/
    /**
     * @var string
     */
    const BANCARIBE = 'Bancaribe'; /**/
      /**
     * @var string
     */
    const BANCRECER = 'Bancrecer'; /**/
     /**
 * @var string
 */
    const DELSUR = 'Delsur'; /**/
    /**
     * @var string
     */
    const BANCAMIGA = 'Bancamiga'; /**/
    /**
     * @var string
     */
    const BANPLUS = 'Banplus'; /**/
    /**
     * @var string
     */
    const AGRICOLA = 'Agricola'; /**/

}