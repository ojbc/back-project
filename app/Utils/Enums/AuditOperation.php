<?php

namespace App\Utils\Enums;

class AuditOperation
{
    /**
     * @string
     */
    const CREATE = 'CREATE';

    /**
     * @string
     */
    const UPDATE = 'UPDATE';

    /**
     * @string
     */
    const DELETE = 'DELETE';

    /**
     * @string
     */
    const RESTORE = 'RESTORE';

    /**
     * @string
     */
    const LOGIN = 'LOGIN';

    /**
     * @string
     */
    const LOGOUT = 'LOGOUT';
}