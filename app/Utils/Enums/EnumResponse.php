<?php

namespace App\Utils\Enums;


class EnumResponse
{
    /**
     * @var string
     */
    const SUCCESS = 'success';

    /**
     * @var string
     */
    const POST_SUCCESS = 'post_success';

    /**
     * @var string
     */
    const CUSTOM_SUCCESS = 'custom_success';

    /**
     * @var string
     */
    const FAILED = 'failed';

    /**
     * @var string
     */
    const CUSTOM_FAILED = 'custom_failed';

    /**
     * @var string
     */
    const EMPTY_NOTIFY = 'empty_notify';

    /**
     * @var string
     */
    const ERROR = 'error';
}