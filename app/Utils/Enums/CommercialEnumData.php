<?php
/**
 * Created by PhpStorm.
 * User: Desarrollo
 * Date: 22/10/2018
 * Time: 3:33 PM
 */

namespace App\Utils\Enums;


class CommercialEnumData
{
    /**
     * @var string
     */
    const DP = 'DP';

    /**
     * @var string
     */
    const NC = 'NC';
    /**
     * @var string
     */
    const ND = 'ND';
    /**
     * @var string
     */
    const START_CONDITION = 47;

    /**
     * @var string
     */
    const END_CONDITION = 2;

    /**
     * @var string
     */
    const CHECK = 'CHEQUE';

    /**
     * @var string
     */
    const CASH = 'EFECTIVO';

    /**
     * @var string
     */
    const INITIAL_POSITION_WITH_CASH = 81;

    /**
     * @var string
     */
    const INITIAL_POSITION_WITHOUT_CHECK = 72;

    const INITIAL_POSITION_WITHOUT_CASH= 70;
    /**
     * @var string
     */
    const START_CONDITIONTRANSTION = 50;
    
    /**
     * @var string
     */
    const END_CONDITIONTRANSTION = 13;

    /**
     * @var string
     */
    const INITIAL_POSITION_WITH_CHECK = 81;

    const IS_INITIAL_BALANCE = 'SALDO INICIAL';

    const IS_FINAL_BALANCE = 'SALDO FINAL';

    const IS_PAY_ORDER = 'ORDEN DE PAGO';

    const IS_PAY_THIRD_INTERNET = 'PAGO A TERCEROS VIA INTERNET';

    const IS_PAY_PROVIDER_ONLINE = 'PAGO A PROVEEDORES EN LINEA';

    const IS_PAY_ORDER_ELECTRONIC = 'ORDEN DE PAGO VIA ELECTRONICA';

    const IS_CASH_DEPOSIT = 'DEPOSITO EN EFECTIVO';

    const IS_CHECK_DEPOSIT = 'DEPOSITO EN CHEQUE';
    /** Transactions Debit **/
    const IS_DEBIT= 'DEBITO CHEQUE DIFERIDO';

    const IS_TRANS= 'TRANSF.ENTRE CUENTAS MISMO TITULAR-EXENT';

    const IS_COMMISSION= 'COMISION POR RECEPCION SERVICIOS ESPECIA';

    const IS_TAX= 'IMPUESTOS A LAS TRANSACCIONES FINANCIERA';

    const IS_CHARGE_FOR_CHECKS = 'CARGO POR CHEQUES INCONFORMES';

    const IS_RATE = 'TARIFA MENSUAL POR MERCANTIL EN LINEA EM';

    const IS_PAY_PROVIDER = 'PAGO A PROVEEDORES EN LINEA';

    const IS_RECHARGE_COMMISSION = 'RECARGO COMISION';
    
    const IS_PAY_TRANSACTION_ONLINE = 'TRANSFERENCIA DE FONDOS VIA INTERNET';

}