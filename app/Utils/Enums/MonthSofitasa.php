<?php


namespace App\Utils\Enums;


class MonthSofitasa
{
    /**
     * @var String
     */
    const JAN = 'Ene';

    /**
     * @var String
     */
    const FEB = 'Feb';

    /**
     * @var String
     */
    const MAR= 'Mar';

    /**
     * @var String
     */
    const APR = 'Abr';

    /**
     * @var String
     */
    const MAY = 'May';

    /**
     * @var String
     */
    const JUN = 'Jun';

    /**
     * @var String
     */
    const JUL = 'Jun';

    /**
     * @var String
     */
    const AUG = 'Ago';

    /**
     * @var String
     */
    const SEPT = 'Sep';

    /**
     * @var String
     */
    const OCT = 'Oct';

    /**
     * @var String
     */
    const NOV = 'Nov';

    /**
     * @var String
     */
    const DEC = 'Dic';

    /**
     * @var String
     */
    const BALANCE = 'Saldo';

    /**
     * @var array
     */
    public static $list_months_sofitasa = [
        'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
    ];

    /**
     * This function a implements verify month select sofitasa bank.
     *
     * @param string $month
     * @return boolean
     */
    public static function exitsMonth($month)
    {
        return isset(self::$list_months_sofitasa[$month]) ? TRUE : FALSE;
    }
}