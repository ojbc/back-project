<?php
namespace App\Repository\Payment;
use App\Payment;
use App\User;
use App\Profile;
use App\ImportingDataBank;
use App\Repository\Base\BaseRepository;
use App\Utils\Enums\AuditOperation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use carbon\carbon;
use App;
use View;
use iio\libmergepdf\Merger;
use Symfony\Component\Finder\Finder;
use Illuminate\Support\Facades\File;
use App\Utils\Utility;
use Maatwebsite\Excel\Facades\Excel;
use App\Utils\ServerSide;
use Illuminate\Support\Facades\Storage;

/**
 * Class RegisterPayRepository.
 *
 * @package App\Repository\RegisterPay
 * @author  <info@expertosdelaweb.net>
 */
class PaymentRepository extends BaseRepository
{
    /**
     * RegisterPayRepository construct.
     *
     * @param RegisterPayRepository $model
     * @return void
     */
    public function __construct( Payment $model )
    {
        parent::__construct( $model );
    }

    /**
     * Save RegisterPay
     * Guardamos los datos
     * @param  $request
     * @return $pay
     */
    public function save( $request )
    {
        // Verificamos si tenemos que crear o actualizar
        if( empty($request->id) ) {
            // Creamos el usuario
            $pay = new Payment;
        } else {
            // Obtenemos los datos
            $pay =Payment::find( (int) $request->id );
        }
        $user = \JWTAuth::parseToken()->toUser();
        $rol = DB::table('role_user')
            ->where('user_id', $user->id)
            ->first();

        $pay->user_id =   $user->id;
        $pay->analyst_id = $rol->id;
        $pay->form_pay = $request->form_pay;
        $pay->reference = $request->reference;
        $pay->bank = $request->bank;
        $pay->voucher_date = $request->voucher_date;
        $pay->amount =  $request->amount;
        $pay->company_id = $request->company_id;
        $pay->save();

        return $pay;
    }

    /**
     * PaidValidate RegisterPay
     *
     * @param  $request
     * @return $pay
     */
    public function paidValidate($request)
    {
        $pay = DB::table('data_banks_transactions')
            ->where('reference', $request->reference)
            ->where('transaction_date', $request->voucher_date)
            ->where('amount', $request->amount)
            ->where('name_bank', $request->bank)
            ->where('company_id', $request->company_id)
            ->where('status', '=', '0')
            ->first();

        return $pay;
    }
    /**
     * ReferenceID RegisterPay
     *
     * @param  $request
     * @return $refe
     */
    public function referenceID($request)
    {
        $refe = ImportingDataBank::with('bank','references')
            ->where('reference', $request->reference)
            ->where('company_id', $request->company_id)
            ->where('status', 0)
            ->get();
        return $refe;
    }
    /**
     * ReferenceInfo RegisterPay
     *
     * @param  $request
     * @return $refe
     */
    public function referenceInfo($request)
    {
        $refe = Payment::where('reference', $request->reference)
            ->where('bank', $request->bank)
            ->where('amount', $request->amount)
            ->where('voucher_date', $request->voucher)
            ->where('company_id', $request->company_id)
            ->first();
        return $refe;
    }
    /**
     * IsExistRegisterPay RegisterPay
     *
     * @param  $request
     * @return $pay
     */
    public function isExistRegisterPay($request)
    {
        $pay = DB::table('payments')
            ->where('reference', $request->reference)
            ->where('bank', $request->bank)
            ->where('amount', $request->amount)
            ->where('voucher_date', $request->voucher_date)
            ->where('company_id', $request->company_id)
            ->first();

        return $pay;
    }
    /**
     * IsLockedBank RegisterPay
     *
     * @param  $request
     * @return $locked
     */
    public function isLockedBank($request)
    {
        $locked = DB::table('data_banks_transactions')
            ->where('reference', $request->reference)
            ->where('name_bank', $request->bank)
            ->where('company_id', $request->company_id)
            ->where('locked','=',1)
            ->first();

        return $locked;
    }
    /**
     * check RegisterPay
     *
     * @param  $request
     * @return $locked
     */
    public function check($request)
    {
        $data = ImportingDataBank::with('bank','references')
            ->where('reference', $request->data['ref'])
            ->where('name_bank', $request->data['bank']['name'])
            ->where('transaction_date', $request->data['date'])
            ->where('amount', $request->data['amount'])
            ->where('company_id', $request->company_id)
            ->where('status', 0)
            ->get();


        return $data;
    }
    public function checks($request)
    {
        $data = ImportingDataBank::with('bank','references')
            ->where('reference', $request->data['ref'])
            ->where('name_bank', $request->bank)
            ->where('transaction_date', $request->voucher_date)
            ->where('amount', $request->data['amount'])
            ->where('company_id', $request->company_id)
            ->where('status', 0)
            ->get();


        return $data;
    }
    /**
     * IsExistReferenceBank RegisterPay
     *
     * @param  $request
     * @return $locked
     */
    public function isExistReferenceBank($request)
    {
        $locked = DB::table('data_banks_transactions')
            ->where('reference', $request->reference)
            ->where('company_id', $request->company_id)
            ->where('status','=', 0)
            ->first();

        return $locked;
    }
    /**
     * Payments RegisterPay
     *
     * @param  $request
     * @return $payments
     */
    public function payments($request)
    {
        $payments = DB::table('payments')
            ->where('invoice_id', $request->invoice)
            ->get();
        return $payments;
    }
    /**
     *  total RegisterPay
     *
     * @param  $request
     * @return $payments
     */
    public function total($request)
    {
        if (isset($request['company_id'])){
            if((isset($request->dato['analysts'])) && (isset( $request->dato['dates'])) && (isset($request->dato['date']))){
               
                $query= "
                SELECT 
                SUM(amount) AS suma
                FROM `payments`
                WHERE `payments`.company_id = {$request['company_id']}
                AND `payments`.user_id = {$request->dato['analysts']['id']}
                AND DATE(`payments`.created_at)  BETWEEN DATE('{$request->dato['dates']}')
                AND DATE('{$request->dato['date']}')";
                $data = \DB::select( $query );
                    return $data;

                
                }elseif((isset($request->dato['analysts'])) && (isset( $request->dato['dates']))){
                   
                    $date = Carbon::now();
                    $date = $date->format('Y-m-d');
                    $query= "
                    SELECT 
                    SUM(amount) AS suma
                    FROM `payments`
                    WHERE `payments`.company_id = {$request['company_id']}
                    AND `payments`.user_id = {$request->dato['analysts']['id']}
                    AND DATE(`payments`.created_at)  BETWEEN DATE('{$request->dato['dates']}')
                    AND DATE('{$date}')";
                    $data = \DB::select( $query );
                    return $data;
                   
                
                }elseif((isset( $request->dato['dates'])) && (isset($request->dato['date']))){
                   
                   
                    $query= "
                    SELECT 
                    SUM(amount) AS suma
                    FROM `payments`
                    WHERE `payments`.company_id = {$request['company_id']}
                    AND DATE(`payments`.created_at)  BETWEEN DATE('{$request->dato['dates']}')
                    AND DATE('{$request->dato['date']}')";
                    $data = \DB::select( $query );
                    return $data;
               
                } elseif(isset( $request->dato['dates'])){
                    $date = Carbon::now();
                    $date = $date->format('Y-m-d');
                  
                   
                    $query= "
                    SELECT 
                    SUM(amount) AS suma
                    FROM `payments`
                    WHERE `payments`.company_id = {$request['company_id']} AND
                    DATE(`payments`.created_at)  BETWEEN DATE('{$request->dato['dates']}')
                    AND DATE('{$date}')";
                    $data = \DB::select( $query );
                    return $data;
               
                }elseif(isset($request->dato['analysts'])){
                    
                    $query= "
                    SELECT 
                    SUM(amount) AS suma
                    FROM `payments` 
                    WHERE `payments`.company_id = {$request['company_id']}
                    AND `payments`.user_id = {$request->dato['analysts']['id']}";
                    $data = \DB::select( $query );
                    return $data;
               
            
                }else{
                    
                    $query= "
                    SELECT 
                    SUM(amount) AS suma
                    FROM `payments` 
                    WHERE `payments`.company_id =0";
                    $data = \DB::select( $query );
                    return $data;
                  
                }
            
          }else{
            $query= "
             SELECT 
             SUM(amount) AS suma
             FROM `payments` 
            WHERE `payments`.company_id =0";
            $data = \DB::select( $query );
            return $data;
          }
    }
    public function paymentsTotal2($request)
    {
        
        if (isset($request['company_id'])){
            if((isset($request->dato['analysts'])) && (isset( $request->dato['dates'])) && (isset($request->dato['date']))){
                return  "
                SELECT 
                    `payments`.id,
                    concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
               `payments`.reference,
               `payments`.bank, 
               `payments`.created_at,
               `payments`.amount,
               `payments`.description
               FROM `profiles` 
               INNER JOIN payments ON payments.user_id = `profiles`.user_id
               WHERE `payments`.company_id = {$request['company_id']}
                AND `payments`.user_id = {$request->dato['analysts']['id']}
                AND DATE(`payments`.created_at)  BETWEEN DATE('{$request->dato['dates']}')
                AND DATE('{$request->dato['date']}')";

                
            }elseif((isset($request->dato['analysts'])) && (isset( $request->dato['dates']))){
                    $date = Carbon::now();
                    $date = $date->format('Y-m-d');
                    return  "
                    SELECT 
                        `payments`.id,
                        concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
                   `payments`.reference,
                   `payments`.bank, 
                   `payments`.created_at,
                   `payments`.amount,
                   `payments`.description
                   FROM `profiles` 
                   INNER JOIN payments ON payments.user_id = `profiles`.user_id
                   WHERE `payments`.company_id = {$request['company_id']}
                   AND `payments`.user_id = {$request->dato['analysts']['id']}
                   AND DATE(`payments`.created_at)  BETWEEN DATE('{$request->dato['dates']}')
                   AND DATE('{$date}')";
                   
                
                }elseif((isset( $request->dato['dates'])) && (isset($request->dato['date']))){
                    return  "
                    SELECT 
                        `payments`.id,
                        concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
                   `payments`.reference,
                   `payments`.bank, 
                   `payments`.created_at,
                   `payments`.amount,
                   `payments`.description
                   FROM `profiles` 
                   INNER JOIN payments ON payments.user_id = `profiles`.user_id
                   WHERE `payments`.company_id = {$request['company_id']}
                   AND DATE(`payments`.created_at)  BETWEEN DATE('{$request->dato['dates']}')
                   AND DATE('{$request->dato['date']}')";
               
                } elseif(isset( $request->dato['dates'])){
                    $date = Carbon::now();
                    $date = $date->format('Y-m-d');
                    return  "
                    SELECT 
                        `payments`.id,
                        concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
                   `payments`.reference,
                   `payments`.bank, 
                   `payments`.created_at,
                   `payments`.amount,
                   `payments`.description
                   FROM `profiles` 
                   INNER JOIN payments ON payments.user_id = `profiles`.user_id
                   WHERE `payments`.company_id = {$request['company_id']} AND
                   DATE(`payments`.created_at)  BETWEEN DATE('{$request->dato['dates']}')
                   AND DATE('{$date}')";
               
                }elseif(isset($request->dato['analysts'])){
                return  "
                SELECT 
                    `payments`.id,
                    concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
               `payments`.reference,
               `payments`.bank, 
               `payments`.created_at,
               `payments`.amount,
               `payments`.description
               FROM `profiles` 
               INNER JOIN payments ON payments.user_id = `profiles`.user_id
               WHERE `payments`.company_id = {$request['company_id']}
               AND `payments`.user_id = {$request->dato['analysts']['id']}";
               
            
                }
                else{
                    return  "
             SELECT 
                 `payments`.id,
                 concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
            `payments`.reference,
            `payments`.bank, 
            `payments`.created_at,
            `payments`.amount,
            `payments`.description
            FROM `profiles` 
            INNER JOIN payments ON payments.user_id = `profiles`.user_id
            WHERE `payments`.company_id =0";
                }
            
          }else{
            return  "
             SELECT 
                 `payments`.id,
                 concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
            `payments`.reference,
            `payments`.bank, 
            `payments`.created_at,
            `payments`.amount,
            `payments`.description
            FROM `profiles` 
            INNER JOIN payments ON payments.user_id = `profiles`.user_id
            WHERE `payments`.company_id =0";
          }
    }
    /**
     * Today RegisterPay
     *
     * @param  $request
     * @return $payments
     */
    public function today($request)
    { 
        $query="
                 SELECT  
                SUM(amount) AS suma
                FROM `payments` 
                WHERE `payments`.company_id = {$request['company_id']}
                AND DATE(`payments`.created_at) = '{$request['data']}'";

        $data = \DB::select( $query );
        return $data;
    }

   

    /**
     * PrintPdf RegisterPay
     * Generate a PDF
     * @param $request
     * @return $url
     */
    public function printPdf($request)
    {
        $date = Carbon::now();
        $date = $date->format('d/m/Y H:i:s');
        $user = \JWTAuth::parseToken()->toUser();
        $fileName = uniqid();
        $routeFile = '';  
        $query = $this->paymentsTotal2($request);
        $totalS = $this->total($request);
        $total = $totalS[0]->suma;
        $data = \DB::select( $query );
        if ($request->type === 'pdf') {
            $routeFile = Utility::buildPdf(
                json_decode(json_encode($data), true),
                compact('date', 'total'), 
                'filtros', 
                35, 
                'filtros', 
                $fileName
            );
        }        
        return $routeFile;
    }
    
    public function searchVoucher($request)
    {
        $refe = ImportingDataBank::with('bank','references')
            ->where('reference', (int)$request->reference)
            ->where('name_bank', $request->bank)
            ->where('transaction_date', (String)$request->voucher_date)
            ->where('amount', (float)$request->amount)
            ->where('company_id', $request->company_id)
            ->get();

        return $refe;
    }
    public function searchCDN($request)
    {

        $refe = ImportingDataBank::with('bank','references')
            ->where('name_bank', 'Provincial')
            ->where('DNI', $request->cdn)
            ->where('company_id', $request->company_id)
            ->where('status', 0)
            ->get();

        return $refe;
    }
    public function searchCDNData($request)
    {
        $refe = ImportingDataBank::with('bank','references')
            ->where('name_bank', 'Provincial')
            ->where('DNI', $request->dnc)
            ->where('transaction_date', $request->voucher_date)
            ->where('amount', $request->data['amount'])
            ->where('company_id', $request->company_id)
            ->where('status', 0)
            ->get();

        return $refe;
    }
    public function upData($request)
    {
        $refe = DB::table('payments')
            ->where('id', $request->id)
            ->update(['description' => $request->description]);

        return $refe;
    }

    public function paymentsToday($request)
    {
           
            return  "
             SELECT 
                 `payments`.id,
                 concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
            `payments`.reference,
            `payments`.bank, 
            `payments`.created_at,
            `payments`.amount,
            `payments`.description
            FROM `profiles` 
            INNER JOIN payments ON payments.user_id = `profiles`.user_id
            WHERE `payments`.company_id = {$request['company_id']}
            AND DATE(`payments`.created_at) = '{$request['data']}'";
    }

    public function paymentsTotal($request)
    {
        
        if (isset($request['company_id'])){
            if((isset($request['analyst'])) && (isset( $request['d1'])) && (isset($request['d2']))){
                return  "
                SELECT 
                    `payments`.id,
                    concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
               `payments`.reference,
               `payments`.bank, 
               `payments`.created_at,
               `payments`.amount,
               `payments`.description
               FROM `profiles` 
               INNER JOIN payments ON payments.user_id = `profiles`.user_id
               WHERE `payments`.company_id = {$request['company_id']}
               AND `payments`.user_id = {$request['analyst']}
               AND DATE(`payments`.created_at) BETWEEN DATE('{$request['d1']}')
                AND DATE('{$request['d2']}')";

                
                }elseif((isset($request['analyst'])) && (isset( $request['d1']))){
                    $date = Carbon::now();
                    $date = $date->format('Y-m-d');
                    return  "
                    SELECT 
                        `payments`.id,
                        concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
                   `payments`.reference,
                   `payments`.bank, 
                   `payments`.created_at,
                   `payments`.amount,
                   `payments`.description
                   FROM `profiles` 
                   INNER JOIN payments ON payments.user_id = `profiles`.user_id
                   WHERE `payments`.company_id = {$request['company_id']}
                   AND `payments`.user_id = {$request['analyst']}
                   AND DATE(`payments`.created_at) BETWEEN DATE('{$request['d1']}')
                AND DATE('{$date}')";
                   
                
                }elseif((isset( $request['d1'])) && (isset($request['d2']))){
                    return  "
                    SELECT 
                        `payments`.id,
                        concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
                   `payments`.reference,
                   `payments`.bank, 
                   `payments`.created_at,
                   `payments`.amount,
                   `payments`.description
                   FROM `profiles` 
                   INNER JOIN payments ON payments.user_id = `profiles`.user_id
                   WHERE `payments`.company_id = {$request['company_id']}
                   AND DATE(`payments`.created_at) BETWEEN DATE('{$request['d1']}')
                   AND DATE('{$request['d2']}')";
               
                } elseif(isset( $request['d1'])){
                    $date = Carbon::now();
                    $date = $date->format('Y-m-d');
                    return  "
                    SELECT 
                        `payments`.id,
                        concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
                   `payments`.reference,
                   `payments`.bank, 
                   `payments`.created_at,
                   `payments`.amount,
                   `payments`.description
                   FROM `profiles` 
                   INNER JOIN payments ON payments.user_id = `profiles`.user_id
                   WHERE `payments`.company_id = {$request['company_id']}
                   AND DATE(`payments`.created_at) BETWEEN DATE('{$request['d1']}')
                   AND DATE('{$date}')";
               
                }elseif(isset($request['analyst'])){
                return  "
                SELECT 
                    `payments`.id,
                    concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
               `payments`.reference,
               `payments`.bank, 
               `payments`.created_at,
               `payments`.amount,
               `payments`.description
               FROM `profiles` 
               INNER JOIN payments ON payments.user_id = `profiles`.user_id
               WHERE `payments`.company_id = {$request['company_id']}
               AND `payments`.user_id = {$request['analyst']}";
               
            
                }
                else{
                    return  "
             SELECT 
                 `payments`.id,
                 concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
            `payments`.reference,
            `payments`.bank, 
            `payments`.created_at,
            `payments`.amount,
            `payments`.description
            FROM `profiles` 
            INNER JOIN payments ON payments.user_id = `profiles`.user_id
            WHERE `payments`.company_id =0";
                }
            
          }else{
            return  "
             SELECT 
                 `payments`.id,
                 concat(`profiles`.name, ' ', `profiles`.lastname) AS full_name,
            `payments`.reference,
            `payments`.bank, 
            `payments`.created_at,
            `payments`.amount,
            `payments`.description
            FROM `profiles` 
            INNER JOIN payments ON payments.user_id = `profiles`.user_id
            WHERE `payments`.company_id =0";
          }
    }
 

    public function paginates($request)
    {
        $primaryKey = 'id';

        $query = $this->paymentsToday($request);
        
        $columns = array(
            array( 'db' => 'id', 'dt' => 'id' ),
            array( 'db' => 'full_name', 'dt' => 'full_name' ),
            array( 'db' => 'reference', 'dt' => 'reference' ),
            array( 'db' => 'bank', 'dt' => 'bank' ),
            array( 'db' => 'created_at', 'dt' => 'created_at' ),
            array( 'db' => 'amount', 'dt' => 'amount' ),
            array( 'db' => 'description', 'dt' => 'description' ),
        );
        
        return ServerSide::simple( $request, $query, $primaryKey, $columns );
    }

    public function totalPaginates($request)
    {
        $primaryKey = 'id';

        $query = $this->paymentsTotal($request);
        
        $columns = array(
            array( 'db' => 'id', 'dt' => 'id' ),
            array( 'db' => 'full_name', 'dt' => 'full_name' ),
            array( 'db' => 'reference', 'dt' => 'reference' ),
            array( 'db' => 'bank', 'dt' => 'bank' ),
            array( 'db' => 'created_at', 'dt' => 'created_at' ),
            array( 'db' => 'amount', 'dt' => 'amount' ),
            array( 'db' => 'description', 'dt' => 'description' ),
        );
        
        return ServerSide::simple( $request, $query, $primaryKey, $columns );
    }

   

    public function printPdfToday($request)
    {
        $date = Carbon::now();
        $date = $date->format('d/m/Y H:i:s');
        $user = \JWTAuth::parseToken()->toUser();
        $fileName = uniqid();
        $routeFile = '';  
        $query = $this->paymentsToday($request);
        $totalS = $this->Today($request);
        $total = $totalS[0]->suma;
        $data = \DB::select( $query );
        if ($request->type === 'pdf') {
            $routeFile = Utility::buildPdf(
                json_decode(json_encode($data), true),
                compact('date', 'total'), 
                'todaypdf', 
                35, 
                'today', 
                $fileName
            );
        }        
       
        return $routeFile;
    }


}