<?php
namespace App\Repository\Company;
use App\AnalystManager;
use App\User;
use App\Company;
use App\Profile;
use App\BankCompany;
use Ultraware\Roles\Models\Role;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repository\Base\BaseRepository;
use App\Utils\Enums\AuditOperation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
/**
 * Class CompanyRepo.
 *
 * @package App\Repository\Company
 * @author  <info@expertosdelaweb.net> 
 */
class CompanyRepo extends BaseRepository
{
    /**
     * CompanyRepo construct.
     * 
     * @param Company $model
     * @return void
     */
    public function __construct( Company $model )
    {
        parent::__construct( $model );
    }
    /**
     * Save company.
     *
     * @param $request
     * @return $user
     */
    public function save( $request )
    {   


        $users = JWTAuth::parseToken()->authenticate();
       
        if (empty($request['data']['id'])) {
            //creamos la empresa
            $company = new Company;
            // Creamos el usuario
            $user = new User;
            // Creamos el perfil
            $perfil = new Profile;
          
        } else {
            // Obtenemos los datos
           
            $company = Company::find($request['data']['id']);
            $user = User::find($request['data']['user']['id']);
            $perfil = Profile::where('user_id', '=', $user->id)->firstOrFail();
            
        }

        $rol = Role::where( 'id', 1 )->first();
        $company->name =$request['data']['name'];
        $company->status = true;
        $company->save();
        
        $user->email = $request['data']['user']['email'];
        $user->company_id =  $company->id;
        $user->status = true;
        $user->role_id = 1;


        // Agregamos los datos
        if (empty($request['data']['id']) && isset($request['data']['password']) || isset($request['data']['password'])) {
            $user->password = bcrypt($request['data']['password']);
           
        }
        $user->save();
       
        // Estipulamos los datos del perfil
        $perfil->name = ucwords($request['data']['user']['profile']['name']);
        $perfil->lastname = ucwords($request['data']['user']['profile']['lastname']);
        $perfil->ci = $request['data']['user']['profile']['ci'];
        $perfil->user_id = $user->id;
        $perfil->save();
       
        $user->attachRole( $rol );
        // CREAMOS LA AUDITORIA.
       

        return $user;
    }
    /**
     * association BankCompany.
     *
     * @param $request
     * @return $company
     */
    public function association( $request )
    {
        $company = new BankCompany;
        $company->bank_id= $request->bank;
        $company->status= true;
        $company->company_id = $request ->company;
        $company->save();
        return $company;

        
    }
    /**
     * statusBank Company.
     *
     * @param array $data, $value, $attribute = 'id'
     * @return $this->model->where( $attribute, '=', $value )->update( $data )
     */
    public function statusBank( array $data, $value, $attribute = 'id' )
    {

        return BankCompany::where( $attribute, '=', $value )->update( $data );
    }
        /**
     * verification BankCompany.
     *
     * @param $request
     * @return $data
     */
    public function verification( $request )
    {
        $data=BankCompany::where( 'bank_id', $request->bank)
                            ->where('company_id', $request ->company)
                            ->first();
        return $data;
    }
        /**
     * verificationExit BankCompany.
     *
     * @param $request
     * @return $data
     */
    public function verificationExit( $request )
    {
        $data=BankCompany::where( 'bank_id', $request->bank)
                            ->where('company_id', $request ->company)
                            ->where('id', '<>', $request->id)
                            ->first();
        return $data;
    }
      /**
     * verificationExitName Company.
     *
     * @param $request
     * @return $data
     */
    public function SearchName( $request )
    {
        $data=Company::where( 'name', $request['data']['name'])
                            ->first();
        return $data;
    }
     
    /**
     * statusVerification Company.
     *
     * @param $user
     * @return $data
     */
    public function statusVerification( $user)
    {
        $data=Company::where( 'id', $user)
                            ->where('status', 0)
                            ->first();
        return $data;
    }

    /**
     * Status Company.
     *
     * @param array $data, $value, $attribute = 'id'
     * @return $this->model->where( $attribute, '=', $value )->update( $data )
     */
    public function status( array $data, $value, $attribute = 'id' )
    {
        return $this->model->where( $attribute, '=', $value )->update( $data );
    }
     /**
     * updateAssociation company.
     *
     * @param $request
     * @return $company
     */
    public function updateAssociation( $request )
    {   
        $company= BankCompany::find($request->id);
              
        $company->company_id =$request->company;
        $company->bank_id = $request->bank;
        $company->save();
      
            return $company;
    }

}