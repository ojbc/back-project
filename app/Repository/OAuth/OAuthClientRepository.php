<?php
namespace App\Repository\OAuth;
use Laravel\Passport\Client;
use App\Repository\Base\BaseRepository;
/**
 * Class OAuthClientRepository.
 *
 * @package App\Repository\OAuth
 * @author  <info@expertosdelaweb.net> 
 */
class OAuthClientRepository extends BaseRepository
{
    /**
     * OAuthClientRepository construct.
     * 
     * @param Client $model
     * @return void
     */
    public function __construct( Client $model )
    {
        parent::__construct( $model );
    }
}