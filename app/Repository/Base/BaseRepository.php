<?php

namespace App\Repository\Base;

use Illuminate\Database\Eloquent\Model;

/**
 * Abstract Class BaseRepository
 *
 * Class that defines an abstract behavior of the repository pattern in the project
 *
 * @package App\Repository\Base
 */
abstract class BaseRepository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct( Model $model )
    {
        $this->model = $model;
    }

    /**
     * Create a new Row data.
     *
     * @param array $data
     * @return Model
     */
    public function create( array $data )
    {
        return $this->model->create( $data );
    }

    /**
     * Update Profile.
     *
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute="id")
    {
        return $this->model->where($attribute, '=', $id)->update($data);
    }

    /**
     * All Data.
     *
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all($columns = array('*'), $order = 'DESC', $keyOrder = 'id')
    {
        $attrsColumns = count( $this->model->getFillable() )
            ? $this->model->getFillable()
            : $columns;

        return $results = $this->model->orderBy($keyOrder, $order)->get($attrsColumns);
    }


    public function allWith( $data = array(''), $order = 'DESC' )
    {       
        return $this->model->with( (array) $data )->orderBy('id', $order);
    }

    /**
     * Find Model Data By Id.
     *
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find( $id, $columns = array('*') ) 
    {
        return $this->model->find( $id, $columns );
    }

    /**
     * Find Model Data By Id.
     *
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function findWith( $id, $with = [] ) 
    {
        return $this->model->where( 'id', $id )->with($with)->first();
    }

    /**
     * Find First data by field.
     *
     * @param $field
     * @param null $value
     * @param array $columns
     * @return mixed
     */
    public function findFirstByFiled( $field, $value = null, $columns=['*'] )
    {
        return $this->model->where( $field, '=', $value )->first( $columns );
    }

    /**
     * Find data by field.
     *
     * @param $field
     * @param null $value
     * @return mixed
     */
    public function findByField( $field, $value = null )
    {
        return $this->model->where( $field, '=', $value );
    }

    /**
     * Is Exists Row data.
     *
     * @param $id
     * @return bool
     */
    public function isExist( $id )
    {
        return $this->find( $id ) ? TRUE : FALSE;
    }

    /**
     * Destroy Element Apply Soft Deletes.
     * 
     * @param int $id
     * @param string $attribute
     * @return mixed 
     */
    public function destroy( $id, $attribute = 'id' )
    {

        return $this->model->where( $attribute, '=', $id )->delete();
    }

    /**
     * Restore Element With Soft Deletes.
     * 
     * @param int $id
     * @param string $attribute
     * @return mixed 
     */
    public function restore( $id, $attribute = 'id' )
    {
        return $this->model->where( $attribute, '=', $id )->restore();
    }


    public function last()
    {
        return $this->model->last();
    }


    public function removeAccent($string)
    {
        $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );


        $str = strtr( $string, $unwanted_array );

        return $str;

    }
}