<?php
namespace App\Repository\Preload;
use App\AnalystManager;
use App\User;
use App\Preload;
use App\DataBankTransaction;
use Ultraware\Roles\Models\Role;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Repository\Base\BaseRepository;
use App\Utils\Enums\AuditOperation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Payment;
use App;
use View;
use iio\libmergepdf\Merger;
use Symfony\Component\Finder\Finder;
use Illuminate\Support\Facades\File;
use carbon\carbon;
use App\Utils\ServerSide;
use App\Utils\Utility;
/**
 * Class PreloadRepo.
 *
 * @package App\Repository\Preload
 * @author  <info@expertosdelaweb.net> 
 */
class PreloadRepo extends BaseRepository
{
    /**
     * PreloadRepo construct.
     *
     * @param Preload $model
     * @return void
     */
    public function __construct(Preload $model)
    {
        parent::__construct($model);
    }

    /**
     * Save Preload.
     *
     * @param $request
     * @return $pay
     */
    public function save($request)
    {
        $pay = new Preload;
        $pay->user_id = $request->user_id;
        $pay->analyst_id = $request->user_id;
        $pay->form_pay = 0;
        $pay->reference = $request->reference;
        $pay->bank = $request->bank;
        $pay->voucher_date = $request->voucher_date;
        $pay->amount = (Float)$request->amount;
        $pay->company_id = $request->company_id;
        $pay->description = $request->description;
        $pay->save();
        return $pay;
    }

    /**
     * deletePreload Preload.
     *
     * @param $request
     * @return $data
     */
    public function deletePreload($request)
    {
       

        $data =   DB::table('preloads')
                ->where('company_id', $request['company_id'])
                ->delete();
            
        
        return $data;
    }

    /**
     * checkPreload Preload.
     *
     * @param $request
     * @return $data
     */
    public function checkPreload($request)
    {
        $apro = [];
        $npro = [];
        $data = [];
        $cont1 = 0;
        $cont2 = 0;
        $query = $this->queryPreloads($request); 
        $consult = \DB::select( $query );
        foreach ($consult as $k => $item) {
           
            $cont1 = $cont1 + 1;
            $dataBanksTransactions = DB::table('data_banks_transactions')->select('id')
                ->where('name_bank', (string) $item->bank)
                ->where('transaction_date', $item->voucher_date)
                ->where('reference', (string) $item->reference)
                ->where('amount',  (float) $item->amount)
                ->where('company_id', $item->company_id)
                ->where('locked', false)
                ->where('status', false)
                ->first();

            if (!isset($dataBanksTransactions)) {

                   $npro[$cont1] = $item;



            }else{

                    $user = \JWTAuth::parseToken()->toUser();
                    $rol = DB::table('role_user')
                    ->where('user_id', $user->id)
                    ->first();
                    $Payment = new Payment;
                    $Payment->user_id = $user->id;
                    $Payment->analyst_id =  $rol->id;
                    $Payment->form_pay = 0;
                    $Payment->reference = $item->reference;
                    $Payment->bank  = $item->bank;
                    $Payment->voucher_date = $item->voucher_date;
                    $Payment->amount = (float) $item->amount;
                    $Payment->company_id =$item->company_id;
                    $Payment->description =  $item->description;
                    $Payment->save();
                    $bank = DataBankTransaction::find( (int) $dataBanksTransactions->id );
                    $bank->status = true;
                    $bank->save();
                     DB::table('preloads')
                        ->where('id', $item->id)
                        ->delete();
                    $apro[$cont1] = $item;
            }

        }
        return  ['aprobados'=>$apro, 'nprobados'=>$npro];
    }
    /**
     * printPdfap Preload
     * Generate a PDF
     * @param $request
     * @return $url
     */
    public function printPdfap($request)
    {
        $limiRegister = 1000;
        $mkdirName = [];
        $sPdf = [];
        $prom = 0;
        $total =0;

        $arraytest =[];
       
        $acountProduct = 0;
      
        $invoiceProduct = $request->table[0];
        
        $date = $request->date;
        $hour = $request->hour;
        $user = \JWTAuth::parseToken()->toUser();
        $fileName = uniqid();
        $routeFile = '';  
        $routeFile = Utility::buildPdf(
            $invoiceProduct,
            compact('date', 'hour'), 
            'approved', 
            35, 
            'approved', 
            $fileName
        );
        return $routeFile;
    }
    /**
     * printPdfnp Preload
     * Generate a PDF
     * @param $request
     * @return $url
     */
    public function printPdfnp($request)
    {
       $limiRegister = 1000;
        $mkdirName = [];
        $sPdf = [];
        $prom = 0;
        $total =0;

        $arraytest =[];
       
        $acountProduct = 0;
      
        $invoiceProduct = $request->table[0];
        
        $date = $request->date;
        $hour = $request->hour;
        $user = \JWTAuth::parseToken()->toUser();
        $fileName = uniqid();
        $routeFile = '';  
        $routeFile = Utility::buildPdf(
            $invoiceProduct,
            compact('date', 'hour'), 
            'napproved', 
            35, 
            'napproved', 
            $fileName
        );
        return $routeFile;
    }

    /**
     * deletelist Preload.
     *
     * @param $request
     * @return $data
     */
    public function deletelist($request)
    {
            $data=DB::table('preloads')
                ->where('id', $request->id)
                ->delete();

        return $data;
    }

    public function queryPreloads($request)
    {
        
        if (isset($request['company_id'])){
           return "
            SELECT 
                `preloads`.id, 
                `preloads`.reference, 
                `preloads`.bank,
                `preloads`.voucher_date,
                `preloads`.amount,
                `preloads`.description,
                `preloads`.company_id
            FROM `preloads` 
            WHERE `preloads`.company_id = {$request['company_id']}";
            
          }else{
                return  "
                SELECT 
                `preloads`.id, 
                `preloads`.reference, 
                `preloads`.bank,
                `preloads`.voucher_date,
                `preloads`.amount,
                `preloads`.description,
                `preloads`.company_id
            FROM `preloads` 
            WHERE `preloads`.company_id =0";
          }
    }

    /**
     *  paginate PRELOADS.
     *
     * @param
     * @return bodyResponseRequest $data
     */
    public function paginate($request)
    {
        
        $primaryKey = 'id';
        $query = $this->queryPreloads($request); 
         
        $columns = array(
            array( 'db' => 'reference', 'dt' => 'reference' ),
            array( 'db' => 'bank', 'dt' => 'bank' ),
            array( 'db' => 'voucher_date', 'dt' => 'voucher_date' ),
            array( 'db' => 'amount', 'dt' => 'amount' ),
            array( 'db' => 'description', 'dt' => 'description' ),
            array( 'db' => 'id', 'dt' => 'id' ),
        );
        
        return $data = ServerSide::simple( $request, $query, $primaryKey, $columns );

       
    }
}
