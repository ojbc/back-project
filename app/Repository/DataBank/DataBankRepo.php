<?php
namespace App\Repository\DataBank;
use App\DataBank;
use App\Bank;
use App\Repository\Base\BaseRepository;
use App\Repository\DataBankTransaction\DataBankTransactionRepo;
use PhpParser\Node\Scalar\String_;
use App\DataBankTransaction;
use App\Utils\Enums\BankEnumData;
use App\Utils\Enums\CommercialEnumData;
use App\Utils\Enums\EnumResponse;
use App\Utils\Enums\InventoryError;
use App\Utils\Enums\MonthSofitasa;
use App\Utils\Utility;
use Carbon\Carbon;
use Excel;
/**
 * Class DataBankRepo.
 *
 * @package App\Repository\DataBank
 * @author  <info@expertosdelaweb.net>
 */
class DataBankRepo extends BaseRepository
{
    /**
     * @var DataBankTransactionRepo $dataBankTransactionRepo
     */
    private $dataBankTransactionRepo;

    /**
     * DataBankRepo construct.
     *
     * @param DataBank $model
     * @param DataBankTransactionRepo $dataBankTransactionRepo
     * @return void
     */
    public function __construct(DataBank $model, DataBankTransactionRepo $dataBankTransactionRepo)
    {
        parent::__construct($model);
        $this->dataBankTransactionRepo = $dataBankTransactionRepo;
    }

    /**
     * Transaction DataBank
     * This function processing data bank.
     * @param  array $dataBank
     * @param  array $details
     * @return $bankInformation->values()->all()
     */
    public function transaction(array $dataBank)
    {
        $bankInformation = collect();

        $resultDataBank = $this->create([
            'owner_user' => $dataBank['owner_user'],
            'bank_id' => $dataBank['bank_id'],
            'bank_description' => $dataBank['bank_description'],
            'company_id' => $dataBank['company_id']
        ]);

        return $resultDataBank ;
    }
    /**
     * GetTransactionByReference DataBank
     * This function queries a record by its reference.
     * @param  string $reference
     * @return mixed
     */
    public function getTransactionByReference($reference)
    {
        return $this->model->select(
            'data_banks_transactions.id',
            'data_banks.bank_description as bank',
            'data_banks_transactions.transaction_date',
            'data_banks_transactions.reference',
            'data_banks_transactions.amount',
            'data_banks_transactions.description',
            'data_banks_transactions.locked',
            'data_banks_transactions.status'
        )
            ->join('data_banks_transactions', 'data_banks.id', '=', 'data_banks_transactions.databank_id')
            ->where('data_banks_transactions.reference','like', $reference.'%')
            ->get();
    }
}
