<?php

namespace App\Repository\DataBank\Collection;

use App\Utils\Enums\BankEnumData;
use App\Utils\Enums\CommercialEnumData;
use App\Utils\Enums\EnumResponse;
use App\Utils\Enums\InventoryError;
use App\Utils\Enums\MonthSofitasa;
use App\Utils\Utility;
use Carbon\Carbon;
use Excel;

class BankCollection
{
    /**
     * @var $data
     */
    private $fileUpload = true;

    /**
     * Build a collection for the Mercantil bank.
     *
     * @param  array $lines
     * @author Emilio Hernandez <[<emilio25informatic@gmail.com>]>
     * @return array
     */
    public function buildCommercialBankInformation($lines)
    {
        $bankInformation = collect();

        foreach ($lines as $line) {
            $condition = substr(
                $line,
                CommercialEnumData::START_CONDITION,
                CommercialEnumData::END_CONDITION
            );

            $day = substr($line, 26, 2);
            $month = substr($line, 28, 2);
            $year = substr($line, 30, 4);
            $date = $day . '-' . $month . '-' . $year;
            $reference = str_pad(substr($line, 37, 9), 9, '0', STR_PAD_LEFT);
            $amount = 0;
            $information = '';

            switch ($condition) {
                case CommercialEnumData::DP:
                    $stringConsulted = substr($line, 50);

                    $existenceOfTheWordCheck = strpos($stringConsulted, CommercialEnumData::CHECK);

                    if ($existenceOfTheWordCheck === false) {
                        $initialPosition = CommercialEnumData::INITIAL_POSITION_WITHOUT_CHECK;
                        $information = substr($line, 50, 20);
                    } else {
                        $existenceOfTheWordCash = strpos($stringConsulted, CommercialEnumData::CASH);

                        if ($existenceOfTheWordCash === false) {
                            $initialPosition = CommercialEnumData::INITIAL_POSITION_WITHOUT_CASH;
                            $information = substr($line, 50, 18);
                        } else {
                            $initialPosition = CommercialEnumData::INITIAL_POSITION_WITH_CASH;
                            $information = substr($line, 50, 29);
                        }
                    }

                    $stringConsulted = substr($line, $initialPosition);
                    $dividedString = explode(' ', $stringConsulted);
                    $amount = $dividedString[0];

                    break;
                case CommercialEnumData::NC:
                    $stringConsulted = substr($line, 80);
                    $dividedString = explode(' ', $stringConsulted);
                    $amount = $dividedString[0];
                    $information = substr($line, 50, 28);

                    break;
            }

            $amount = Utility::tofloat($amount);

            if ($amount > 0) {
                $bankInformation->push([
                    'transaction_date' => $date,
                    'reference' => $reference,
                    'description' => $information,
                    'amount' => $amount
                ]);
            }
        }

        return $bankInformation->values()->all();
    }

    /**
     * Returns processed excel.
     *
     * @param  array $file
     * @param  string $bank
     * @author Emilio Hernandez <[<emilio25informatic@gmail.com>]>
     * @return array
     */
    public function buildInformationWithExcel($file, $bank)
    {
        $bankData = null;

        $excelData = Excel::load($file, function ($reader) use ($bank) {
            
            $reader->setDateFormat('d-m-Y');
            $reader->skipRows(13);

         $reader->noHeading();
            if ($bank === BankEnumData::EXTERIOR) {
                $reader->noHeading();
            }
        })->all();
      
       // dd($excelData);
        $bankInformation = collect();
    
        foreach ($excelData as $values) {
           /* if (!count($values)) {
                $this->fileUpload = false;

                return [];
            }*/

         // dd($excelData);
            foreach ($values as $value) {
                //$additional = $bank === BankEnumData::SOFITASA ? $values[2] : null;
                $additional = null;
                $data = $this->buildArrangedForTheBank($bank, $value, $additional);
                
                if (count($data)) {
                    $bankInformation->push($data);
                }
            }
        }

        return $bankInformation->values()->all();
    }

    /**
     * Builds the array of information needed for the given bank.
     *
     * @param  string $bank
     * @param  string $value
     * @author Emilio Hernandez <[<emilio25informatic@gmail.com>]>
     * @return array
     */
    public function buildArrangedForTheBank($bank, $value, $additional = null)
    {
      
        $bankInformation = [];
       
        switch ($bank) {
            case BankEnumData::BANESCO:
                $bankInformation = [
                    'data_bank' => $bank,
                    'transaction_date' => $value->fecha,
                    'reference' => $value->referencia,
                    'description' => $value->descripcion,
                    'amount' => $value->monto
                ];

                break;
            case BankEnumData::PROVINCIAL:
                if ($value[5] > 0 && !is_null($value[0]) && $value[0] != '') {
                    $bankInformation = [
                        'data_bank' => $bank,
                        'transaction_date' => $value[0],
                        'reference' => $value[3],
                        'description' => $value[4],
                        'amount' => $value[5]
                    ];
                }

                break;
            case BankEnumData::PROVINCIAL_PERU:
            $amount_t = str_replace('.', ',', $value[2]);
            $amount = Utility::tofloat($amount_t);
            $reference = trim($value[4]);
            $titulo = str_slug($reference, "");

            
                if ($value[2] > 0 && !is_null($value[0]) && $value[0] != '') {
                    $bankInformation = [

                        'name_bank' => $bank,
                        'transaction_date' => $value[0],
                        'reference' => $titulo,
                        'description' => $value[1],
                        'amount' => $amount
                    ];
                  
                }

                break;
            case BankEnumData::BANCOACTIVO:

                $amount = Utility::tofloat($value[6]);

                if ($amount > 0) {
                    $bankInformation = [
                        'data_bank' => $bank,
                        'transaction_date' => $value[0],
                        'reference' => $value[3],
                        'description' => $value[2],
                        'amount' => $amount
                    ];
                }

                break;
            case BankEnumData::BICENTENARIO:
                $amount_t = str_replace('.', ',', $value[6]);

                $amount = Utility::tofloat($amount_t);

                if ($amount > 0) {
                    $bankInformation = [
                        'data_bank' => $bank,
                        'transaction_date' => $value[2],
                        'reference' => $value[3],
                        'description' => $value[4],
                        'amount' => $amount
                    ];
                }

                break;
            case BankEnumData::BFC:
                $amount = Utility::tofloat($value[4]);

                if ($amount > 0) {
                    $bankInformation = [
                        'data_bank' => $bank,
                        'transaction_date' => str_replace('/', '-', $value[0]),
                        'reference' => $value[2],
                        'description' => $value[3],
                        'amount' => $amount
                    ];
                }

                break;
            case BankEnumData::CIEN_BANCO:
                $amount = Utility::tofloat($value[3]);

                if ($amount > 0) {
                    $bankInformation = [
                        'transaction_date' => $value[0],
                        'reference' => $value[1],
                        'description' => $value[2],
                        'amount' => $amount
                    ];
                }

                break;
            case BankEnumData::EXTERIOR:
                $symbol = $value[4];

                if ($symbol === '+') {
                    $amount = Utility::tofloat($value[3]);

                    if ($amount > 0) {
                        $dividedString = explode('/', $value[1]);

                        $day = str_pad($dividedString[0], 2, '0', STR_PAD_LEFT);
                        $month = str_pad($dividedString[1], 2, '0', STR_PAD_LEFT);
                        $year = str_pad($dividedString[2], 4, '20', STR_PAD_LEFT);

                        $date = $day.'-'.$month. '-'.$year;

                        $bankInformation = [
                            'transaction_date' => $date,
                            'reference' => $value[2],
                            'description' => $value[0],
                            'amount' => $amount
                        ];
                    }
                }

                break;
            case BankEnumData::VENEZUELA:
                if ($value[7] !== 'ND') {
                    $str = (string)$value[5];

                    $strSize = strlen($str);
                    $wholePart = substr($str, 0, $strSize - 2);
                    $decimalPart = substr($str, $strSize - 2, 2);

                    $amountToFormat = $wholePart . ',' . $decimalPart;

                    $amount = Utility::tofloat($amountToFormat);

                    if ($amount > 0) {
                        $fileDate = $value[1];

                        $day = substr($fileDate, 0, 2);
                        $month = substr($fileDate, 2, 2);
                        $year = substr($fileDate, 4, 4);

                        $date = $day.'-'.$month.'-'.$year;

                        $bankInformation = [
                            'transaction_date' => $date,
                            'reference' => (string)$value[2],
                            'description' => $value[3],
                            'amount' => $amount
                        ];
                    }
                }

                break;
            case BankEnumData::CARONI:
                if ($value[3] !== "WC" || $value[2] > 0) {
                    $amount = Utility::tofloat($value[5]);

                    if ($amount > 0) {
                        $date = str_replace('/', '-', $value[1]);
                        $description = trim($value[0]);

                        $bankInformation = [
                            'transaction_date' => $date,
                            'reference' => (string)$value[2],
                            'description' => $description,
                            'amount' => $amount
                        ];
                    }
                }
                break;
            case BankEnumData::BOD:
                if ($value[0] !== 'NA' && !is_null($value[0]) && $value[0] !== 'Fecha') {
                    if ($value[4] > 0) {
                        $date = str_replace('/', '-', $value[0]);
                        $reference = trim($value[2]);
                        $description = trim($value[3]);
                        $amount = Utility::tofloat($value[4]);

                        $bankInformation = [
                            'transaction_date' => $date,
                            'reference' => $reference,
                            'description' => $description,
                            'amount' => $amount
                        ];
                    }
                }

                break;
            case BankEnumData::BNC:

                if ($value[7] > 0 && !is_null($value[1]) && $value[1] != '') {

                    $bankInformation = [
                        'transaction_date' => $value[0],
                        'reference' => $value[1],
                        'description' => $value[5],
                        'amount' => $value[7]
                    ];

                }

                break;
            case BankEnumData::SOFITASA:
                if (!is_null($additional)) {
                    $stringBalance = explode(':', $additional[0]);

                    $resultString = trim($stringBalance[1]);

                    foreach (MonthSofitasa::$list_months_sofitasa as $key => $month) {
                        $existenceOfMonth = strpos($resultString, $month);

                        if ($existenceOfMonth !== false) {
                            $month = $key + 1;

                            break;
                        }
                    }

                    if ($value[4] > 0 && !is_null($value[2]) && ($value[2] === 'NC' || $value[2] === 'DP')) {
                        $date = Carbon::now();

                        $day = str_pad((string)$value[0], 2, '0', STR_PAD_LEFT);
                        $month = str_pad((string)$month, 2, '0', STR_PAD_LEFT);
                        $date = $day . '-' . $month . '-' . $date->year;

                        $bankInformation = [
                            'transaction_date' => (string)$date,
                            'reference' => $value[1],
                            'description' => $value[1],
                            'amount' => $value[4]
                        ];
                    }
                }

                break;
        }

        return $bankInformation;
    }

    /**
     * Returns the message if the file is invalid.
     *
     * @param  string $bank
     * @author Emilio Hernandez <[<emilio25informatic@gmail.com>]>
     * @return mixed
     */
    public function makeValidation($bank)
    {
        return responseRequest(
            EnumResponse::CUSTOM_FAILED,
            sprintf('Error al leer carga masiva de %s',
                $bank
            ),
            InventoryError::CUSTOM_ERROR_MESSAGE
        );
    }

    /**
     * Returns attribute fileUpload.
     *
     * @author Emilio Hernandez <[<emilio25informatic@gmail.com>]>
     * @return bool
     */
    public function getFileUpload()
    {
        return $this->fileUpload;
    }
}
