<?php
namespace App\Repository\Bank;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Bank;
use App\Repository\Base\BaseRepository;
use App\Utils\Enums\AuditOperation;
use Illuminate\Support\Facades\Auth;
use App\BankCompany;
/**
 * Class BankRepository.
 *
 * @package App\Repository\Bank
 * @author  <info@expertosdelaweb.net> 
 */
class BankRepository extends BaseRepository
{
    /**
     * BankRepository construct.
     * 
     * @param Bank $model
     * @return void
     */
    public function __construct( Bank $model )
    {
        parent::__construct( $model );
    }
    /**
     * all Bank.
     *
     * @param $request
     * @return $bank
     */
    public function save( $request )
    {
        $data=BankCompany::where('company_id', $request->company_id)
        ->with('Banks')
        ->get();
        return $data;
    }
}