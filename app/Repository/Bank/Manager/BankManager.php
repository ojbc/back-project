<?php

namespace App\Repository\Bank\Manager;

use App\Utils\BaseManager;
use Illuminate\Validation\Rule;
/**
 * Class BankManager
 *
 * @package App\Repository\Bank\Manage;
 * @author Alejandro Pérez <alejandroprz2011@gmail.com>
 */
class BankManager extends BaseManager
{
     /**
     * BankManager constructor.
     *
     * @param array|mixed $data
     */
    public function __construct($data)
    {
        parent::__construct($data);
    }

    /**
     * @inheritdoc
     */
    protected function onValidate()
    {
        return [
            'nombre'  =>  [
                'required',
                'unique:banks'
            ],
            'cuenta'  =>  'required'
        ];
    }
}