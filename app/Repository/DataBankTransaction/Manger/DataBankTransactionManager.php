<?php

namespace App\Repository\DataBankTransaction\Manager;

use App\Utils\BaseManager;
use Illuminate\Validation\Rule;
/**
 * Class DistributorManager
 *
 * @package App\Repository\DataBankTransaction\Manage;
 */
class DataBankTransactionManager extends BaseManager
{
    /**
     * DistributorManager constructor.
     *
     * @param array|mixed $data
     */
    public function __construct($data)
    {
        parent::__construct($data);
    }

    /**
     * @inheritdoc
     */
    protected function onValidate()
    {
        return [
            'amount'  =>  'required',
            'reference'  =>  'required',
            'bank'  =>  'required',
            'today'  =>  'required'
        ];
    }
}