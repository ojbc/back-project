<?php
namespace App\Repository\DataBankTransaction;
use App\DataBankTransaction;
use App\Repository\Base\BaseRepository;
use Illuminate\Support\Facades\DB;
/**
 * Class DataBankTransactionRepo.
 *
 * @package App\Repository\DataBankTransaction
 * @author  <info@expertosdelaweb.net> 
 */
class DataBankTransactionRepo extends BaseRepository
{
    /**
     * DataBankTransactionRepo construct.
     *
     * @param DataBankTransaction $model
     * @return void
     */
    public function __construct(DataBankTransaction $model)
    {
        parent::__construct($model);
    }

    /**
     * Create DataBankTransaction
     * This function creates a new record in the table data_banks_transactions.
     * @param  array $transaction
     * @return $detail
     */
    public function create(array $transaction)
    {
        $detail = new $this->model;
        $detail->name_bank = $transaction['name_bank'];
        $detail->transaction_date = $transaction['transaction_date'];
        $detail->reference = $transaction['reference'];
        $detail->amount = $transaction['amount'];
        $detail->description = $transaction['description'];
        $detail->locked = false;
        $detail->status = false;
        $detail->company_id = $transaction['company_id'];
        return $detail;
    }
    /**
     * CheckExistence DataBankTransaction
     * This function verifies the existence of a record in the table.
     * @param  array $transaction
     * @return bool
     */
    public function checkExistence($request)
    {
        $dataBanksTransactions = $this->model->select('id')
            ->where('name_bank', $request->bank)
            ->where('reference',$request->reference)
            ->where('amount',$request->amount)
            ->where('transaction_date',(String) $request->today)
            ->where('status', 0)
            ->where('company_id', $request->company_id)
            ->first();
           
        return $dataBanksTransactions;
    }
    /**
     * CheckExistences DataBankTransaction
     * This function verifies the existence of a record in the table.
     * @param  array $transaction
     * @return bool
     */
    public function checkExistences($request)
    {
        $dataBanksTransactions = DB::table('data_banks_transactions')
            ->where('name_bank', (String)$request->bank)
            ->where('reference',(String)$request->reference)
            ->where('amount',(Float)$request->amount)
            ->where('transaction_date',(String) $request->today)
            ->where('company_id', $request->company_id)
            ->where('status', '=', '0')
            ->first(); 
        return $dataBanksTransactions;
    }
    /**
     * LockedUnlocked DataBankTransaction
     * This function lock / unLock register
     * @param int $id
     * @param string $option
     * @return mixed
     */
    public function lockedUnlocked($id, $option)
    {
        return $option === 'locked'
            ? $this->update(['locked' => true], $id)
            : $this->update(['locked' => false], $id);
    }
    /**
     * Save Bank.
     *
     * @param $request
     * @return $bank
     */
    public function save( $exit )
    {

        $bank = DataBankTransaction::find( (int) $exit->id );

        $bank->locked = true;
        $bank->save();

        return $bank;
    }
    public function Unlocked( $exit )
    {

        $bank = DataBankTransaction::find( (int) $exit->id );

        $bank->locked = false;
        $bank->save();

        return $bank;
    }
    public function verified($request)
    { 
        $bank = DataBankTransaction::find( (int) $request['idtransation'] );

        $bank->status = true;
        $bank->save();

        return $bank;
    }
    public function VerificaDeleteTransaction($request)
    {

    $dataBanksTransactions = DB::table('data_banks_transactions')
        ->where('name_bank', (String)$request->bank)
        ->where('transaction_date',(String) $request->today)
        ->where('company_id', $request->company)
        ->first();


    return $dataBanksTransactions;
    }
    public function deleteTransaction($request)
    {
        $alm = [];
        $data =[];
        $cont= 0;
        $dataBanksTransactions = DB::table('data_banks_transactions')
            ->where('name_bank', (String)$request->bank)
            ->where('transaction_date',(String) $request->today)
            ->where('company_id', $request->company)
            ->get();
        foreach ($dataBanksTransactions as $k =>  $item) {
            $cont = $cont + 1;
            $dataBanks = DB::table('data_banks_transactions')
                ->where('id', $item->id)
                ->delete();
            $alm[$cont]  = $dataBanks;
        }
        $data = [$alm];


        return $data;
    }

}