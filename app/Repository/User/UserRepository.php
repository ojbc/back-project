<?php
namespace App\Repository\User;
use App\User;
use App\Profile;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;
use Ultraware\Roles\Models\Role;
use App\Repository\Base\BaseRepository;
use App\Utils\Enums\AuditOperation;
use Illuminate\Support\Facades\Auth;
/**
 * Class UserRepository.
 *
 * @package App\Repository\User
 * @author  <info@expertosdelaweb.net> 
 */
class UserRepository extends BaseRepository
{
    /**
     * UserRepository construct.
     * 
     * @param User $model
     * @return void
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }
    /**
     * FindUsers User.
     *
     * @param array $params
     * @return $users->get()
     */
    public function findUsers(array $params)
    {
        $users =  $this->model->select(
            'users.id',
            'users.email',
            'profiles.name as names',
            'profiles.lastname as lastnames',
            'profiles.ci',
            'roles.name as rol_name'
        )
        ->join('profiles', 'users.id', '=', 'profiles.user_id')
        ->join('role_user', 'users.id', '=', 'role_user.user_id')
        ->join('roles', 'role_user.role_id', '=', 'roles.id');

        if (isset($params['email'])) {
           $users = $users->where('users.email', 'LIKE', '%' . $params['email'] . '%');
        }

        if (isset($params['role'])) {
            $users = $users->where('roles.id', $params['role']);
        }

        return $users->get();
    }
    /**
     * ChangeRole User.
     *
     * @param $request
     * @return $user
     */
    public function changeRole( $request )
    {
        // Obtenemos el usuario
        $user = User::find( $request->user );

        $old_role = $user->role();

        // Obtenemos el rol a establecer
        $rol = Role::where( 'slug', '=' , $request->role )->first();

        // CREAMOS LA AUDITORIA.
        auditSecurity( Auth::id(), AuditOperation::UPDATE, 'SECURITY.USERS', 'User', [ 'id' => $user->id, 'old' => $old_role, 'new' => $rol ]);

        // Eliminamos todos los roles del usuario
        $user->detachAllRoles();
        // Le añadimos el rol al usuario.
        $user->role_id = $rol->id;
        $user->attachRole( $rol );
        $user->save();

        // retornamos el usuario
        return $user;
    }
    /**
     * Save User.
     *
     * @param $request
     * @return $user
     */
    public function save( $request )
    {   
        $users = JWTAuth::parseToken()->authenticate();
        // Buscamos el rol.
        $rol = Role::where('slug', '=', $request->role['slug'])->first();
        // Creamos el usuario
        $user = new User;
        $user->email = $request->email;
        $user->password =  bcrypt($request->password);
        $user->company_id = $users->company_id;
        $user->role_id = $rol->id;
        $user->status = true;
        $user->save();
        // Creamos el perfil
        $perfil = new Profile;
        $perfil->name = ucwords($request->nombres);
        $perfil->lastname = ucwords($request->apellidos);
        $perfil->gender = $request->sexo === 'male' ? 0 : 1;
        $perfil->user_id = $user->id;
        $perfil->ci = $request->ci;
        $perfil->save();

        // Vinculamos el rol con el usuario
        $user->attachRole( $rol );
        
        // CREAMOS LA AUDITORIA.
        auditSecurity( Auth::id(), $users->company_id, AuditOperation::CREATE, 'SECURITY.USERS', 'User', [ 'id' => $user->id ]);

        return $user;
    }
    /**
     * ShowScalesManager User.
     *
     * @param 
     * @return mixed
     */
    public function showScalesManager()
    {
       return $this->model->select(
                'users.id as user_id',
                'users.email',
                'profiles.nombres as name',
                'profiles.apellidos as last_name',
                'scales.escala as scale',
                'campaigns.id as campaign_id',
                'campaigns.nombre as campaign_name',
                'assign_scale_camp_user.scale_id'
            )
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->join('assign_scale_camp_user', 'users.id', '=', 'assign_scale_camp_user.user_id')
            ->join('scales', 'assign_scale_camp_user.scale_id', '=', 'scales.id')
            ->join('campaigns', 'assign_scale_camp_user.campaing_id', '=', 'campaigns.id')
            ->get();
    }
    /**
     * CheckUserByRol User.
     * This function a ckeck user by rol.
     * @param 
     * @return boolean
     */
    public function checkUserByRol()
    {
        return true;
    }
    public function SaveUpdate($request)
    {
        // Buscamos el rol
        $users = JWTAuth::parseToken()->authenticate();
        // 3 => Gerentes
        
        // Verificamos si tenemos que crear o actualizar
        if (empty($request['id'])) {
            // Creamos el usuario
            $user = new User;
            // Creamos el perfil
            $perfil = new Profile;

        } else {
            // Obtenemos los datos
            $user = User::find($request['id']);
            $perfil = Profile::where('user_id', '=', $user->id)->firstOrFail();
        }

        // Estipulamos los datos del usuario
        $user->email = $request['email'];
        $user->company_id = $users->company_id;
        $user->status = true;



        // Agregamos los datos
        if (empty($request['id']) && isset($request['passwords']) || isset($request['passwords'])) {
            $user->password = bcrypt($request['passwords']);
            
        }
        $user->save();
        // Estipulamos los datos del perfil
        $perfil->name = ucwords($request['profile']['name']);
        $perfil->lastname = ucwords($request['profile']['lastname']);
        $perfil->gender = (int)$request['profile']['gender'];
        $perfil->ci = $request['profile']['ci'];
        $perfil->user_id = $user->id;
        $perfil->save();
        // Verificamos
        if (empty($request['id'])) {
            // CREAMOS LA AUDITORIA.
            auditSecurity(Auth::id(), $users->company_id, AuditOperation::CREATE, 'User', 'User', ['id' => $user->id]);
            auditSecurity(Auth::id(), $users->company_id, AuditOperation::CREATE, 'Profile', 'Profile', ['id' => $perfil->id]);
        } else {
            // CREAMOS LA AUDITORIA.
            auditSecurity(Auth::id(), $users->company_id, AuditOperation::UPDATE, 'User', 'User', ['id' => $user->id]);
            auditSecurity(Auth::id(), $users->company_id, AuditOperation::UPDATE, 'Profile', 'Profile', ['id' => $perfil->id]);
        }

        return $user;
    }
    
     /**
     * resetPassword User.
     *
     * @param $request
     * @return $user
     */
    public function resetPassword( $request )
    {   
        $user=User::find($request->id);
      
        $user->password = bcrypt($request->password);
        $user->save();
      
        return $user;
    }
   
     /**
     * Status User.
     *
     * @param array $data, $value, $attribute = 'id'
     * @return $this->model->where( $attribute, '=', $value )->update( $data )
     */
    public function status( array $data, $value, $attribute = 'id' )
    {
        return $this->model->where( $attribute, '=', $value )->update( $data );
    }
    /**
     * statusVerification User.
     *
     * @param $user
     * @return $data
     */
    public function statusVerification( $user)
    {
        $data=User::where( 'id', $user)
                            ->where('status', 0)
                            ->first();
        return $data;
    }

}
