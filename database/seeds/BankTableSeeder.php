<?php

use Illuminate\Database\Seeder;
use App\Bank;
class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {    
        Bank::create([
            'name' => 'Banco Activo',
            'account' => '0171'
        ]);
        Bank::create([
            'name' => 'Banesco',
            'account' => '0134'
        ]);
       
        Bank::create([
            'name' => 'BFC',
            'account' => '0151'
        ]);
        Bank::create([
            'name' => 'Bicentenario',
            'account' => '0175'
        ]);
        Bank::create([
            'name' => 'BNC',
            'account' => '0191'
        ]);
        Bank::create([
            'name' => 'BOD',
            'account' => '0116'
        ]);
        Bank::create([
            'name' => 'Caroni',
            'account' => '0128'
        ]);
        Bank::create([
            'name' => 'Exterior',
            'account' => '0115'
        ]);
        Bank::create([
            'name' => 'Mercantil',
            'account' => '0105'
        ]);
        Bank::create([
            'name' => 'Provincial',
            'account' => '0108'
        ]);
        Bank::create([
            'name' => 'Sofitasa',
            'account' => '0171'
        ]);
        Bank::create([
            'name' => 'Venezuela',
            'account' => '0102'
        ]);
        Bank::create([
            'name' => '100%Banco',
            'account' => '0156'
        ]);
        Bank::create([
            'name' => 'Plaza',
            'account' => '0138'
        ]);
        Bank::create([
            'name' => 'Tesoro',
            'account' => '0163'
        ]);
        Bank::create([
            'name' => 'Bancaribe',
            'account' => '0114'
        ]);
        Bank::create([
            'name' => 'Bancrecer',
            'account' => '0168'
        ]);
        Bank::create([
            'name' => 'Delsur',
            'account' => '0157'
        ]);
        Bank::create([
            'name' => 'Bancamiga',
            'account' => '0172'
        ]);
       
    }
}
