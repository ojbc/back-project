<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'SuperAdmin',
            'slug' =>'superadmin.sistema',
        ]);
        DB::table('users')->insert([
            'email' =>'ExpertosdelaWebs@gmail.com',
            'password' => bcrypt('12345'),
            'role_id' => 3,
            'status' => true
        ]);

        DB::table('profiles')->insert([
            'name' => 'Administrador',
            'lastname' =>' ',
            'gender' => '0', // 0:male, 1:female, 2:other
            'user_id' => 1,
        ]);
        DB::table('role_user')->insert([
            'role_id' => 3,
            'user_id' => 1,
        ]);

        DB::table('permissions')->insert([
            'name' => 'superadmin',
            'slug' => 'superadmin',
            'description' => 'Permiso para acceder al modulo super administrador'
        ]);

        DB::table('permission_role')->insert([
            'role_id' => 3,
            'permission_id' => 24
        ]);
    }
}
