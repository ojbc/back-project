<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_banks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('owner_user')->index()->unsigned();
            $table->integer('bank_id');
            $table->integer('company_id');
            $table->string('bank_description', 125);
            $table->foreign('owner_user', 'owneruserdatabank_fk')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
