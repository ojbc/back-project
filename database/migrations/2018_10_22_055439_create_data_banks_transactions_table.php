<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataBanksTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_banks_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('databank_id')->index()->unsigned();
            $table->string('name_bank', 40)->nullable();
            $table->string('transaction_date', 40)->nullable();
            $table->string('reference', 60)->nullable();
            $table->string('DNI', 60)->nullable();
            $table->double('amount', 15, 2)->nullable();
            $table->longText('description')->nullable();
            $table->integer('company_id');
            $table->boolean('locked');
            $table->boolean('status');
            $table->foreign('databank_id')->references('id')->on('data_banks')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
