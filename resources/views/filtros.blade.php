
    <?php  use Carbon\Carbon; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Verificapagos</title>
    <link rel="stylesheet" href="css/stylepdf.css" media="all" />
</head>
    <body>
      
        <body style="text-align: center;">
<table style="width:100%">
    <tr>

    <td>Fecha de impresion:{{ $dataCompact['date'] }}</td>
               

    </tr>
</table>
    <header  class="clearfix">

        <h1 align="center" >Reportes por filtros</h1>

    </header>
    
<table style="text-align: center;">
            <thead>
                <tr>
                    <th style="width:5%;">ID</th>
                    <th style="width:5%;">Referencia</th>
                    <th style="width:5%;">Analista</th>
                    <th style="width:5%;">Banco</th>
                    <th style="width:5%;">Fecha</th>
                    <th style="width:5%;">Monto</th>
                    <th  style=" width:5%;text-align: center;">Descripción</th>
                </tr>
            </thead>
            <tbody>
                @foreach($items as  $item)
                <tr>
                    <td style="text-align: center;">{{$item['id']}}</td>
                    <td style="text-align: center;">{{$item['reference']}}</td>
                    <td style="text-align: center;">{{$item['full_name']}}</td>
                    <td style="text-align: center;">{{$item['bank']}}</td>
                    <td style="text-align: center;">{{$item['created_at']}}</td>
                    <td style="text-align: center;">{{$item['amount']}}</td>
                    <td style="text-align: center;">{{$item['description']}}</td>

                </tr>
                @endforeach
                @if(($key+1)===($totalChunks))
                <tr>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;"></td>
                    <td style="text-align: center;">Total Monto:</td>
                    <td style="text-align: center;">{{$dataCompact['total']}}</td>
                    <td style="text-align: center;"></td>

                </tr>
                @endif
            </tbody>
        </table>
       
        
    </body>
</html>