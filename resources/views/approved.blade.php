<?php  use Carbon\Carbon; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Verificapagos</title>
    <link rel="stylesheet" href="css/stylepdf.css" media="all" />
</head>
<body style="text-align: center;">
<table style="width:100%">
    <tr>

    <td>Fecha de impresion:{{ $dataCompact['date'] }}</td>

    </tr>
</table


<header  class="clearfix">

    <h1 align="center" >Reporte de lista de pagos aprobados</h1>

</header>

<main>


<table  style="text-align: center;">
        <thead>
        <tr>
            <th style="width:5%;">Referencia</th>
            <th style="width:5%;">Banco</th>
            <th style="width:5%;">Fecha</th>
            <th style="width:5%;">Monto</th>
            <th style="width:5%;">Descripción</th>
        </tr>
        </thead>
        <tbody>
        @foreach($items as  $item)
                <tr>
                    <td style="text-align: center;">{{$item['reference']}}</td>
                    <td style="text-align: center;">{{$item['bank']}}</td>
                    <td style="text-align: center;">{{$item['voucher_date']}}</td>
                    <td style="text-align: center;">{{$item['amount']}}</td>
                    <td style="text-align: center;">{{$item['description']}}</td>

                </tr>
        @endforeach
        </tbody>
    </table>

    <center>
        <div id="notices">
            <div>NOTICIA:</div>
            <div class="notice">Este documento es su respaldo en caso de algun inconveniente.</div>
        </div>
    </center>
</main>
<footer>
    Todo los derechos reservados
</footer>

</body>
</html>