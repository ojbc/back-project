<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Verificapagos</title>
    <link rel="stylesheet" href="css/stylepdf.css" media="all" />
</head>
<body style="text-align: center;">
<table style="width:100%">
    <tr>

        <td>Fecha de impresion:{{$date}}</td>
        <td style="text-align: right;">Hora:{{$hour}}</td>

    </tr>
</table>



<header  class="clearfix">

    <h1 align="center" >Reportes por dia</h1>


</header>

<main>

    <table style="text-align: center;">
        <thead>
        <tr>
            <th style="width:5%;">Referencia</th>
            <th style="width:5%;">Banco</th>
            <th style="width:5%;">Fecha</th>
            <th style="width:5%;">Monto</th>

        </tr>
        </thead>
        <tbody>

        @foreach ($value as $invoice)
            <tr>
                <td class="service" style="text-align: center;"> {{$invoice['reference']}}</td>
                <td class="service" style="text-align: center;" >{{$invoice['bank'] }}</td>
                <td  class="service" style="text-align: center;" >{{$invoice['created_at'] }}</td>
                <td class="service" style="text-align: center;" >{{$invoice['amount'] }}</td>
            </tr>
        @endforeach
        <tr>
            <td class="service"></td>
            <td class="service"></td>
            <td class="service bg-color">Total</td>
            <td class="service bg-color">{{$totalInvo}}</td>

        </tr>
        </tbody>
    </table>

    <center>
        <div id="notices">
            <div>NOTICIA:</div>
            <div class="notice">Este documento es su respaldo en caso de algun inconveniente.</div>
        </div>
    </center>
</main>
<footer>
    Todo los derechos reservados
</footer>

</body>
</html>