<?php

Route::group(['middleware' => 'cors'], function() {
    Route::post('/access',[
        'as'=>'auth.access',
        'uses'=>'AuthController@auth'
    ]);

    Route::post('/revoke-access',[
        'as'=>'auth.revoke-access',
        'uses'=>'AuthController@logout'
    ]);
});