<?php

Route::post('/oauth2-payment-access',[
    'as'=>'customoauth2.oauth2-payment-access',
    'uses'=>'CustomOAuthController@getAuthorization'
]);

Route::post('/oauth2-revoke-access',[
    'as'=>'customoauth2.oauth2-revoke-access',
    'uses'=>'CustomOAuthController@revokeAuthorization'
]);