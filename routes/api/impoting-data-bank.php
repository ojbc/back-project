<?php
/**
 * Created by PhpStorm.
 * User: Desarrollo
 * Date: 22/10/2018
 * Time: 4:36 PM
 */

Route::group(['prefix' => 'importing-data-bank','middleware'=> ['jwt.auth', 'cors']], function () {

    Route::post('/processing', [
        'as' => 'app.importing-data-bank.processing',
        'uses' => 'ImportingDataBankController@processing'
    ]);

    Route::post('processing-data-bank', [
        'as' => 'importing-data-bank.processing-data-bank',
        'uses' => 'ImportingDataBankController@processingDataBank'
    ]);

    Route::get('get-transaction-log', [
        'as' => 'importing-data-bank.get-transaction-log',
        'uses' => 'ImportingDataBankController@getTransactionLog'
    ]);

    Route::post('lock-unlock-transaction/{id}', [
        'as' => 'importing-data-bank.post-lock-unlock-transaction',
        'uses' => 'ImportingDataBankController@lockUnlockData'
    ]);
    Route::get('/get-transaction', [
        'as' => 'importing-data-bank.get-transaction',
        'uses' => 'ImportingDataBankController@all'
    ]);
    Route::post('/lockUnLocked', [
        'as' => 'importing-data-bank.post-lockUnLocked',
        'uses' => 'ImportingDataBankController@lockUnLocked'
    ]);
    Route::post('/UnLocked', [
        'as' => 'importing-data-bank.post-UnLocked',
        'uses' => 'ImportingDataBankController@UnLocked'
    ]);
    Route::post('/deleteTransaction', [
        'as' => 'importing-data-bank.post-deleteTransaction',
        'uses' => 'ImportingDataBankController@deleteTransaction'
    ]);


});