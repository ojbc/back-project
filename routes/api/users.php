<?php
Route::group(['prefix' => 'user','middleware'=> ['cors']], function () {

    Route::post('find-users', [
        'as'=> 'users.find-users',
        'uses' => 'UsersController@findUsers'
    ]);
    Route::get('Profile', [
        'as'=> 'users.profile',
        'uses' => 'UsersController@profile'
    ]);
});