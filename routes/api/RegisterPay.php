<?php
Route::group(['prefix' => 'register-pay','middleware'=> ['cors']], function () {



    Route::get('/', [
        'as' => 'app.Payment.all',
        'uses' => 'PaymentController@all'
    ]);
    Route::get('/invoice', [
        'as' => 'app.Payment.invoice',
        'uses' => 'PaymentController@invoice'
    ]);
    Route::get('/paginate', [
        'as' => 'app.Payment.paginate',
        'uses' => 'PaymentController@paginate'
    ]);
    Route::post('/save', [
        'as' => 'app.Payment.save',
        'uses' => 'PaymentController@save'
    ]);
    Route::post('/referenceID', [
        'as' => 'app.Payment.referenceID',
        'uses' => 'PaymentController@referenceID'
    ]);
    Route::get('/payments', [
        'as' => 'app.Payment.payments',
        'uses' => 'PaymentController@payments'
    ]);
    Route::get('/referenceInfo', [
        'as' => 'app.Payment.referenceInfo',
        'uses' => 'PaymentController@referenceInfo'
    ]);
    Route::get('/todaysPaginates', [
        'as' => 'app.Payment.todaysPaginates',
        'uses' => 'PaymentController@todaysPaginates'
    ]);
    Route::get('/totalPaginates', [
        'as' => 'app.Payment.totalPaginates',
        'uses' => 'PaymentController@totalPaginates'
    ]);
   
    Route::post('/total', [
        'as' => 'app.Payment.total',
        'uses' => 'PaymentController@total'
    ]);
    Route::post('/today', [
        'as' => 'app.Payment.today',
        'uses' => 'PaymentController@today'
    ]);
    Route::post('/printPdf', [
        'as' => 'app.Payment.printPdf',
        'uses' => 'PaymentController@printPdf'
    ]);

    Route::post('/printPdfToday', [
      'as' => 'app.Payment.printPdfToday',
      'uses' => 'PaymentController@printPdfToday'
    ]);
    Route::post('/check', [
        'as' => 'app.Payment.check',
        'uses' => 'PaymentController@check'
    ]);
    Route::post('/searchVoucher', [
        'as' => 'app.Payment.searchVoucher',
        'uses' => 'PaymentController@searchVoucher'
    ]);
    Route::post('/searchCDN', [
        'as' => 'app.Payment.searchCDN',
        'uses' => 'PaymentController@searchCDN'
    ]);
    Route::post('/searchCDNData', [
        'as' => 'app.Payment.searchCDNData',
        'uses' => 'PaymentController@searchCDNData'
    ]);
    Route::post('/upData', [
        'as' => 'app.Payment.upData',
        'uses' => 'PaymentController@upData'
    ]);
    

});