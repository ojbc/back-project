<?php
/**
 * Created by PhpStorm.
 * User: Desarrollo
 * Date: 22/10/2018
 * Time: 4:35 PM
 */
Route::group(['prefix' => 'managers','middleware'=> ['jwt.auth', 'cors']], function () {

    Route::get('/all', [
        'as' => 'app.manager.all',
        'uses' => 'ManagerController@all'
    ]);



});