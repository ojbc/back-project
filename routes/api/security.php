<?php

Route::group(['prefix' => 'security','middleware'=> ['jwt.auth', 'cors']], function () {

    Route::group(['prefix' => 'roles'], function () {
        Route::get('/', [
            'as' => 'app.all-roles',
            'uses' => 'SecurityController@allRoles'
        ]);

        Route::get('/{slug}/permissions', [
            'as' => 'app.all-roles',
            'uses' => 'SecurityController@getPermissionByRole'
        ]);

        Route::post('/permission/toggle', [
            'as' => 'app.all-roles',
            'uses' => 'SecurityController@togglePermissionRole'
        ]);

        Route::post('/save', [
            'as' => 'app.save-roles',
            'uses' => 'SecurityController@saveRole'
        ]);
    });

    Route::group(['prefix' => 'permission'], function () {
        Route::get('/', [
            'as' => 'app.all-permission',
            'uses' => 'SecurityController@allPermission'
        ]);

        Route::post('/save', [
            'as' => 'app.save-permission',
            'uses' => 'SecurityController@savePermission'
        ]);
    });

    Route::group(['prefix' => 'consulting'], function () {
        Route::get('/', [
            'as' => 'app.all-consulting',
            'uses' => 'SecurityController@allConsulting'
        ]);
        Route::get('/paginate', [
            'as' => 'app.security.paginate',
            'uses' => 'SecurityController@paginate'
        ]);
    });
    Route::group(['prefix' => 'company'], function () {
        Route::post('/save', [
            'as' => 'app.company.save',
            'uses' => 'CompanyController@save'
        ]);
        Route::get('/alls', [
            'as' => 'app.company.alls',
            'uses' => 'CompanyController@alls'
        ]);
        Route::post('/association', [
            'as' => 'app.company.association',
            'uses' => 'CompanyController@association'
        ]);
        Route::get('/allUsers', [
            'as' => 'app.company.allUsers',
            'uses' => 'CompanyController@allUsers'
        ]);
        Route::get('/allassociation', [
            'as' => 'app.company.allassociation',
            'uses' => 'CompanyController@allassociation'
        ]);
        Route::post('/status', [
            'as' => 'app.company.status',
            'uses' => 'CompanyController@status'
        ]);
        Route::post('/statusBank', [
            'as' => 'app.company.statusBank',
            'uses' => 'CompanyController@statusBank'
        ]);
    });

    Route::group(['prefix' => 'users','middleware'=> ['jwt.auth', 'cors']], function () {
        Route::get('/', [
            'as' => 'app.users.all',
            'uses' => 'UsersController@all'
        ]);

        Route::get('/full-details', [
            'as' => 'app.users.full-details',
            'uses' => 'UsersController@getFullDetails'
        ]);

        Route::post('/save', [
            'as' => 'app.user.save',
            'uses' => 'UsersController@save'
        ]);

        Route::post('/role/change', [
            'as' => 'app.user.change-role',
            'uses' => 'UsersController@changeRole'
        ]);

        Route::post('/delete', [
            'as' => 'app.user.delete',
            'uses' => 'UsersController@delete'
        ]);
        Route::get('/profile', [
            'as'=> 'app.user.profile',
            'uses' => 'UsersController@profile'
        ]);
        Route::post('/saveUpdate', [
            'as'=> 'app.user.SaveUpdate',
            'uses' => 'UsersController@SaveUpdate'
        ]);
        Route::get('/allUserCompany', [
            'as'=> 'app.user.allUserCompany',
            'uses' => 'UsersController@allUserCompany'
        ]);
        Route::post('/resetPassword', [
            'as'=> 'app.user.resetPassword',
            'uses' => 'UsersController@resetPassword'
        ]);
        Route::post('/status', [
            'as'=> 'app.user.status',
            'uses' => 'UsersController@status'
        ]);

    });

});