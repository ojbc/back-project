<?php
Route::group(['prefix' => 'preload','middleware'=> ['cors']], function () {



    Route::get('/all', [
        'as' => 'app.Preload.alls',
        'uses' => 'PreloadController@alls'
    ]);
    Route::get('/paginate', [
        'as' => 'app.Preload.paginate',
        'uses' => 'PreloadController@paginate'
    ]);
    Route::post('/save', [
        'as' => 'app.Preload.save',
        'uses' => 'PreloadController@save'
    ]);
    Route::post('/delete', [
        'as' => 'app.Preload.delete',
        'uses' => 'PreloadController@deletePreload'
    ]);
    Route::post('/checkPreload', [
        'as' => 'app.Preload.checkPreload',
        'uses' => 'PreloadController@checkPreload'
    ]);
    Route::post('/printPdfnp', [
        'as' => 'app.Preload.printPdfnp',
        'uses' => 'PreloadController@printPdfnp'
    ]);
    Route::post('/printPdfap', [
        'as' => 'app.Preload.printPdfap',
        'uses' => 'PreloadController@printPdfap'
    ]);
    Route::post('/deletelist', [
        'as' => 'app.Preload.deletelist',
        'uses' => 'PreloadController@deletelist'
    ]);





});