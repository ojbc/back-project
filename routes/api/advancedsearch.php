<?php

Route::group(['prefix' => 'advancedsearch', 'middleware' => ['jwt.auth','cors']], function () {


    Route::get('/search', [
        'as' => 'app.advancedSearch.search',
        'uses' => 'AdvancedSearchController@search'
    ]);
    Route::get('/searchadmin', [
        'as' => 'app.advancedSearchadmin.searchadmin',
        'uses' => 'AdvancedSearchController@searchadmin'
    ]);

});