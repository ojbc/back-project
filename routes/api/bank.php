<?php
/**
 * Created by PhpStorm.
 * User: Desarrollo
 * Date: 22/10/2018
 * Time: 4:37 PM
 */
Route::group(['prefix' => 'banks','middleware'=> ['jwt.auth', 'cors']], function () {

    Route::get('/', [
        'as' => 'app.bank.all',
        'uses' => 'BankController@all'
    ]);

    Route::post('/save', [
        'as' => 'app.bank.save',
        'uses' => 'BankController@save'
    ]);

    Route::post('/delete', [
        'as' => 'app.bank.delete',
        'uses' => 'BankController@delete'
    ]);
    Route::get('/alls', [
        'as' => 'app.bank.alls',
        'uses' => 'BankController@alls'
    ]);


});